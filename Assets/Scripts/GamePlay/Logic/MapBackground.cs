using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapBackground : MonoBehaviour
{
    [SerializeField] private Transform Camera;
    private readonly Vector2 vectSpriteSize = new Vector2(10.24f, 10.24f);
    //[SerializeField] 

    //private void LateUpdate()
    //{
    //    Vector3 deltaMovement = cameraTransform.position - lastCameraPosition;
    //    transform.position += new Vector3(deltaMovement.x * parallaxEffectMultiplier.x, deltaMovement.y * parallaxEffectMultiplier.y);
    //    lastCameraPosition = cameraTransform.position;
    //    if (Mathf.Abs(cameraTransform.position.x - transform.position.x )>= textureUnitSizeX)
    //    {
    //        float offsetPosition = (cameraTransform.position.x - transform.position.x) % textureUnitSizeX;
    //        transform.position = new Vector3(cameraTransform.position.x + offsetPosition, transform.position.y, transform.position.z);

    //    }
    //    if (Mathf.Abs(cameraTransform.position.y - transform.position.y) >= textureUnitSizeY)
    //    {
    //        float offsetPosition = (cameraTransform.position.y - transform.position.y) % textureUnitSizeY;
    //        transform.position = new Vector3(transform.position.y , cameraTransform.position.y + offsetPosition, transform.position.z);

    //    }
    //}
    private void LateUpdate()
    {
        Vector3 jumpVector = Vector3.zero;
        bool isJump = false;
        if (Mathf.Abs(Camera.position.x - transform.position.x) > vectSpriteSize.x * 0.5f)
        {
            isJump = true;
            jumpVector += new Vector3(vectSpriteSize.x * Mathf.Sign(Camera.position.x - transform.position.x), 0, 0);
        }
        if (Mathf.Abs(Camera.position.y - transform.position.y) > vectSpriteSize.y * 0.5f)
        {
            isJump = true;
            jumpVector += new Vector3(0, vectSpriteSize.y * Mathf.Sign(Camera.position.y - transform.position.y), 0);
        }
        if (isJump)
        {
            transform.position += jumpVector;
        }
    }
}
