using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public abstract class CharacterBase : MonoBehaviour
{
    [SerializeField] protected Rigidbody2D rigidBody2D;
    [HideInInspector] [SerializeField] protected int currentHp;
    [HideInInspector] [SerializeField] protected int speed;
    [HideInInspector] [SerializeField] protected GameManager gameManager;

    [HideInInspector] public bool isDead = false;
    protected void Move(Vector2 vect)
    {
        rigidBody2D.velocity = Vector2.Lerp(vect.normalized * speed, vect.normalized * speed * 1.5f, speed * Time.deltaTime);
    }

    public virtual void TakeDamage(int damage)
    {
        if (isDead) return;
        currentHp -= damage;
        if (currentHp <= 0)
        {
            Dead();
            isDead = true;
        }
    }

    protected virtual void Dead()
    {
        CommonTweener.TweenRotateNinetyDegreeWithCallBack(transform, () => gameObject.SetActive(false));
    }


}
