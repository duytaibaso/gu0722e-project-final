using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[RequireComponent(typeof(Animator))]
public class Enemy : CharacterBase
{
    public EnemyType enemyType;
    [SerializeField] protected Animator animator;
    [SerializeField] protected ProgressBar healthBar;
    [SerializeField] protected EnemyData enemyData;
    [SerializeField] protected SpriteRenderer enemySpriteRenderer;
    [SerializeField] protected CapsuleCollider2D capsuleCollider;
    [SerializeField] protected Transform spriteObject;
    [HideInInspector] [SerializeField] protected EnemyBulletSpawner bulletSpawner;
    [HideInInspector] [SerializeField] protected float cooldownAttackTimer = 0;
    [HideInInspector] [SerializeField] protected float ogCooldownAttackTime;
    [HideInInspector] [SerializeField] int totalHpNow;
    [HideInInspector] [SerializeField] int damage;

    private bool canBeFlashedRed = true;

    // Start is called before the first frame update
    void Start()
    {
        animator.SetBool(AnimatorParameterData.Die, false);
        animator.Play(AnimatorParameterData.Idle);
        ogCooldownAttackTime = enemyData.attributes.cooldownAttack;
        currentHp = enemyData.attributes.hp;
        speed = enemyData.attributes.speed;
        damage = enemyData.attributes.damage;
        totalHpNow = currentHp;
    }
    public void SetData(GameManager _gameManager, EnemyBulletSpawner _enemyBulletSpawner, EnemyAttributes attributes)
    {
        Default();
        gameManager = _gameManager;
        bulletSpawner = _enemyBulletSpawner;
        PlusData(attributes);
    }

   

    private void PlusData(EnemyAttributes attributes)
    {
        ogCooldownAttackTime += enemyData.attributes.cooldownAttack;
        cooldownAttackTimer = ogCooldownAttackTime;
        currentHp += attributes.hp;
        speed += attributes.speed;
        damage += attributes.damage;
        totalHpNow = currentHp;
    }

    protected void Update()
    {
        if (gameManager.GetPlayer() != null)
        {
            Follow(gameManager.GetPlayer().transform);
        }
        cooldownAttackTimer -= Time.deltaTime;
    }

    public void SetEnemyType(EnemyType type)
    {
        enemyType = type;
    }

    void Follow(Transform target)
    {
        rigidBody2D.velocity = (target.position - transform.position).normalized * Time.deltaTime * enemyData.attributes.speed;
        //enemySpriteRenderer.flipX = Mathf.Sign(target.position.x - transform.position.x) == -1;
        enemySpriteRenderer.flipX = target.position.x - transform.position.x < 0;
    }
    protected void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag(ObjectTagsAndLayersData.playerTagAndLayer) && cooldownAttackTimer < 0)
        {
            CommonTweener.TweenShaker(spriteObject);
            spriteObject.position = collision.transform.position ;
            //Player player = collision.gameObject.GetComponent<Player>();
            gameManager.SendDamageToPlayer(damage);
            cooldownAttackTimer = ogCooldownAttackTime;
        }
    }
    protected void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag(ObjectTagsAndLayersData.playerTagAndLayer))
        {
            spriteObject.position = transform.position;
        }
    }

    public override void TakeDamage(int damage)
    {
        base.TakeDamage(damage);
        //healthBar.SetProgressBar((float)currentHp / (float)enemyData.attributes.hp);
        healthBar.SetProgressBar((float)currentHp / (float)totalHpNow);
        if (canBeFlashedRed)
        {
            CommonTweener.FlashRed(enemySpriteRenderer);
            canBeFlashedRed = false;
            StartCoroutine(WaitToFlash());
        }
    }
    IEnumerator WaitToFlash()
    {
        yield return new WaitForSeconds(0.1f);
        canBeFlashedRed = true;
    }


    protected override void Dead()
    {
        capsuleCollider.enabled = false;
        gameManager.OnEnemyDie(transform.position);
        animator.SetBool(AnimatorParameterData.Die, true);
        animator.Play(AnimatorParameterData.Die);
        CommonTweener.TweenRotateNinetyDegreeWithCallBack(transform, () => gameObject.SetActive(false));
    }
    public void Default()
    {
        capsuleCollider.enabled = true;
        gameObject.SetActive(true);
        animator.SetBool(AnimatorParameterData.Die, false);
        animator.Play(AnimatorParameterData.Idle);
        healthBar.Default();
        transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, 0f);
        isDead = false;
        currentHp = enemyData.attributes.hp;
    }
}
