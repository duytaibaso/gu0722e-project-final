using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;

public class EnemySpawner : SerializedMonoBehaviour
{
    [SerializeField] GameManager gameManager;
    [SerializeField] EnemyBulletSpawner enemyBulletSpawner;
    [SerializeField] TypeOfSpawn typeOfSpawn;

    //[SerializeField] List<GameObject> enemyPrefabs;
    [SerializeField] Dictionary<EnemyType, GameObject> enemyPrefabsDict;
    [SerializeField] float randomAroundRadius = 2f;
    [SerializeField] int spawnAmount;
    [SerializeField] float spawnRate = 2f;
    [HideInInspector] EnemyAttributes enemyAttributes;
    [SerializeField] int enemyPrefabsPoolSize = 250;
    [HideInInspector][SerializeField] List<Enemy> enemyPool = new List<Enemy>();

    private float timer;

    private void Start()
    {
        for (int i = 0; i < (int)enemyPrefabsPoolSize * 0.8f; i++)
        {
            var obj = Instantiate(enemyPrefabsDict[EnemyType.Zombie], transform);
            obj.SetActive(false);
            enemyPool.Add(obj.GetComponent<Enemy>());
        }
        for (int i = enemyPrefabsPoolSize - 1; i > enemyPrefabsPoolSize - enemyPrefabsPoolSize * 0.8f; i--)
        {
            var obj = Instantiate(enemyPrefabsDict[EnemyType.Range], transform);
            obj.SetActive(false);
            enemyPool.Add(obj.GetComponent<Enemy>());
        }

    }

    public void SetMainSpawnerData(MapLevelDataSpawn spawnData,  int minute, int mapLevel)
    {
        SetSpawnerData(spawnData.GetNumbersOfEnemySpawnInSpecificMinute(minute, mapLevel), spawnData.GetTimerOfEachSpawn(minute, mapLevel));
        enemyAttributes = spawnData.GetEnemyAttributesPlusEachMinute(minute, mapLevel);
    }

    public void SetSpawnerData(int amount, float rate)
    {
        spawnAmount = amount;
        spawnRate = rate;
    }

    public Tuple<int, float> GetSpawnerData()
    {
        return (spawnAmount, spawnRate).ToTuple();
    }
    public TypeOfSpawn GetTypeOfSpawn()
    {
        return typeOfSpawn;
    }
    private void Update()
    {
        timer -= Time.deltaTime;
        if (timer < 0)
        {
            Spawn();
            timer = spawnRate;
        }
    }
    private void Spawn()
    {
        if (OutOfSpawnAmount()) return;

        if (typeOfSpawn == TypeOfSpawn.OnParentTransform)
        {
            SpawnNormally();
        }
        else if (typeOfSpawn == TypeOfSpawn.RandomRadius)
        {
            SpawnRandomAroundRadius();
        }
        else if (typeOfSpawn == TypeOfSpawn.AroundCamera)
        {
            SpawnAroundCamera();
        }

    }
    private void SpawnNormally()
    {
        var obj = GetRandomEnemyType();
        obj.gameObject.SetActive(true);
        //enemy.Default();
        obj.SetData(gameManager, enemyBulletSpawner, enemyAttributes);
        spawnAmount--;
    }
    private void SpawnRandomAroundRadius()
    {
        float randomPosX = UnityEngine.Random.Range(-randomAroundRadius - transform.position.x, randomAroundRadius + transform.position.x);
        float randomPosY = UnityEngine.Random.Range(-randomAroundRadius - transform.position.y, randomAroundRadius + transform.position.y);
        var obj = GetRandomEnemyType();
        obj.gameObject.SetActive(true);
        //enemy.Default();
        obj.SetData(gameManager, enemyBulletSpawner, enemyAttributes);
        obj.transform.position = new Vector3(randomPosX, randomPosY, transform.position.z);
        spawnAmount--;
    }
    private void SpawnAroundCamera()
    {
        Vector3 cameraVect = new Vector3(Camera.main.pixelWidth, Camera.main.pixelHeight);
        float randomNumber = UnityEngine.Random.Range(-2f, 2f);
        //trai
        float randomPosX = UnityEngine.Random.Range(Camera.main.ScreenToWorldPoint(Vector3.zero).x, 
            Camera.main.ScreenToWorldPoint(Vector3.zero).x - randomAroundRadius);
        float randomPosY = UnityEngine.Random.Range(Camera.main.ScreenToWorldPoint(Vector3.zero).y, 
            Camera.main.ScreenToWorldPoint(cameraVect).y);
   
        switch (randomNumber)
        {
            //tren
            case float i when i < -1f:
                randomPosX = UnityEngine.Random.Range(Camera.main.ScreenToWorldPoint(Vector3.zero).x - randomAroundRadius,
                    Camera.main.ScreenToWorldPoint(cameraVect).x + randomAroundRadius);
                randomPosY = UnityEngine.Random.Range(Camera.main.ScreenToWorldPoint(cameraVect).y + randomAroundRadius - 2f,
                    Camera.main.ScreenToWorldPoint(cameraVect).y + randomAroundRadius);
                break;
            //duoi
            case float i when -1f < i && i < 0:
                randomPosX = UnityEngine.Random.Range(Camera.main.ScreenToWorldPoint(Vector3.zero).x - randomAroundRadius,
                    Camera.main.ScreenToWorldPoint(cameraVect).x + randomAroundRadius);
                randomPosY = UnityEngine.Random.Range(Camera.main.ScreenToWorldPoint(Vector3.zero).y - randomAroundRadius + 2f, 
                    Camera.main.ScreenToWorldPoint(Vector3.zero).y - randomAroundRadius);
                break;
            //phai
            case float i when 0 < i && i < 1f:
                randomPosX = UnityEngine.Random.Range(Camera.main.ScreenToWorldPoint(cameraVect).x,
                    Camera.main.ScreenToWorldPoint(cameraVect).x + randomAroundRadius);
                randomPosY = UnityEngine.Random.Range(Camera.main.ScreenToWorldPoint(Vector3.zero).y, Camera.main.ScreenToWorldPoint(cameraVect).y);
                break;
        }
        //var obj = Instantiate(GetRandomEnemyType(), new Vector3(randomPosX, randomPosY, transform.position.z), Quaternion.identity, transform);
        //obj.GetComponent<Enemy>().SetData(gameManager, enemyBulletSpawner);
        var obj = GetRandomEnemyType();
        obj.gameObject.SetActive(true);
        //enemy.Default();
        obj.SetData(gameManager, enemyBulletSpawner, enemyAttributes);
        obj.transform.position = new Vector3(randomPosX, randomPosY, transform.position.z);
        spawnAmount--;
    }

    private Enemy GetRandomEnemyType()
    {
        int enemyTypeInt = UnityEngine.Random.Range((int)EnemyType.Zombie, (int)EnemyType.Range + 1);
        if (UnityEngine.Random.Range(0,100) > gameManager.GetSpawnSystem().GetEnemySpawnRate((EnemyType)enemyTypeInt))
        {
            if (enemyTypeInt > 0)
            {
                enemyTypeInt--;
            }
        }
        for (int i = 0; i < enemyPrefabsPoolSize; i++)
        {
            if (enemyPool[i].gameObject.activeSelf == false && (EnemyType)enemyTypeInt == enemyPool[i].enemyType)
            {
                return enemyPool[i];
            }
        }
        Debug.Log("null");
        return null;
    }

    private bool OutOfSpawnAmount()
    {
        return spawnAmount <= 0;
    }

}

public enum TypeOfSpawn
{
    OnParentTransform = 0, RandomRadius = 1, AroundCamera = 2
}

public enum EnemyType
{
    Zombie, Range
}