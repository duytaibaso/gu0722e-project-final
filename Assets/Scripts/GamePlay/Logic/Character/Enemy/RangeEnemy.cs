using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeEnemy : Enemy
{
    //[SerializeField] LayerMask hitMask;
    [SerializeField] private int desiredMaginuteDistanceToPlayer = 10;
    [HideInInspector] [SerializeField] float shootTimer = 1f;

    // Update is called once per frame
    new void Update()
    {
        shootTimer -= Time.deltaTime;
        if (Vector3.Distance(gameManager.GetPlayer().transform.position, transform.position) > desiredMaginuteDistanceToPlayer) {
            base.Update();
        }
        else if (shootTimer < 0)
        {
            ShootBullet();
            shootTimer = 1f;
        }                
    }

    private void ShootBullet()
    {
        bulletSpawner.GetBullet(gameManager.GetPlayer().transform.position - transform.position, enemyData.attributes.damage, 
            enemyData.attributes.speed, transform.position);
    }

}
