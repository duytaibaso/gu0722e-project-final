using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public abstract class Player : CharacterBase
{
    [SerializeField] protected SpriteRenderer sprite;   
    [SerializeField] protected GunWeaponController gunController;
    [SerializeField] protected MeleeWeaponController meleeController;
    [HideInInspector] [SerializeField] protected Transform PointerTarget;
    [SerializeField] protected SkillData[]  skillsData = new SkillData[3];
    [HideInInspector] [SerializeField] float moveSoundDelay;
    protected void SendSkillInfoToGameManager()
    {
        gameManager.SetUpPlayerSkillsIconToInterface(skillsData, GetListOfActions());
    }
    protected abstract Action[] GetListOfActions();
    public virtual void ActivateSKill1()
    {
        gameManager.SkillActivate(0);
    }
    public virtual void ActivateSKill2()
    {
        gameManager.SkillActivate(1);
    }
    public virtual void ActivateSKill3()
    {
        gameManager.SkillActivate(2);
    }
    protected void Start()
    {
        currentHp = PlayerData.maxHP;
        speed = PlayerData.speed;
    }
    public void SetObjectTypes(Transform pointer, GameManager _gameManager)
    {
        PointerTarget = pointer;
        gameManager = _gameManager;
        gunController.Execute(_gameManager);
        meleeController.Execute(_gameManager);
    }

    public void AddHp(int amount)
    {
        currentHp = Mathf.Min(amount + currentHp, PlayerData.maxHP);
        gameManager.SetPlayerHealthToMenuController(currentHp);
    }

    public GunWeaponController GetGunController()
    {
        return gunController;
    }

    public GameManager GetGameManager()
    {
        return gameManager;
    }
    

    protected void Update()
    {
        if (InputAdapter.Instance.KeyEscPressedUp) gameManager.GetMenu().OpenPauseMenu();
        CheckFlipScale();
    }

    protected void FixedUpdate()
    {
        moveSoundDelay -= Time.deltaTime;
        CheckMovement();
    }

    protected void CheckMovement()
    {
        float x = InputAdapter.Instance.Horizontal;
        float y = InputAdapter.Instance.Vertical;
        Vector2 newVect = new Vector2(x, y);
        Move(newVect);
        if (newVect.sqrMagnitude > 0 && moveSoundDelay < 0) {
            moveSoundDelay = 1f;
            gameManager.GetSoundMaster().PlayPlayerFxSound(PlayerSoundType.Run); 
        }
    }

    private void CheckFlipScale()
    {
        transform.localScale = new Vector3( Mathf.Sign(PointerTarget.position.x - transform.position.x),
            transform.localScale.y, transform.localScale.z);
    }
    public override void TakeDamage(int damage)
    {
        CommonTweener.FlashRed(sprite);
        gameManager.SetPlayerHealthToMenuController(currentHp);
        base.TakeDamage(damage);
        gameManager.GetSoundMaster().PlayPlayerFxSound(PlayerSoundType.TakeDamage);
    }

    protected override void Dead()
    {
        base.Dead();
        gameManager.GetSoundMaster().PlayPlayerFxSound(PlayerSoundType.Die);
        enabled = false;
        gameManager.OnPlayerDie();

    }

}


