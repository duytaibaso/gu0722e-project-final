using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class Maiden : Player
{
    [SerializeField] ParticleSystem healingFx;
    [HideInInspector] [SerializeField] float healCoolDownTimer;

    [SerializeField] List<Spinner> ShurikenSpinners;
    [HideInInspector] [SerializeField] float shurikenSpinnersCdTimer;

    [SerializeField] ForceField forceField;
    [HideInInspector] [SerializeField] float forcefieldCdTimer;

    // Start is called before the first frame update
    new void Start()
    {
        SendSkillInfoToGameManager();
        base.Start();
        SetShurikenSpinners();
        SetForceFieldData();
    }
    // Update is called once per frame
    new void Update()
    {
        base.Update();
        CoolDownHeal();
        CoolDownShurikenSpinner();
        CoolDownForceField();
        Input();
    }
    private void SetForceFieldData()
    {
        forceField.SetData((skillsData[2] as RadiusCircleSkillData).duration, skillsData[2].damage);
    }
    private void SetShurikenSpinners()
    {
        RadiusCircleSkillData data = (skillsData[1] as RadiusCircleSkillData);
        for (int i = 0; i < ShurikenSpinners.Count; i++)
        {
            ShurikenSpinners[i].gameObject.SetActive(false);
            ShurikenSpinners[i].SetSpinnerData(transform, data.range, data.angularSpeed, data.damage, data.duration);
        }
    }
    //new void FixedUpdate()
    //{
    //    base.FixedUpdate();
    //}
    private void Input()
    {
        if (InputAdapter.Instance.Key1Pressed && healCoolDownTimer < 0) ActivateSKill1();
        if (InputAdapter.Instance.Key2Pressed && shurikenSpinnersCdTimer < 0) ActivateSKill2();
        if (InputAdapter.Instance.Key3Pressed && forcefieldCdTimer < 0) ActivateSKill3();
    }

    protected override Action[] GetListOfActions()
    {
        return new Action[3] { ActivateSKill1, ActivateSKill2, ActivateSKill3 };
    }
    public override void ActivateSKill1() //Heal
    {
        base.ActivateSKill1();
        currentHp += Mathf.Min(skillsData[0].damage, PlayerData.maxHP);
        // play healing fx
        ActiveHealingFx();
        gameManager.SetPlayerHealthToMenuController(currentHp);
        healCoolDownTimer = skillsData[0].cooldown;
    }

    private void ActiveHealingFx()
    {
        healingFx.Play();
    }

    public override void ActivateSKill2()
    {
        base.ActivateSKill2();
        for (int i = 0; i < ShurikenSpinners.Count; i++)
        {
            ShurikenSpinners[i].gameObject.SetActive(true);
        }
        shurikenSpinnersCdTimer = skillsData[1].cooldown;
    }
    public override void ActivateSKill3()
    {
        base.ActivateSKill3();
        forceField.Active();
        forcefieldCdTimer = skillsData[2].cooldown;
    }
    private void CoolDownHeal()
    {
        healCoolDownTimer -= Time.deltaTime;
    }

    private void CoolDownShurikenSpinner()
    {
        shurikenSpinnersCdTimer -= Time.deltaTime;
    }

    private void CoolDownForceField()
    {
        forcefieldCdTimer -= Time.deltaTime;
    }

}
