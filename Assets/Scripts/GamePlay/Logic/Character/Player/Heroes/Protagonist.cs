using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Protagonist : Player
{
    [SerializeField] float ogGenerateSpriteGhostEachTimeTimer;
    [SerializeField] float dashSpeedMultiplier = 100f;
    
    private GameObject[] poolSprite;
    private float ogCoolDownDash;
    private float generateSpriteGhostEachTimeTimer;
    private bool isDashing;
    private bool onDash;
    [HideInInspector] [SerializeField] float coolDownDash;

    [SerializeField] Grenade grenade;
    [HideInInspector] [SerializeField] float coolDownGrenadeThrown;
    [HideInInspector] [SerializeField] GameObject[] grenadesPool;

    [SerializeField] List<RocketRainFall> rockets;
    [HideInInspector] [SerializeField] float coolDownRocketSkill;


    //protected override void SendSkillInfoToGameManager()
    //{
    //    gameManager.SetUpPlayerSkillsIconToInterface(skillsData, GetListOfActions());
    //}
    protected override Action[] GetListOfActions()
    {
        return new Action[3] { ActivateSKill1, ActivateSKill2, ActivateSKill3 };
    }
    private new void Start()
    {
        SendSkillInfoToGameManager();
        base.Start();
        //SendSkillInfoToGameManager();
        SetUpDashSkill();
        SetUpThrowThreeGrenadesSkill();
        SetUpRocketRainSkill();
    }
    private void SetUpRocketRainSkill()
    {
        for (int i = 0; i < rockets.Count; i++)
        {
            rockets[i].SetDamage(skillsData[2].damage);
            var obj = Instantiate(rockets[i], gameManager.transform);
            obj.name = "rocket " + i.ToString();
            rockets[i] = obj.GetComponent<RocketRainFall>();
        }
    }
    private new void Update()
    {
        base.Update();
        CoolDownRocket();
        CoolDownGrenadeSkill();
        Input();
    }
    private new void FixedUpdate()
    {
        if (!isDashing) base.FixedUpdate();
        ExecutingDashSkillSpriteAndCollisionOff();
    }

    private void Input()
    {
        if (InputAdapter.Instance.Key1Pressed && !onDash) ActivateSKill1();
        if (InputAdapter.Instance.Key2Pressed && coolDownGrenadeThrown <= 0) ActivateSKill2();
        if (InputAdapter.Instance.Key3Pressed && coolDownRocketSkill <= 0) ActivateSKill3();
    }

    private void CoolDownRocket()
    {
        coolDownRocketSkill -= Time.deltaTime;
    }
    public override void ActivateSKill1()
    {
        base.ActivateSKill1();
        isDashing = true;
        onDash = true;
        DashOnVelocity();
    }
    public override void ActivateSKill2()
    {
        base.ActivateSKill2();
        coolDownGrenadeThrown = skillsData[1].cooldown;
        var angle = transform.eulerAngles;
        var _dir = PointerTarget.transform.position - transform.position;
        GetGrenade().GetComponent<Grenade>().SetTransform(_dir, transform.position, Quaternion.Euler(angle));
        angle.z += 20f;
        GetGrenade().GetComponent<Grenade>().SetTransform(_dir, transform.position, Quaternion.Euler(angle));
        angle.z -= 40f;
        GetGrenade().GetComponent<Grenade>().SetTransform(_dir, transform.position, Quaternion.Euler(angle));
    }

    public override void ActivateSKill3()
    {
        base.ActivateSKill3();
        for (int i = 0; i < rockets.Count; i++)
        {
            var random = new Vector2( transform.position.x , transform.position.y) + UnityEngine.Random.insideUnitCircle * (skillsData[2] as RocketRainSkillData).rangeOfActtack;
            rockets[i].SetInit(random, gameManager); ;
            rockets[i].gameObject.SetActive(true);
            rockets[i].ActiveSprite();
        }
        coolDownRocketSkill = skillsData[2].cooldown;
    }

    
    private void CoolDownGrenadeSkill()
    {
        coolDownGrenadeThrown -= Time.deltaTime;
    }
    private void ExecutingDashSkillSpriteAndCollisionOff()
    {
        if (onDash)
        {
            coolDownDash -= Time.deltaTime;
            generateSpriteGhostEachTimeTimer -= Time.deltaTime;
            gameObject.layer = LayerMask.NameToLayer(ObjectTagsAndLayersData.IgnoreCollisionLayer);
            if (isDashing && generateSpriteGhostEachTimeTimer <= 0)
            {
                GameObject target = GetInActiveGhostSprite();
                target.SetActive(true);
                target.transform.position = transform.position;
                target.transform.localScale = new Vector3(transform.localScale.x * sprite.transform.localScale.x,
                    sprite.transform.localScale.y, sprite.transform.localScale.z);
                generateSpriteGhostEachTimeTimer = ogGenerateSpriteGhostEachTimeTimer;
            }
            else if (coolDownDash <= 0)
            {
                coolDownDash = ogCoolDownDash;
                gameObject.layer = LayerMask.NameToLayer(ObjectTagsAndLayersData.playerTagAndLayer);
                onDash = false;
            }
        }
    }

    private GameObject GetInActiveGhostSprite()
    {
        for (int i = 0; i < poolSprite.Length; i++)
        {
            if (poolSprite[i].activeSelf == false)
            {
                return poolSprite[i];
            }
        }
        return null;
    }

    private void DashOnVelocity()
    {
        Vector2 dir;
        if (!InputAdapter.Instance.CheckIsNotMoving())
            dir = new Vector2(InputAdapter.Instance.Horizontal, InputAdapter.Instance.Vertical).normalized;
        else
            dir = new Vector2(PointerTarget.position.x - transform.position.x, PointerTarget.position.y - transform.position.y).normalized;
        rigidBody2D.velocity = dir * PlayerData.speed * dashSpeedMultiplier;
        StartCoroutine(PreparingDashOff());
    }

    IEnumerator PreparingDashOff()
    {
        yield return new WaitForSeconds((skillsData[0] as GhostDashData).dashDuration);
        isDashing = false;
    }

    private void SetUpDashSkill()
    {
        var dashData = (skillsData[0] as GhostDashData);
        coolDownDash = dashData.cooldown;
        ogCoolDownDash = coolDownDash;
        ogGenerateSpriteGhostEachTimeTimer = dashData.generateSpriteGhostEachTimeTimer;
        dashSpeedMultiplier = dashData.dashSpeedMultiplier;
        int length = (int)Mathf.Round(dashData.dashDuration / ogGenerateSpriteGhostEachTimeTimer);
        poolSprite = new GameObject[length];
        for (int i = 0; i < length; i++)
        {
            var obj = Instantiate(sprite, transform.position, Quaternion.identity, gameManager.transform);
            obj.sortingOrder = -10;
            obj.color = new Color(Color.magenta.r, Color.magenta.g, Color.magenta.b, 0.5f);
            obj.gameObject.AddComponent<GhostDash>();
            poolSprite[i] = obj.gameObject;
            obj.gameObject.SetActive(false);
        }
    }
    private void SetUpThrowThreeGrenadesSkill()
    {
        grenade.SetInit(skillsData[1], gameManager); ;
        grenadesPool = new GameObject[6];
        for (int i = 0; i < grenadesPool.Length; i++)
        {
            var obj = Instantiate(grenade.gameObject, gameManager.transform, false);
            obj.SetActive(true);
            grenadesPool[i] = obj;
        }
    }
    private GameObject GetGrenade()
    {
        for (int i = 1; i < grenadesPool.Length; i++)
        {
            if (grenadesPool[i].activeSelf == false)
            {
                grenadesPool[i].SetActive(true);
                return grenadesPool[i];
            }
        }
        return grenadesPool[0];
    }

}
