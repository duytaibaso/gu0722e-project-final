using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaptureObjective : MonoBehaviour
{
    [SerializeField] float point = 0;
    [SerializeField] GameManager gameManager;

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.collider.CompareTag(ObjectTagsAndLayersData.playerTagAndLayer))
        {
            point += Time.deltaTime;
            if (point >= 100)
            {
                //victory;
                //tell gameManager is completely capture;
            }
        }
    }




}
