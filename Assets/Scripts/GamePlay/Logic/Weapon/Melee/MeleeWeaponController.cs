using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeWeaponController : MonoBehaviour
{
    [SerializeField] Player player;
    [SerializeField] WeaponData weaponData;
    [HideInInspector] [SerializeField] GameManager gameManager;

    private Melee melee;
    
    public void Execute(GameManager gameManager)
    {
        this.gameManager = gameManager;
        LoadMelee();
    }

    private void LoadMelee()
    {
        MeleeWeaponType type = (MeleeWeaponType)EquipmentSystem.meleeEnumWeaponEquipped.Item1;
        Rarity rarity = (Rarity)EquipmentSystem.meleeEnumWeaponEquipped.Item2.Item1;
        int level = EquipmentSystem.meleeEnumWeaponEquipped.Item2.Item2;
        //MeleeWeaponInfo meleeInfo = weaponData.GetMeleeWeaponInfo(type);

        melee = Resources.Load<Melee>(WeaponData.MeleeWeaponPrefabsFileResourcePath + 
            weaponData.GetMeleeWeaponResourcesPrefabStringPath(type));
        var obj = Instantiate(melee.gameObject, transform);
        obj.SetActive(true);
        obj.GetComponent<Melee>().SetMeleeData(weaponData.GetMeleeWeaponDamage(type, rarity, level), this, player, gameManager);

        gameManager.GetMenu().SetMeleeIcon(weaponData.GetMeleeItemIconIngame(type));
    
    }
}
