﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Melee : MonoBehaviour
{
    public MeleeWeaponType weaponType;
    [SerializeField] private Animator meleeAnimator;
    [SerializeField] private CapsuleCollider2D capsuleCollider2D;
    [SerializeField] private HitBox hitbox;

    [HideInInspector] [SerializeField] private MeleeWeaponController meleeController;
    [HideInInspector] [SerializeField] private Player player;

    [HideInInspector] [SerializeField] private float attackSpeed = 0f;
    [HideInInspector] [SerializeField] GameManager gameManager;
    [HideInInspector] [SerializeField] private bool uiIconClicked;

    public void SetUIClicked()
    {
        uiIconClicked = true;
    }

    private void Start()
    {
        capsuleCollider2D.enabled = false;
        meleeAnimator.SetBool(AnimatorParameterData.MeleeSlash, false);
    }

    public void SetMeleeData(int damage, MeleeWeaponController controller, Player player, GameManager gameManager)
    {
        this.player = player;
        meleeController = controller;
        //hitbox.SetData(gameManager, info.damage);
        hitbox.SetData( damage);
        gameManager.SetMeleeAction(this);
        this.gameManager = gameManager;
    }

    void FixedUpdate()
    {
        attackSpeed -= Time.deltaTime;
        if (attackSpeed <= 0)
        {
            capsuleCollider2D.enabled = false;
            ActivateSlashAnimation(false);
            uiIconClicked = false;
        }
    }

    void Update()
    {
        if ((InputAdapter.Instance.MeleeAttackPressedUp && attackSpeed <= 0) || (uiIconClicked && attackSpeed <= 0))
        {
            Slash();
        }
    }

    private void Slash()
    {
        capsuleCollider2D.enabled = true;
        ActivateSlashAnimation();
        attackSpeed = meleeAnimator.GetCurrentAnimatorStateInfo(0).length;
        gameManager.GetSoundMaster().PlayMeleeFxSound(weaponType);
    }

    private void ActivateSlashAnimation(bool check = true)
    {
        meleeAnimator.SetBool(AnimatorParameterData.MeleeSlash, check);
        meleeAnimator.SetBool(AnimatorParameterData.MeleeSlashCompleted, !check);
    }


}
