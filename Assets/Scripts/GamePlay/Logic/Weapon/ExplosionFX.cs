using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionFX : MonoBehaviour
{
    private float timer = 2f;
    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        if (timer < 0)
        {
            //Destroy(gameObject);
            gameObject.SetActive(false);
            timer = 2f;
        }
    }
}
