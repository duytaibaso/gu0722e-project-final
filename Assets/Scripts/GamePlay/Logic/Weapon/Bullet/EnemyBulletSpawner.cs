using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBulletSpawner : BulletSpawner
{
    private void Start()
    {
        SetData();
    }

    private void SetData()
    {
        bulletPool = new GameObject[poolSize];
        for (int i = 0; i < poolSize; i++)
        {
            bulletPool[i] = Instantiate(bulletPrefabs[BulletType.Normal], transform);
            bulletPool[i].SetActive(false);
            bulletPool[i].name = "Enemy Bullet" + i.ToString();
        }
    }

    public EnemyBullet GetBullet(Vector2 vectAtk, int damage, int speed, Vector3 startPos)
    {
        for (int i = 0; i < poolSize; i++)
        {
            GameObject bulletObj = bulletPool[i];
            if (!bulletObj.activeSelf)
            {
                EnemyBullet bullet = bulletObj.GetComponent<EnemyBullet>();
                bullet.SetBulletData(vectAtk, speed, damage, startPos);
                bullet.gameObject.SetActive(true);
                return bullet;
            }
        }
        return null;
    }






}
