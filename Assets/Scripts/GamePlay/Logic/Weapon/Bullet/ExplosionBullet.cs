using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionBullet : Bullet
{
    [SerializeField] HitBox hitBox;
    [SerializeField] GameObject spriteObject;
    [SerializeField] GameObject fxParticle;

    //private void Start()
    //{
    //    hitBox.SetData(damage);
    //    hitBox.gameObject.SetActive(false);
    //}

    new void Update()
    {
        base.Update();
        transform.localScale = new Vector3(scaleSign, transform.localScale.y, transform.localScale.z);
    }

    private new void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(ObjectTagsAndLayersData.enemyTag))
        {
            hitBox.gameObject.SetActive(true);
            spriteObject.SetActive(false);
            //gameObject.SetActive(false);
            gameManager.GetSoundMaster().PlayAnExplosion();
            gameManager.GetExplosiveManager().SpawnExplosion(transform.position);
            fxParticle.SetActive(false);
            enabled = false;
        }
    }

    public void Default()
    {
        hitBox.SetData(damage);
        hitBox.gameObject.SetActive(false);
        spriteObject.SetActive(true);
        fxParticle.SetActive(true);
        enabled = true;
    }

}
