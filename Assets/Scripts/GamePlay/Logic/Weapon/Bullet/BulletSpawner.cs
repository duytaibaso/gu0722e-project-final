﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class BulletSpawner : SerializedMonoBehaviour
{
    [SerializeField] protected Dictionary<BulletType, GameObject> bulletPrefabs;
    [SerializeField] protected int poolSize;
    
    [HideInInspector] [SerializeField] protected GameObject bulletPrefab;
    [SerializeField] protected GameObject[] bulletPool;

    public void SetData(GameManager _gameManager, GunWeaponController _guncontroller, WeaponAttributes attribute, 
        Rarity rarity, int level, GunWeaponType gunWeaponType)
    {
        bulletPrefab = GetBulletPrefab(_guncontroller.GetWeaponData().GetBulletType(
            _guncontroller.GetWeaponData().GetGunModStatType(gunWeaponType, rarity).Item1));
        poolSize = (int)(attribute.ammoMagazine * 2);
        bulletPool = new GameObject[poolSize];
        for (int i = 0; i < poolSize; i++)
        {
            GunWeaponType type = _guncontroller.GetUsingGun().GetWeaponType();
            bulletPool[i] = Instantiate(bulletPrefab, _gameManager.transform);
            bulletPool[i].GetComponent<Bullet>().SetBulletData( _guncontroller.GetWeaponData().GetGunBulletSpeed(type),
                    _guncontroller.GetWeaponData().GetGunWeaponDamage(type, rarity, level), _gameManager);           
            bulletPool[i].SetActive(false);
            bulletPool[i].name = bulletPrefab.name + " " + i.ToString();
        }
    }

    public GameObject GetBulletPrefab(BulletType bulletType)
    {
        return bulletPrefabs[bulletType];
    }
    
    public Bullet GetBullet()
    {
        for (int i = 0; i < poolSize; i++)
        {
            GameObject bulletObj = bulletPool[i];
            if ( !bulletObj.activeSelf)
            {
                Bullet bullet = bulletObj.GetComponent<Bullet>();
                bullet.gameObject.SetActive(true);
                return bullet;
            }
        }
        return null;
    }

}


