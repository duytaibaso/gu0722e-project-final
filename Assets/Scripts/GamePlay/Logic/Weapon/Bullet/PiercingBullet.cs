using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PiercingBullet : Bullet
{
    private new void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(ObjectTagsAndLayersData.enemyTag))
        {
            collision.gameObject.GetComponent<Enemy>().TakeDamage(damage);
        }
    }



}
