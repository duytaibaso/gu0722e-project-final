using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    const float originTimer = 3f;
    [HideInInspector] [SerializeField] protected GameManager gameManager;
    [HideInInspector] [SerializeField] protected int damage;
    [HideInInspector] [SerializeField] protected float speed;

    [HideInInspector] [SerializeField] protected float timer = originTimer;

    [HideInInspector] [SerializeField] protected float scaleSign;

    public void SetBulletData(float _speed, int _damage, GameManager _gameManager)
    {
        damage = _damage;
        speed = _speed;
        gameManager = _gameManager;
    }

    protected void Update()
    {
        transform.Translate(Vector3.right * (Time.deltaTime * speed) * scaleSign, Space.Self);
        //transform.localScale = new Vector3(scaleSign, transform.localScale.y, transform.localScale.z);
        timer -= Time.deltaTime;
        if (timer < 0)
        {
            OnBulletTimeOutDeactivate();
        }
    }

    protected void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.CompareTag(ObjectTagsAndLayersData.enemyTag))
        {
            collider.GetComponent<Enemy>().TakeDamage(damage);
            gameObject.SetActive(false);
        }
    }

    protected void OnBulletTimeOutDeactivate()
    {
        gameObject.SetActive(false);
        timer = originTimer;
    }
    public void SetBulletTransform(Vector3 pos, Quaternion rotation, float scale)
    {
        transform.position = pos;
        transform.rotation = rotation;
        scaleSign = Mathf.Sign(scale);
    }



}





