using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : Bullet
{
    [HideInInspector] [SerializeField] Vector2 atkVector2;


    public void SetBulletData(Vector2 _vectAtk, float _speed, int _damage, Vector3 startPos)
    {
        atkVector2 = _vectAtk;
        speed = _speed;
        damage = _damage;
        transform.rotation = Quaternion.Euler(new Vector3(transform.rotation.x, transform.rotation.y,
            Vector2.SignedAngle(Vector2.right, atkVector2.normalized)));
        transform.position = startPos;
    }

    // Update is called once per frame
    new void Update()
    {
        transform.Translate(Vector3.right * (Time.deltaTime * speed / 3) , Space.Self);
        timer -= Time.deltaTime;
        if (timer < 0)
        {
            OnBulletTimeOutDeactivate();
        }
    }

    private new void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(ObjectTagsAndLayersData.playerTagAndLayer))
        {
            collision.GetComponent<Player>().TakeDamage(damage);
        }
    }

}
