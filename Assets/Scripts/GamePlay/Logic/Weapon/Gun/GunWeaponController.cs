using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunWeaponController : MonoBehaviour
{
    [SerializeField] Player player;
    [SerializeField] WeaponData weaponData;
    [HideInInspector] [SerializeField] GameManager gameManager;
    [HideInInspector] [SerializeField] Dictionary<bool, Gun> GunUsingDict;

    private Gun currentGun;
    private Gun onHoldGun;

    private bool switchedGun = false;

    private float delayPress;

    public void Execute(GameManager gameManager)
    {
        this.gameManager = gameManager;
        LoadGun();
    }
    public WeaponData GetWeaponData()
    {
        return weaponData;
    }
    public Gun GetUsingGun()
    {
        if (switchedGun)
            return onHoldGun;
        return currentGun;
    }

    private void Start()
    {
        GunUsingDict = new Dictionary<bool, Gun>()
        {
            {false, currentGun },
            {true, onHoldGun },
        };
    }

    private void Update()
    {
        delayPress -= Time.deltaTime;
        if (Input.GetKeyDown(KeyCode.Tab ) && delayPress < 0)
        {
            SwitchGun();
        }
    }

    public void SupplyAmmo(int amount)
    {
        currentGun.ReceiveAmmo(amount);
        onHoldGun.ReceiveAmmo(amount);
        UpdateAmmo(GunUsingDict[switchedGun].GetCurrentAmmoStatus().Item1, GunUsingDict[switchedGun].GetCurrentAmmoStatus().Item2);
    }

    public void UpdateAmmo(int ammo, int supply)
    {
        gameManager.SendBulletInfoMenuController(ammo, supply);
    }

    public void SwitchGun()
    {
        switchedGun = !switchedGun;
        transform.GetChild(0).gameObject.SetActive(!switchedGun);
        transform.GetChild(1).gameObject.SetActive(switchedGun);
        delayPress = 1f;
        gameManager.GetMenu().SwitchGunIcon();
        UpdateAmmo(GunUsingDict[switchedGun].GetCurrentAmmoStatus().Item1, GunUsingDict[switchedGun].GetCurrentAmmoStatus().Item2);
    }

    private void LoadGun()
    {
        GunWeaponType type1 = (GunWeaponType)EquipmentSystem.primaryEnumWeaponEquipped.Item1;
        GunWeaponType type2 = (GunWeaponType)EquipmentSystem.secondaryEnumWeaponEquipped.Item1;
        GunWeaponInfo primaryWeaponInfo = weaponData.GetGunWeaponInfo(type1);
        GunWeaponInfo secondaryWeaponInfo = weaponData.GetGunWeaponInfo(type2);

        currentGun = Resources.Load<Gun>(WeaponData.GunWeaponPrefabsFileResourcePath +
            weaponData.GetGunWeaponResourcesPrefabStringPath(type1));
        currentGun.SetGunData(player, this, primaryWeaponInfo, gameManager);
        currentGun.GetBulletSpawner().SetData(gameManager, this, primaryWeaponInfo.weaponAttributes,
            (Rarity)EquipmentSystem.primaryEnumWeaponEquipped.Item2.Item1, EquipmentSystem.primaryEnumWeaponEquipped.Item2.Item1, type1);

        onHoldGun = Resources.Load<Gun>(WeaponData.GunWeaponPrefabsFileResourcePath + 
            weaponData.GetGunWeaponResourcesPrefabStringPath(type2));
        onHoldGun.SetGunData(player, this, secondaryWeaponInfo, gameManager);
        onHoldGun.GetBulletSpawner().SetData(gameManager, this, secondaryWeaponInfo.weaponAttributes,
            (Rarity)EquipmentSystem.secondaryEnumWeaponEquipped.Item2.Item1, EquipmentSystem.secondaryEnumWeaponEquipped.Item2.Item1, type2);

        Instantiate(currentGun.gameObject, transform).SetActive(true);
        Instantiate(onHoldGun.gameObject, transform).SetActive(false);
        UpdateAmmo(primaryWeaponInfo.weaponAttributes.ammoMagazine, primaryWeaponInfo.weaponAttributes.ammoSupply);

        gameManager.GetMenu().SetGunssIcon(weaponData.GetGunItemIconIngame(type1), weaponData.GetGunItemIconIngame(type2));
    }





}
