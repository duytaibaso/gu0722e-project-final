﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(BulletSpawner))]

public abstract class Gun : MonoBehaviour
{
    [SerializeField] protected Animator animator;
    [SerializeField] protected BulletSpawner BulletSpawner;
    [SerializeField] protected Transform BulletSpawnPos;
    [HideInInspector] [SerializeField] protected GameManager gameManager;
    [HideInInspector] [SerializeField] protected GunWeaponType weaponType;
    [HideInInspector] [SerializeField] protected GunWeaponController gunController;
    [HideInInspector] [SerializeField] protected Player player;

    [HideInInspector] [SerializeField] protected int fireratePerSecond;
    [HideInInspector] [SerializeField] protected float bulletSpeed;
    [HideInInspector] [SerializeField] protected int bulletDamage;
    [HideInInspector] [SerializeField] protected int reloadTime;

    [HideInInspector] [SerializeField] protected int ammoMagazine;
    [HideInInspector] [SerializeField] protected int ogAmmoMagazine;
    [HideInInspector] [SerializeField] protected int ammoSupply;

    [HideInInspector] [SerializeField] protected float gunReloadTimer;
    [HideInInspector] [SerializeField] protected float ogGunReloadTimer;

    [HideInInspector] [SerializeField] protected bool canPlayGunSound = true;

    /// <summary>
    /// Ảnh hưởng bởi Weapon Data Accuracy
    /// </summary>
    [SerializeField] protected float spreadAngle;

    [SerializeField] protected float bulletTimerCountDown = 0f;
    public bool isReloading { protected set; get; }


    /// <summary>
    /// int dau la ammoMagazine, int sau la ammoSupply
    /// </summary>
    /// <returns></returns>
    public Tuple<int,int> GetCurrentAmmoStatus()
    {
        return (ammoMagazine, ammoSupply).ToTuple();
    }

    public GunWeaponType GetWeaponType()
    {
        return weaponType;
    }
    public BulletSpawner GetBulletSpawner()
    {
        return BulletSpawner;
    }

    public void SetGunData(Player _player, GunWeaponController gunController, GunWeaponInfo weaponInfo, GameManager gameManager)
    {
        player = _player;
        this.gunController = gunController;
        weaponType = (GunWeaponType)EquipmentSystem.primaryEnumWeaponEquipped.Item1;
        fireratePerSecond = weaponInfo.weaponAttributes.fireratePerSecond;
        ammoMagazine = weaponInfo.weaponAttributes.ammoMagazine;
        ammoSupply = weaponInfo.weaponAttributes.ammoSupply;
        ogGunReloadTimer = weaponInfo.weaponAttributes.reloadTime;
        ogAmmoMagazine = weaponInfo.weaponAttributes.ammoMagazine;
        spreadAngle = gunController.GetWeaponData().AccuracyToSpreadAngleFormula(weaponInfo.weaponAttributes.accuracy);
        this.gameManager = gameManager;
    }
    public void ReceiveAmmo(int amount)
    {
        ammoSupply += amount / 2;
    }
    protected void Start()
    {
        animator.SetBool(AnimatorParameterData.GunReloadCompleted, true);
        animator.SetBool(AnimatorParameterData.GunNeedReload, false);
    }
    protected void Update()
    {
        bulletTimerCountDown -= Time.deltaTime;
        if (isReloading ) Reloading();
        if (InputAdapter.Instance.KeyRPressedUp && ammoMagazine <= ogAmmoMagazine && !isReloading) 
            StartReloadGun();
    }
    protected void FixedUpdate()
    {
        OnLeftButtonMouseClicked();
    }
    
    protected void OnLeftButtonMouseClicked()
    {
        if (InputAdapter.Instance.Mouse0Pressed)
        {
            int relativeAddictiveScale = player.transform.localScale.x > 0 ? 1 : 180;
            var dir = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
            transform.eulerAngles = new Vector3(transform.localRotation.x, transform.localRotation.y,
               -Vector2.SignedAngle(dir, Vector2.right) + relativeAddictiveScale);
            ShootBullet();
        }
        else transform.rotation = Quaternion.identity;
    }

    protected bool OutOfAmmunation()
    {
        return ammoSupply <= 0 && ammoMagazine <= 0;
    }

    protected void Reloading()
    {
        gunReloadTimer -= Time.deltaTime;
        gameManager.GetSoundMaster().PLayerGunFxSound(GunSoundType.Reload);
        CheckReloadComplete();
    }
    private void CheckReloadComplete()
    {
        if (gunReloadTimer <= 0)
        {
            SetAnimationTransitionParamsWhenLoading(false);
            isReloading = false;
            ammoMagazine = Mathf.Min(ogAmmoMagazine, ammoSupply - ammoMagazine);
            ammoSupply -= ammoMagazine;
            ReportAmmo();
        }
    }

    protected bool CheckReloading()
    {
        if (IsInSufficientAmmo() && !isReloading)
        {
            StartReloadGun();
        }
        return isReloading;
    }
    protected void StartReloadGun()
    {
        isReloading = true;
        gunReloadTimer = ogGunReloadTimer;
        SetAnimationTransitionParamsWhenLoading(true);
    }

    protected void SetAnimationTransitionParamsWhenLoading(bool reloading)
    {
        animator.SetBool(AnimatorParameterData.GunNeedReload, reloading);
        animator.SetBool(AnimatorParameterData.GunReloadCompleted, !reloading);
    }

    protected bool IsInSufficientAmmo()
    {
        return ammoMagazine <= 0;
    }
    protected bool IsDelayedByRateOfFire()
    {
        return bulletTimerCountDown > 0;
    }

    private void SetBulletCD()
    {
        bulletTimerCountDown = 1f / fireratePerSecond;
    }
    public abstract void ShootBullet();

    private  void ReportAmmo() {
        gunController.UpdateAmmo(ammoMagazine, ammoSupply);
    }
    protected void SetUpAfterShot()
    {
        ammoMagazine--;
        SetBulletCD();
        ReportAmmo();
    }

    
    protected void PLayerGunFxSound(GunSoundType type)
    {
        if (canPlayGunSound)
        {
            gameManager.GetSoundMaster().PLayerGunFxSound(type);
            StartCoroutine(WaitSecondsForGunSoundPlaying());
        }
    }
    IEnumerator WaitSecondsForGunSoundPlaying()
    {
        //yield return new WaitForSeconds(0.2f);
        yield return new WaitForSeconds(1f);
        canPlayGunSound = true;
    }
}
