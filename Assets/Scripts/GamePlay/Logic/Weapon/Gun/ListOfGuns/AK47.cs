using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AK47 : Gun
{


    public override void ShootBullet()
    {
        if (CheckReloading() || IsDelayedByRateOfFire() || OutOfAmmunation()) return;

        Bullet bullet = BulletSpawner.GetBullet();
        var angle = transform.eulerAngles;
        angle.z += Random.Range(-spreadAngle, spreadAngle);
        bullet.SetBulletTransform(BulletSpawnPos.position, Quaternion.Euler(angle), player.transform.localScale.x);
        PLayerGunFxSound(GunSoundType.AK47);
        SetUpAfterShot();
    }

}
