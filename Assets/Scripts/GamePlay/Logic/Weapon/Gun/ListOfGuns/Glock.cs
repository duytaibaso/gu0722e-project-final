using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Glock : Gun
{
    public override void ShootBullet()
    {
        if (CheckReloading() || OutOfAmmunation()) return;
        if (bulletTimerCountDown > 0) return;

        Bullet bullet = BulletSpawner.GetBullet();
        bullet.SetBulletTransform(BulletSpawnPos.position, transform.rotation, player.transform.localScale.x);
        PLayerGunFxSound(GunSoundType.Glock); 

        SetUpAfterShot();
    }
}
