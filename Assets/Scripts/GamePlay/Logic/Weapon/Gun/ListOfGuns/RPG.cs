using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RPG : Gun
{
    public override void ShootBullet()
    {
        if (CheckReloading() || OutOfAmmunation()) return;
        if (bulletTimerCountDown > 0) return;

        Bullet bullet = BulletSpawner.GetBullet();
        bullet.SetBulletTransform(BulletSpawnPos.position, transform.rotation, player.transform.localScale.x);
        (bullet as ExplosionBullet).Default();
        PLayerGunFxSound(GunSoundType.RPG); ;

        SetUpAfterShot();


    }

}
