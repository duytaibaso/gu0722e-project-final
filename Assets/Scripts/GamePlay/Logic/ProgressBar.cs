using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgressBar : MonoBehaviour
{
    [SerializeField] Transform bar;
    /// <summary>
    /// 
    /// </summary>
    /// <param name="amount">float 0 - 1</param>
    public void SetProgressBar(float amount)
    {
        bar.localScale = new Vector3( Mathf.Max(0, amount), bar.localScale.y, bar.localScale.z);
    }
    public void Default()
    {
        bar.localScale = Vector3.one;
    }


}
