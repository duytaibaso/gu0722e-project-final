using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grenade : MonoBehaviour
{
    //[SerializeField] GameObject explosionFX;
    [SerializeField] GameObject spriteGrenade;
    [SerializeField] HitBox hitBox;
    [HideInInspector][SerializeField] GameManager gameManager;
    private float timer = 2f;
    private Vector3 dir;
    public void SetInit(SkillData data, GameManager gameManager)
    {
        hitBox.SetData( data.damage);
        this.gameManager = gameManager;
    }
    public void SetTransform(Vector3 dir, Vector3 pos, Quaternion rotation)
    {
        gameObject.SetActive(true);
        spriteGrenade.SetActive(true);
        transform.position = pos;
        transform.rotation = rotation;
        this.dir = dir;
    }
    private void Start()
    {
        gameObject.SetActive(false);
        hitBox.gameObject.SetActive(false);
        spriteGrenade.SetActive(false);
    }

    void FixedUpdate()
    {
        transform.Translate( dir.normalized * (Time.deltaTime * 3f), Space.Self);
        timer -= Time.deltaTime;
        //transform.rotation = Quaternion.Euler(new Vector3(0,0,Time.deltaTime));
        if (timer <= 0)
        {
            Explode();
            timer = 2f;
        }
    }
    private void Explode()
    {
        ActiveHitBox();
        //Instantiate(explosionFX, transform.position, Quaternion.identity);
        gameManager.GetExplosiveManager().SpawnExplosion(transform.position);
        gameManager.GetSoundMaster().PlayAnExplosion();
        StartCoroutine(Deactivate());
    }

    IEnumerator Deactivate()
    {
        yield return new WaitForSeconds(0.5f);
        Deactive();
    }
    private void Deactive()
    {
        hitBox.gameObject.SetActive(false);
        gameObject.SetActive(false);
    }
    private void ActiveHitBox()
    {
        hitBox.gameObject.SetActive(true);
        spriteGrenade.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(ObjectTagsAndLayersData.enemyTag))
        {
            Explode();
        }
    }
}
