using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceField : MonoBehaviour
{
    const float collisionImpactTime = 0.5f;
    //private float collisionImpactTimer;
    private bool canCollide = true;
    [SerializeField] Animator animator;
    [HideInInspector] [SerializeField] float duration;
    [HideInInspector] [SerializeField] float ogDuration;
    [HideInInspector] [SerializeField] int damage;

    private void Update()
    {
        duration -= Time.deltaTime;
        if (duration < 0)
        {
            gameObject.SetActive(false);
            duration = ogDuration;
        }
    }

    public void Active()
    {
        gameObject.SetActive(true);
        animator.Play(AnimatorParameterData.ActiveForceFieldLightning);
    }

    public void SetData(float _duration, int _damage)
    {
        gameObject.SetActive(false);
        ogDuration = _duration;
        duration = _duration;
        damage = _damage;
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.collider.CompareTag(ObjectTagsAndLayersData.enemyTag) && canCollide)
        {
            Debug.Log("detect");
            collision.collider.GetComponent<Enemy>().TakeDamage(damage);
            canCollide = false;
            StartCoroutine(RestartCollisionImpactTimer());
        }
    }
    IEnumerator RestartCollisionImpactTimer()
    {
        yield return new WaitForSeconds(collisionImpactTime);
        canCollide = true;
    }

}
