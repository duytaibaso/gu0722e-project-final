using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spinner : MonoBehaviour
{
    [HideInInspector] [SerializeField] Transform Center;
    [HideInInspector] [SerializeField] private float PosX, PosY, Angle;
    [HideInInspector] [SerializeField] float Radius;
    [HideInInspector] [SerializeField] float AngularSpeed;
    [HideInInspector] [SerializeField] int damage;
    [HideInInspector] [SerializeField] float duration;


    public void SetSpinnerData(Transform _target, float _radius, float _speed, int _damage, float _duration)
    {
        AngularSpeed = _speed;
        Radius = _radius;
        Center = _target;
        damage = _damage;
        duration = _duration;
        Angle = Quaternion.FromToRotation(Vector3.right, (transform.position - _target.position)).eulerAngles.z;
    }

    // Update is called once per frame
    void Update()
    {
        if (duration <= 0) {
            gameObject.SetActive(false);
            return; 
        }
        PosX = Center.position.x + Mathf.Cos(Angle * Mathf.Deg2Rad) * Radius;
        PosY = Center.position.y + Mathf.Sin(Angle * Mathf.Deg2Rad) * Radius;
        transform.position = new Vector2( PosX,  PosY);
        Angle += Time.deltaTime * AngularSpeed;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(ObjectTagsAndLayersData.enemyTag))
        {
            collision.GetComponent<Enemy>().TakeDamage(damage);
        }


    }


}
