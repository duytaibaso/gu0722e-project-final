using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostDash : MonoBehaviour
{
    private float selfDestroyTimer = 2f;


    // Update is called once per frame
    void Update()
    {
        selfDestroyTimer -= Time.deltaTime;
        if (selfDestroyTimer < 0)
        {
            //Destroy(gameObject);
            gameObject.SetActive(false);
            selfDestroyTimer = 2f;
        }

    }
}
