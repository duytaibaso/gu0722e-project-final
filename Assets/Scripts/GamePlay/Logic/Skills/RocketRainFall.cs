using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketRainFall : MonoBehaviour
{
    //[SerializeField] GameObject ExplosionFx;
    [SerializeField] GameObject rocketSprite;
    [SerializeField] HitBox hitBox;
    [HideInInspector][SerializeField] private Vector3 normalizedDir;
    [HideInInspector] [SerializeField] private Vector3 destination;
    [HideInInspector] [SerializeField] GameManager gameManager;
    private float speed = 0.2f;
    private bool exploded = false;
    public void SetDamage( int damg)
    {
        hitBox.SetData(damg);
    }
    public void SetInit(Vector3 endPos, GameManager gameManager)
    {
        destination = endPos;
        var pos = new Vector3(endPos.x  - 10f, endPos.y  + 10f, endPos.z);
        transform.position = pos;
        normalizedDir = (endPos - pos).normalized;
        this.gameManager = gameManager;
    }

    public void ActiveSprite()
    {
        rocketSprite.SetActive(true);
    }

    private void Start()
    {
        gameObject.SetActive(false);
        hitBox.gameObject.SetActive(false);
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if (Vector3.Distance(transform.position, destination) <= 0.1f && exploded == false)
        {
            Explode();
        }
        else
        {
            transform.Translate(normalizedDir * speed, Space.Self);
        }
    }

    private void Explode()
    {
        exploded = true;
        rocketSprite.SetActive(false);
        hitBox.gameObject.SetActive(true);
        //Instantiate(ExplosionFx, transform.position, Quaternion.identity);
        gameManager.GetExplosiveManager().SpawnExplosion(transform.position);
        gameManager.GetSoundMaster().PlayAnExplosion();
        StartCoroutine(TurnOffHitBox());
    }

    IEnumerator TurnOffHitBox()
    {
        yield return new WaitForSeconds(1f);
        hitBox.gameObject.SetActive(false);
        gameObject.SetActive(false);
        exploded = false;
    }

}
