using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CountdownTimer : MonoBehaviour
{
    [SerializeField] private float timeValue;
    [SerializeField] TextMeshProUGUI timerText;
    [SerializeField] GameManager gameManager;
    [HideInInspector] [SerializeField] float ogTimeValue;
    private bool isCountDown = true;
    private float MinuteElapsed = 60f;

    public void SetTimeValue(float time, bool isCountDown)
    {
        timeValue = time;
        ogTimeValue = time;
        this.isCountDown = isCountDown;
    }

    public float Stop()
    {
        enabled = false;
        return Mathf.Abs(ogTimeValue - timeValue);
    }

    // Update is called once per frame
    void Update()
    {
        CheckMinuteElasped();
        if (isCountDown)
            if (timeValue > 0) timeValue -= Time.deltaTime; else timeValue = 0;
        else 
            timeValue += Time.deltaTime;

        DisplayTime(timeValue);
    }

    private void DisplayTime(float displayTime)
    {
        if (displayTime < 0) return;
        timerText.text = Format.TimeFormatInMinutesSeconds(displayTime);

    }
    private void CheckMinuteElasped()
    {
        MinuteElapsed -= Time.deltaTime;
        if (Mathf.FloorToInt(MinuteElapsed )<= 0)
        {
            //repott
            gameManager.Report1MinuteElasped();
            MinuteElapsed = 60f;
        }
    }




}
