using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    //[SerializeField] private float speed = 100;
    [HideInInspector][SerializeField] GameObject targetFollow;
    public void SetTargetFollow(GameObject target)
    {
        targetFollow = target;
    }

    void Update()
    {
        if (targetFollow != null)
        {
            Follow(targetFollow.transform.position);
        }
    }

    void Follow(Vector3 position)
    {
        position.z = transform.position.z;
        transform.position = position;
        //transform.position = Vector3.Slerp(transform.position, position, Time.deltaTime * speed);
    }



}
