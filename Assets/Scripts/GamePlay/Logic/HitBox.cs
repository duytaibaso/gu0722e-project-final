using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CapsuleCollider2D))]
public class HitBox : MonoBehaviour
{
    [HideInInspector] [SerializeField] int damage;
    
    public void SetData( int _damage)
    {
        damage = _damage;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(ObjectTagsAndLayersData.enemyTag))
        {
            collision.GetComponent<Enemy>().TakeDamage(damage);
        }
    }
}
