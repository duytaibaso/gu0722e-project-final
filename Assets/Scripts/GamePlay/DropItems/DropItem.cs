using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
[RequireComponent(typeof(CapsuleCollider2D))]
[RequireComponent(typeof(SpriteRenderer))]
public  class DropItem : MonoBehaviour
{
    [SerializeField] SpriteRenderer spriteRender;
    [HideInInspector] [SerializeField] IngameItemsDropSystem dropItemSys;
    [HideInInspector] [SerializeField] ItemDrop itemdropType;

    public void SetUp(IngameItemsDropSystem sys, Sprite sprite, ItemDrop type)
    {
        gameObject.SetActive(true);
        dropItemSys = sys;
        spriteRender.sprite = sprite;
        itemdropType = type;
    }

    //private void Default()
    //{

    //}

    protected  void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(ObjectTagsAndLayersData.playerTagAndLayer))
        {
            //call system
            dropItemSys.SendItemToPlayer(itemdropType);
            transform.DOMoveY(transform.position.y + 2f, 0.2f).SetEase(Ease.Linear);
            gameObject.SetActive(false);
            //Destroy(gameObject);
        }
    }

}
