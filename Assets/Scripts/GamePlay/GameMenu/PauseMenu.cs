using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] GameManager gameManager;
    [SerializeField] WarnPopUp warnPopUp;
    [SerializeField] SettingMenu setting;
    [SerializeField] Button continueButton;
    [SerializeField] Button restartButt;
    [SerializeField] Button settingButt;
    [SerializeField] Button mainTitleButton;
    [SerializeField] Button exitButton;

    [SerializeField] TextMeshProUGUI killCounterText;
    [SerializeField] TextMeshProUGUI xpCounterText;
    [SerializeField] TextMeshProUGUI goldCounterText;
    [SerializeField] TextMeshProUGUI upgradePartText;
    [SerializeField] TextMeshProUGUI damageCounter;

    // Start is called before the first frame update
    void Start()
    {
        continueButton.onClick.AddListener(OnContinueButtonClicked);
        restartButt.onClick.AddListener(OnRestartButtonClicked);
        settingButt.onClick.AddListener(OnSettingButtonClicked);
        mainTitleButton.onClick.AddListener(OnMainTitleButtonClicked);
        exitButton.onClick.AddListener(OnExitButtonClicked);
    }

    private void OnExitButtonClicked()
    {
        warnPopUp.SetVisualData("Exit the game. Proceed?", Quit);
    }
    private void Quit()
    {
        gameManager.QuitGame();
    }
    private void OnMainTitleButtonClicked()
    {
        warnPopUp.SetVisualData("Back To Main Title. Proceed?", BackToMainTitle);
    }
    private void BackToMainTitle()
    {
        gameManager.ResumeTime();
        gameManager.BackToTitleScene();
    }

    private void OnSettingButtonClicked()
    {
        setting.Show(true);
    }

    private void OnRestartButtonClicked()
    {
        gameManager.ResumeTime();
        gameManager.ReloadThisScene();
    }

    private void OnContinueButtonClicked()
    {
        gameManager.ResumeTime();
        gameObject.SetActive(false);
    }

    public void SetRecordCount(int killCounter, int dmgCounter, int gold, int upgradePart, int xp)
    {
        gameObject.SetActive(true);
        killCounterText.text = Format.FormatIntThousandMillion(killCounter);
        damageCounter.text =  Format.FormatIntThousandMillion(dmgCounter);
        goldCounterText.text =  Format.FormatIntThousandMillion(gold);
        upgradePartText.text = upgradePart.ToString();
        xpCounterText.text = xp.ToString(); Format.FormatIntThousandMillion(xp);
    }



}
