using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using Sirenix.OdinInspector;
public class GameMenuController : SerializedMonoBehaviour
{
    [SerializeField] GameManager gameManager;
    [SerializeField] PauseMenu pauseMenu;
    [SerializeField] Button switchWeaponButton;
    [SerializeField] Button meleeAttackButton;
    [SerializeField] TextMeshProUGUI bulletReporterText;
    [SerializeField] Image playerHealthBar;
    [SerializeField] TextMeshProUGUI killsCount;

    [SerializeField] Image WeaponIcon;
    [SerializeField] Image MeleeIcon;
    [SerializeField] SkillIconHUD skillIcon1;
    [SerializeField] SkillIconHUD skillIcon2;
    [SerializeField] SkillIconHUD skillIcon3;

    [SerializeField] TextMeshProUGUI missionObjectiveHeader;
    [SerializeField] TextMeshProUGUI missionObjectiveInfo;

    [SerializeField] GameObject optionalPopUp;
    [SerializeField] Button YesButton;
    [SerializeField] Button NoButton;

    [SerializeField] GameObject winnerHeader;
    [SerializeField] GameObject loserHeader;
    [SerializeField] GameObject SceneCompletePanel;
    [SerializeField] Transform rewardVisualItemHolder;
    [SerializeField] TextMeshProUGUI survivalTimeText;
    [SerializeField] TextMeshProUGUI totalKillsText;
    [SerializeField] RewardVisualItem rewardVisualItemPrefab;
    [SerializeField] Dictionary<Currency, Sprite> currencySpriteDict;
    [SerializeField] Dictionary<Currency, Sprite> currencySpriteBGDict;

    [SerializeField] Button mainTitleButton;
    [SerializeField] Button retryButton;
    [SerializeField] Button nextLevelButton;
    [SerializeField] Button settingButton;

    [HideInInspector] [SerializeField] List<SkillIconHUD> skillIcons;
    [HideInInspector] [SerializeField] Melee melee;
    [HideInInspector] [SerializeField] List<Sprite> registerWeaponIcons;

    private void Awake()
    {
        gameManager.AndroidPlatformNecessariesActive += SwitchButtonAndMeleeAttackButtonInteractActive;
        gameManager.AndroidPlatformNecessariesActive += SkillsIconInteractButtonActive;
        //gameManager.HUDIconInteractable += SwitchButtonAndMeleeAttackButtonInteractActive;
        //gameManager.HUDIconInteractable += SkillsIconInteractButtonActive;
    }
    private void SkillsIconInteractButtonActive(bool check)
    {
        skillIcon1.SetInteractiveButton(check);
        skillIcon2.SetInteractiveButton(check);
        skillIcon3.SetInteractiveButton(check);
    }
    private void SwitchButtonAndMeleeAttackButtonInteractActive(bool check)
    {
        switchWeaponButton.interactable = check;
        meleeAttackButton.interactable = check;
    }
    private void Start()
    {

        SceneCompletePanel.SetActive(false);
        switchWeaponButton.onClick.AddListener(OnSwitchWeaponIconButtonClicked);
        meleeAttackButton.onClick.AddListener(OnMeleeAttackButtonClicked);
        mainTitleButton.onClick.AddListener(OnMainTitleButtonClicked);
        retryButton.onClick.AddListener(OnRetryButtonClicked);
        nextLevelButton.onClick.AddListener(OnNextLevelButtonClicked);
        YesButton.onClick.AddListener(OnMainTitleOptionYesButtonClicked);
        NoButton.onClick.AddListener(OnMainTitleOptionNoButtonClicked);
        settingButton.onClick.AddListener(OnSettingButtonClicked);

        skillIcons.Add(skillIcon1);
        skillIcons.Add(skillIcon2);
        skillIcons.Add(skillIcon3);
        killsCount.text = "0";
    }

    private void OnSettingButtonClicked()
    {
        OpenPauseMenu();
    }

    private void OnMainTitleButtonClicked()
    {
        optionalPopUp.SetActive(true);
    }

    private void OnMainTitleOptionYesButtonClicked()
    {
        gameManager.ResumeTime();
        gameManager.BackToTitleScene();
    }
    private void OnMainTitleOptionNoButtonClicked()
    {
        optionalPopUp.SetActive(false);
    }

    private void OnRetryButtonClicked()
    {
        gameManager.ResumeTime();
        gameManager.ReloadThisScene();
    }
    private void OnNextLevelButtonClicked()
    {
        gameManager.ResumeTime();
        
        gameManager.ReloadThisScene();
    }

    public void OpenPauseMenu()
    {
        gameManager.PauseTime();
        Dictionary<Currency, int> dict = gameManager.GetAmountCurrencyDict();
        pauseMenu.SetRecordCount(gameManager.GetEnemiesKilled(), gameManager.GetDmgCounter(), dict[Currency.Gold],
            dict[Currency.UpgradePart], dict[Currency.XP]);
    }

    public void OnSceneCompleted(List<VisualCurrencyRewardItemData> datas, float survivalTime,int totalKills,  bool isVictory = true)
    {
        if (isVictory)
        {
            winnerHeader.SetActive(true);
            loserHeader.SetActive(false);
            nextLevelButton.gameObject.SetActive(MapProgressData.CurrentLevel <= MapProgressData.UnlockedLevel);
            retryButton.gameObject.SetActive(false);
            MapProgressData.CheckUnlockLevel();
        }
        else
        {
            winnerHeader.SetActive(false);
            loserHeader.SetActive(true);
            nextLevelButton.gameObject.SetActive(false);
            retryButton.gameObject.SetActive(true);
        }

        SceneCompletePanel.SetActive(true);
        VisualizeRewards(datas);

        survivalTimeText.text = "Survival Time: " + Format.TimeFormatInMinutesSeconds(survivalTime);
        totalKillsText.text = "Kills: " + totalKills.ToString();
    }

    private void VisualizeRewards(List<VisualCurrencyRewardItemData> datas)
    {
        for (int i = 0; i < datas.Count; i++)
        {
            rewardVisualItemPrefab.SetVisualData(currencySpriteBGDict[datas[i].rewardType], currencySpriteDict[datas[i].rewardType], datas[i].amount);
            Instantiate(rewardVisualItemPrefab.gameObject, rewardVisualItemHolder);
        }
    }


    public void SetKillsCountText(int count)
    {
        killsCount.text = count.ToString();
    }
    
    public void SetMissionObjectiveTextInfo(string header, string info)
    {
        missionObjectiveHeader.text = header;
        missionObjectiveInfo.text = info;
    }

    public void SetPlayerHealth(int currentHp)
    {
        float value = (float)currentHp / (float)PlayerData.maxHP;
        playerHealthBar.fillAmount = value;
    }
    public void SetGunssIcon(Sprite primary, Sprite secondary)
    {
        registerWeaponIcons.Add(primary);
        registerWeaponIcons.Add(secondary);
        WeaponIcon.sprite = primary;
    }
    public void SetMeleeIcon(Sprite melee)
    {
        MeleeIcon.sprite = melee;
    }

    public void SetSkillIconSpritesHUD(SkillData[] datas, Action[] skillActiveAction)
    {
        skillIcon1.SetSkillIconData(datas[0].icon, datas[0].cooldown, skillActiveAction[0]);
        skillIcon2.SetSkillIconData(datas[1].icon, datas[1].cooldown, skillActiveAction[1]);
        skillIcon3.SetSkillIconData(datas[2].icon, datas[2].cooldown, skillActiveAction[2]);
    }
    public void SetMeleeAction(Melee melee)
    {
        this.melee = melee;
    }
    
    private void OnMeleeAttackButtonClicked()
    {
        melee.SetUIClicked();
    }

    private void OnSwitchWeaponIconButtonClicked()
    {
        gameManager.HUDSwitchGunButtonTapped();
        SwitchGunIcon();
    }
    
    public void SwitchGunIcon()
    {
        for (int i = 0; i < registerWeaponIcons.Count; i++)
        {
            if (registerWeaponIcons[i] == WeaponIcon.sprite) continue;
            WeaponIcon.sprite = registerWeaponIcons[i]; break;
        }
    }

    public void EnactSkillIconCoolDown(int skillIndex)
    {
        skillIcons[skillIndex].OnCoolDown();
    }

    public void ReceiveCurrentBulletOfCurrentGunInfo(int ammoMag, int ammoSupply)
    {
        bulletReporterText.text = ammoMag.ToString() + "/" + ammoSupply.ToString();
    }

}
