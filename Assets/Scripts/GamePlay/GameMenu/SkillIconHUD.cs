using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;


public class SkillIconHUD : MonoBehaviour
{
    [SerializeField] Button SelfButton;
    [SerializeField] Image cooldownImage;
    [SerializeField] Image skillIcon;

    [HideInInspector] [SerializeField] float cooldown;
    [HideInInspector] [SerializeField] float resetCoolDownTimer;

    [HideInInspector] [SerializeField] Action onSkillActivated;
    private bool isOnCoolDown = false;
    
    public void SetInteractiveButton(bool check)
    {
        SelfButton.interactable = check;
    }

    public void SetSkillIconData(Sprite icon, float cooldown, Action skillActiveAction)
    {
        resetCoolDownTimer = cooldown;
        skillIcon.sprite = icon;
        this.cooldown = cooldown;
        onSkillActivated += skillActiveAction;
    }
    private void Start()
    {
        cooldownImage.fillAmount = 0f;
        SelfButton.onClick.AddListener(OnSelfButtonClicked);
    }
    private void OnSelfButtonClicked()
    {
        onSkillActivated?.Invoke();
    }
    // Update is called once per frame
    void Update()
    {
        if (isOnCoolDown)
        {
            cooldown -= Time.deltaTime;
            cooldownImage.fillAmount = cooldown / resetCoolDownTimer;
            if (cooldown <= 0)
            {
                cooldown = resetCoolDownTimer;
                isOnCoolDown = false;
            }
        }
    }
    public void OnCoolDown()
    {

        isOnCoolDown = true;
        cooldownImage.fillAmount = 1f;
    }

}
