using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using Sirenix.OdinInspector;
public class LevelManager : SerializedMonoBehaviour
{
    [SerializeField] LevelAssetListData levelAssetListData;
    [SerializeField] SpriteRenderer background;
    [SerializeField] Transform playerLocationRepresentative;
    [SerializeField] Transform enemySpanersContainer;
    [SerializeField] EnemySpawner locationSpawnerPrefab;

    public void Load(Transform player)
    {
        player.transform.position = levelAssetListData.GetPlayerPos(MapProgressData.CurrentLevel) ;
        background.sprite = levelAssetListData.GetBGSprite(MapProgressData.CurrentLevel );

        if (levelAssetListData.GetSpawnerData(MapProgressData.CurrentLevel) == null) return;
        foreach (var enemyData in levelAssetListData.GetSpawnerData(MapProgressData.CurrentLevel))
        {
            locationSpawnerPrefab.SetSpawnerData(enemyData.amount, enemyData.rate);
            Instantiate(locationSpawnerPrefab.gameObject, enemyData.position, Quaternion.identity, enemySpanersContainer);
        }
    }
    
    [Button(ButtonHeight = 50)]
    public void Save()
    {
        LevelAsset newAsset = new LevelAsset();

        var spawners = FindObjectsOfType<EnemySpawner>();
        for (int i = 0; i < spawners.Length; i++)
        {
            if (spawners[i].GetTypeOfSpawn() == TypeOfSpawn.AroundCamera) continue;
            Tuple<int, float> tuple = spawners[i].GetSpawnerData();
            newAsset.enemySpawnerDatas.Add(new EnemySpawnerData()
            {
                amount = tuple.Item1,
                rate = tuple.Item2,
                position = spawners[i].transform.position
            }); ;
        }
        newAsset.playerPosRepresentative = playerLocationRepresentative.position;
        newAsset.backgroundSprite = background.sprite;

        levelAssetListData.AddNewLevel(newAsset);
    }



}
