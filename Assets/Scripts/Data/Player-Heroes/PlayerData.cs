﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

public static class PlayerData
{
    //Có thể sẽ cho playerData vào heroesdataStateList để làm attributes cho các heroes khác nhau
    const int defaultMaxHP = 100;
    const int defaultSpeed = 5;

    

    public static int maxHP
    {
        get => PlayerPrefs.GetInt("maxHP", defaultMaxHP);
        private set => PlayerPrefs.SetInt("maxHP", value);
    }
    public static int speed
    {
        get => PlayerPrefs.GetInt("speed", defaultSpeed);
        private set => PlayerPrefs.SetInt("speed", value);
    }
    //Set trong equipment
    

    public static void SetMaxHP(int _maxHP)
    {
        maxHP = _maxHP;
    }
    public static void SetSpeed(int _speed)
    {
        speed = _speed;
    }

    //[MenuItem("Player Data/Reset Player Data")]
    //public static void ResetPlayerData()
    //{
    //    maxHP = defaultMaxHP;
    //    speed = defaultSpeed;
    //}
}










