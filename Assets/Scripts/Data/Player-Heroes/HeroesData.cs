using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "HeroesData", menuName = "HeroesData")]
public class HeroesData : SerializedScriptableObject
{
    public const string ResourcesPath = "Heroes-Characters/";
    const string HeroesIconImageResourcePath = "Sprites/Mine/Icons/Items/Heroes/";
    const string HeroesBodySpriteResourcePath = "Sprites/Mine/HeroeSprites/";

    public int heroesEquippedEnumInt
    {
        get => PlayerPrefs.GetInt("heroesEquippedEnumInt", 0);
        private set => PlayerPrefs.SetInt("heroesEquippedEnumInt", value);
    }

    [SerializeField] Dictionary<Heroes, HeroesStateList> heroesStateLists;

    public int GetHeroesStateTotal()
    {
        return heroesStateLists.Count;
    }
    
    private Dictionary<Heroes, string> HeroesResourceStringDict = new Dictionary<Heroes, string>()
    {
        {Heroes.Protagonist, "Protagonist"},
        {Heroes.Maiden, "Maiden"},
    };
    private void Awake()
    {
        PlayerData.SetMaxHP(heroesStateLists[(Heroes)heroesEquippedEnumInt].baseHealth);
        PlayerData.SetSpeed(heroesStateLists[(Heroes)heroesEquippedEnumInt].speed);
    }
    public Dictionary<Heroes, string> GetHeroesResourceStringDict()
    {
        return HeroesResourceStringDict;
    }

    public void SetEquipHeroes(int enumInt)
    {
        if (enumInt > Enum.GetNames(typeof(Heroes)).Length)
        {
            Debug.LogError("Co gi sai sai");
            return;
        }
        heroesEquippedEnumInt = enumInt;
    }

    public Sprite GetHeroesIcon(Heroes hero)
    {
        return Resources.Load<Sprite>(HeroesIconImageResourcePath + heroesStateLists[hero].iconResourceName);
    }
    public Sprite GetHeroesBodySprite(Heroes hero)
    {
        return Resources.Load<Sprite>(HeroesBodySpriteResourcePath + heroesStateLists[hero].bodySpriteResourceName);
    }

}
[Serializable]
public class HeroesStateList
{
    public string iconResourceName;
    public string bodySpriteResourceName;
    public int speed;
    public int baseHealth;

}


public enum Heroes
{
    Protagonist = 0 , Maiden = 1
}

