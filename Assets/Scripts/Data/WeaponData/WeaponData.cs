﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "WeaponData", menuName = "Weapon", order = 1)]
public class WeaponData : SerializedScriptableObject
{
    public const string GunWeaponPrefabsFileResourcePath = "WeaponPrefab/GunWeaponPrefabs/";
    public const string MeleeWeaponPrefabsFileResourcePath = "WeaponPrefab/MeleeWeaponPrefabs/";
    const string GunIngameIconResourcePath = "Sprites/_AssetsStore/FPS Icons Pack/Orange Theme/";
    const string GunInventoryIconResourcePath = "Sprites/Mine/Icons/Items/";
    const string MeleeIngameIconResourcePath = "Sprites/Mine/Icons/";
    const string MeleeInventoryIconResourcePath = "Sprites/_Survivor/Sprite/";
    const string PngExtension = ".png";
    const string IngameGunWeaponSpriteSheet = "Orange theme spritesheet 2";
    const string InventoryGunWeaponSpriteSheet1 = "1";
    const string InventoryGunWeaponSpriteSheet2 = "2";

    [SerializeField] private Dictionary<GunWeaponType, GunWeaponInfo> weaponListInfo;
    [SerializeField] private Dictionary<MeleeWeaponType, MeleeWeaponInfo> meleeWeaponInfo;
    //[Tooltip("int dau la level, int sau la Gold price")]
    //[SerializeField] private Dictionary<int, int> levelUpGoldCostDict;

    public int GetLevelUpGoldCostDict(int level)
    {
        //return levelUpGoldCostDict[level];
        return 1000 + level * 2000;
    }

    public int GetTotalGoldRequiredForLevelUp(int startLevel, int endLevel)
    {
        int price = 0;
        for (int i = startLevel; i <= endLevel; i++)
        {
            price += GetLevelUpGoldCostDict(i);
        }
        return price;
    }
    #region WeaponDamage
    public int GetGunWeaponDamage(GunWeaponType type, Rarity rarity, int level)
    {
        int baseDamg = weaponListInfo[type].weaponAttributes.damage;
        int total = baseDamg + RarityBaseDamgGrowthFormula(rarity) + LevelBaseDamgGrowthFormula(level) + GetGunItemModTotalPlusDamage(type, rarity);
        return total + (int)(total * GetGunItemAttackPercent(type, rarity));
    }

    public int GetMeleeWeaponDamage(MeleeWeaponType type, Rarity rarity, int level)
    {
        int baseDamg = meleeWeaponInfo[type].damage;
        int total = baseDamg + RarityBaseDamgGrowthFormula(rarity) + LevelBaseDamgGrowthFormula(level) + GetMeleeItemModTotalPlusDamage(type, rarity);
        return total + (int)(total * GetMeleeItemAttackPercent(type, rarity));
    }

    private int RarityBaseDamgGrowthFormula(Rarity rarity)
    {
        return (int)Mathf.Pow((int)rarity, 2f);
    }
    private int LevelBaseDamgGrowthFormula(int level)
    {
        return (int)Mathf.Pow(level, 1.8f) + 2 * level;
    }
    public int GetGunItemModTotalPlusDamage(GunWeaponType gunType, Rarity rarity)
    {
        int dmg = 0;
        for (int i = 0; i <= (int)rarity; i++)
        {
            Tuple<WeaponModUpgradeStatType, int> tuple = GetGunModStatType(gunType, (Rarity)i);
            if (tuple.Item1 != WeaponModUpgradeStatType.IncreaseAttack) continue;
            dmg += tuple.Item2;
        }
        return dmg;
    }
    public int GetMeleeItemModTotalPlusDamage(MeleeWeaponType type, Rarity rarity)
    {
        int dmg = 0;
        for (int i = 0; i <= (int)rarity; i++)
        {
            Tuple<WeaponModUpgradeStatType, int> tuple = GetMeleeModStatType(type, (Rarity)i);
            if (tuple.Item1 != WeaponModUpgradeStatType.IncreaseAttack) continue;
            dmg += tuple.Item2;
        }
        return dmg;
    }

    public float GetGunItemAttackPercent(GunWeaponType gunType, Rarity rarity)
    {
        float percent = 0;
        for (int i = 0; i <= (int)rarity; i++)
        {
            Tuple<WeaponModUpgradeStatType, int> tuple = GetGunModStatType(gunType, (Rarity)i);
            if (tuple.Item1 != WeaponModUpgradeStatType.IncreaseAttackByPercent) continue;
            percent += tuple.Item2;
        }
        return percent;
    }
    public float GetMeleeItemAttackPercent(MeleeWeaponType type, Rarity rarity)
    {
        int percent = 0;
        for (int i = 0; i <= (int)rarity; i++)
        {
            Tuple<WeaponModUpgradeStatType, int> tuple = GetMeleeModStatType(type, (Rarity)i);
            if (tuple.Item1 != WeaponModUpgradeStatType.IncreaseAttackByPercent) continue;
            percent += tuple.Item2;
        }
        return percent;
    }
    #endregion WeaponDamage

    #region HP

    #endregion HP

    public int GetWeaponMaxLevel(Rarity rarity)
    {
        return 20 + (int)rarity * 20;
    }

    public Tuple<WeaponModUpgradeStatType, int> GetGunItemMod(GunWeaponType type, Rarity rarity) { return weaponListInfo[type].itemMod[rarity]; }

    private Dictionary<GunWeaponType, string> GunWeaponResourcesPrefabStringDict = new Dictionary<GunWeaponType, string>()
    {
        {GunWeaponType.AK47, "AK47" },
        {GunWeaponType.Glock, "Glock" },
        {GunWeaponType.RPG, "RPG" },
        {GunWeaponType.Revolver, "Revolver" }
    };

    private Dictionary<MeleeWeaponType, string> MeleeWeaponResourcesPrefabStringDict = new Dictionary<MeleeWeaponType, string>()
    {
        {MeleeWeaponType.Katana, "Katana" },
        {MeleeWeaponType.BaseBall, "BaseBall" },
    };

    private Dictionary<WeaponModUpgradeStatType, string> WeaponUpgradeStatTextDict = new Dictionary<WeaponModUpgradeStatType, string>()
    {
        {WeaponModUpgradeStatType.IncreaseAttack, "ATK increase by" },
        {WeaponModUpgradeStatType.IncreaseHp, "HP increase by" },
        {WeaponModUpgradeStatType.PiercingBullet, "Piercing bullet upgrade" },
        {WeaponModUpgradeStatType.IncreaseAttackByPercent, "ATK increase by percent of" },
        {WeaponModUpgradeStatType.IncreaseHpByPercent, "HP increase by percent of" },
    };
    public string GetWeaponInUpgradeStatText(WeaponModUpgradeStatType type) { return WeaponUpgradeStatTextDict[type]; }
    
    public List<Tuple<WeaponModUpgradeStatType, int>> GetGunStatTypeAndValue(GunWeaponType type)
    {
        List<Tuple<WeaponModUpgradeStatType, int>> list = new List<Tuple<WeaponModUpgradeStatType, int>>();
        foreach (var keyValuePairs in weaponListInfo[type].itemMod.OrderBy(x => x.Key).ToDictionary(x => x.Key, x => x.Value))
        {
            list.Add(keyValuePairs.Value);
        }
        return list; 
    }

    public List<Tuple<WeaponModUpgradeStatType, int>> GetMeleeStatTypeAndValue(MeleeWeaponType type)
    {
        List<Tuple<WeaponModUpgradeStatType, int>> list = new List<Tuple<WeaponModUpgradeStatType, int>>();
        foreach (var keyValuePairs in meleeWeaponInfo[type].itemMod.OrderBy(x => x.Key).ToDictionary(x => x.Key, x => x.Value))
        {
            list.Add(keyValuePairs.Value);
        }
        return list;
    }

    public string GetGunWeaponResourcesPrefabStringPath(GunWeaponType type)
    {
        return GunWeaponResourcesPrefabStringDict[type];
    }

    public string GetMeleeWeaponResourcesPrefabStringPath(MeleeWeaponType type)
    {
        return MeleeWeaponResourcesPrefabStringDict[type];
    }

    #region LoadSprite
    public Sprite GetGunItemIconIngame(GunWeaponType type)
    {
        Sprite[] listSprite = Resources.LoadAll<Sprite>(GunIngameIconResourcePath + IngameGunWeaponSpriteSheet);
        for (int i = 0; i < listSprite.Length; i++)
        {
            if (weaponListInfo[type].iconInGame == listSprite[i].name) return listSprite[i];
        }
        Debug.LogError("null");
        return null;
    }


    public Sprite GetGunItemIconInInventory(GunWeaponType type)
    {
        Sprite[] listSprite = Resources.LoadAll<Sprite>(GunInventoryIconResourcePath + InventoryGunWeaponSpriteSheet1);
        Sprite[] listSprite2 = Resources.LoadAll<Sprite>(GunInventoryIconResourcePath + InventoryGunWeaponSpriteSheet2);
        List<Sprite> list = listSprite.ToList();
        list.AddRange(listSprite2);
        for (int i = 0; i < list.Count; i++)
        {
            if (weaponListInfo[type].iconInInventory == list[i].name) return list[i];
        }
        Debug.LogError("null");
        return null;
        
    }
    public Sprite GetMeleeItemIconIngame(MeleeWeaponType type)
    {
        return Resources.Load<Sprite>(MeleeIngameIconResourcePath + meleeWeaponInfo[type].iconInGame);
    }
    public Sprite GetMeleeItemIconInInventory(MeleeWeaponType type)
    {
        return Resources.Load < Sprite > (MeleeInventoryIconResourcePath + meleeWeaponInfo[type].iconInInventory);
    }
    #endregion

    public string GetGunDescription(GunWeaponType type)
    {
        return weaponListInfo[type].description;
    }
    public string GetMeleeDescription(MeleeWeaponType type)
    {
        return meleeWeaponInfo[type].description;
    }

    public GunWeaponInfo GetGunWeaponInfo(GunWeaponType type)
    {
        return weaponListInfo[type];
    }

    public MeleeWeaponInfo GetMeleeWeaponInfo(MeleeWeaponType type)
    {
        return meleeWeaponInfo[type];
    }
    public float GetGunBulletSpeed(GunWeaponType _weapon)
    {
        return weaponListInfo[_weapon].weaponAttributes.bulletSpeed;
    }
    public float AccuracyToSpreadAngleFormula(float weaponAccuracy)
    {
        return (1 - Mathf.Clamp(weaponAccuracy, 0f, 1f)) * 10f;
    }

    private Dictionary<WeaponModUpgradeStatType, BulletType> ItemModToBulletTypeTranslateDict = new Dictionary<WeaponModUpgradeStatType, BulletType>()
    {
        {WeaponModUpgradeStatType.PiercingBullet, BulletType.Piercing },
    };
    /// <summary>
    /// mac dinh se la BulletType.Normal neu ko co mod gi lien quan
    /// </summary>
    /// <param name="modType"></param>
    /// <returns></returns>
    public BulletType GetBulletType(WeaponModUpgradeStatType modType)
    {
        if (ItemModToBulletTypeTranslateDict.ContainsKey(modType) == false) return BulletType.Normal;
        return ItemModToBulletTypeTranslateDict[modType];
    }
    /// <summary>
    /// int la value chi? so'
    /// </summary>
    /// <param name="gunType"></param>
    /// <param name="rarity"></param>
    /// <returns></returns>
    public Tuple<WeaponModUpgradeStatType,int> GetGunModStatType(GunWeaponType gunType, Rarity rarity)
    {
        if (weaponListInfo[gunType].itemMod.ContainsKey(rarity) == false) return Tuple.Create(WeaponModUpgradeStatType.IncreaseAttack, 0);
        return weaponListInfo[gunType].itemMod[rarity];
    }
    public Tuple<WeaponModUpgradeStatType, int> GetMeleeModStatType(MeleeWeaponType type, Rarity rarity)
    {
        if (meleeWeaponInfo[type].itemMod.ContainsKey(rarity) == false) return Tuple.Create(WeaponModUpgradeStatType.IncreaseAttack, 0);
        return meleeWeaponInfo[type].itemMod[rarity];
    }
    
    public int GetGunItemModTotalPlusHp(GunWeaponType gunType, Rarity rarity)
    {
        int hp = 0;
        for (int i = 0; i <= (int)rarity; i++)
        {
            Tuple<WeaponModUpgradeStatType, int> tuple = GetGunModStatType(gunType, (Rarity)i);
            if (tuple.Item1 != WeaponModUpgradeStatType.IncreaseHp) continue;
            hp += tuple.Item2;
        }
        return hp;
    }
    public int GetMeleeItemModTotalPlusHp(MeleeWeaponType type, Rarity rarity)
    {
        int hp = 0;
        for (int i = 0; i <= (int)rarity; i++)
        {
            Tuple<WeaponModUpgradeStatType, int> tuple = GetMeleeModStatType(type, (Rarity)i);
            if (tuple.Item1 != WeaponModUpgradeStatType.IncreaseHp) continue;
            hp += GetMeleeModStatType(type, rarity).Item2;
        }
        return hp;
    }
}

[Serializable]
public struct MeleeWeaponInfo
{
    public Dictionary<Rarity, Tuple<WeaponModUpgradeStatType, int>> itemMod;
    public string iconInGame;
    public string iconInInventory;
    public string weaponName;
    public string description;
    //[Tooltip("Phan tram(%) animation speed giam xuong")]
    public float attackSpeed;
    public int damage;
}

[Serializable]
public struct GunWeaponInfo
{
    public Dictionary<Rarity, Tuple< WeaponModUpgradeStatType, int>> itemMod;
    public string iconInGame;
    public string iconInInventory;
    public string weaponName;
    public string description;
    public WeaponAttributes weaponAttributes;

}

public struct WeaponInfo{}

[Serializable]
public struct WeaponAttributes
{
    public int fireratePerSecond;
    [Tooltip("Base Damg")]
    public int damage;
    public float bulletSpeed;
    [Tooltip("Reload time theo float")]
    public float reloadTime;
    public int ammoMagazine;
    public int ammoSupply;
    [Tooltip("Từ 0 ~ 1 theo phần trăm đã chia cho 100 ")]
    /// <summary>
    /// Từ 0 ~ 1 theo phần trăm đã chia cho 100 
    /// </summary>
    public float accuracy;
}

public enum GunWeaponType
{
    Revolver , Glock, AK47 , RPG , 
}
public enum MeleeWeaponType
{
    BaseBall, Katana
}

public enum WeaponModUpgradeStatType
{
    IncreaseAttack, IncreaseHp, PiercingBullet, IncreaseAttackByPercent, IncreaseHpByPercent
}
public enum BulletType
{
    Normal, Piercing, Explosive
}
