﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;

[CreateAssetMenu(fileName = "AchievementData", menuName = "AchievementData")]
public class AchievementData : SerializedScriptableObject
{
    [TableList]
    [Tooltip("Tuple Item1.Item2 là mức value cần vượt để nhận reward Tuple Item2")]
    [SerializeField] List<Tuple<Tuple<AchievementType, int>, AchievementReward>> AchievementObjectiveList;

    public int GetAchievementObjectiveListCount()
    {
        return AchievementObjectiveList.Count;
    }
    
    private Dictionary<AchievementType, string> AchievementObjectiveText = new Dictionary<AchievementType, string>()
    {
        {AchievementType.HighGold , "{0} Gold earned" },
        {AchievementType.HighKills, "{0} Kills" },
        {AchievementType.HighSurvivalTimeInSeconds, "Total time survived ingame reachs to {0} minute(s)" }
    };
    public string GetAchievementObjectiveText(int indx)
    {
        return AchievementObjectiveText[AchievementObjectiveList[indx].Item1.Item1];
    }

    public bool CheckCanClaimCondition(int indx)
    {
        switch (AchievementObjectiveList[indx].Item1.Item1)
        {
            case AchievementType.HighGold:
                return UserData.totalGoldEarned >= AchievementObjectiveList[indx].Item1.Item2;
            case AchievementType.HighKills:
                return UserData.totalKills >= AchievementObjectiveList[indx].Item1.Item2;
            // AchievementType.HighTimeSurvival
            default:
                return UserData.totalTimeSurvive >= AchievementObjectiveList[indx].Item1.Item2;
        }
    }
    public AchievementType GetAchievementType(int indx)
    {
        return AchievementObjectiveList[indx].Item1.Item1;
    }
    public int GetValue(int indx)
    {
        //if (AchievementObjectiveList[indx].Item1.Item1 == AchievementType.HighSurvivalTimeInSeconds && forStringMinutes)
        if (AchievementObjectiveList[indx].Item1.Item1 == AchievementType.HighSurvivalTimeInSeconds)
        {
            return (int)AchievementObjectiveList[indx].Item1.Item2 / 3600;
        }

        return AchievementObjectiveList[indx].Item1.Item2;
    }

    public List<int> GetValueListOf(AchievementType type)
    {
        List<int> newList = new List<int>();
        for (int i = 0; i < AchievementObjectiveList.Count; i++)
        {
            if (AchievementObjectiveList[i].Item1.Item1 == type)
            {
                newList.Add(AchievementObjectiveList[i].Item1.Item2);
            }
        }
        return newList;
    }
    /// <summary>
    /// Tuple bool la IsMelee
    /// </summary>
    /// <param name="index"></param>
    /// <returns></returns>
    public Tuple<ItemBase, bool> GetRewardItemWhetherGunOrMelee(int index)
    {
        ItemBase item = AchievementObjectiveList[index].Item2.item;
        if (item == null) return (item, false).ToTuple();
        if (item.GetType() == typeof(GunItem))
        {
            return (item, false).ToTuple();
        }
        else
        {
            return (item, true).ToTuple();
        }
    }
    /// <summary>
    /// int dau la gold, int sau la xp
    /// </summary>
    /// <param name="index"></param>
    /// <returns></returns>
    public Vector2Int GetRewardGoldAndExp(int index)
    {
        return new Vector2Int (AchievementObjectiveList[index].Item2.gold, AchievementObjectiveList[index].Item2.xp);
    }
    public string GetAchievementText(AchievementType type)
    {
        return AchievementObjectiveText[type];
    }
}

public struct AchievementReward
{
    public int gold;
    public int xp;
    public ItemBase item;
}

public enum AchievementType
{
    HighKills, HighGold, HighSurvivalTimeInSeconds
}