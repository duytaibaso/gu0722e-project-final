using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;
[CreateAssetMenu(fileName = "MapLevelEnemySpawnData", menuName = "EnemySpawnData")]
public class MapLevelDataSpawn : SerializedScriptableObject
{
    [SerializeField] List<EnemySpawnData> mapLevelsEnemySpawnData;
    [SerializeField] List<Dictionary<EnemyType, int>> enemiesSpawnRateDict;

    public int GetEnemySpawnRate(EnemyType type)
    {
        return enemiesSpawnRateDict[MapProgressData.CurrentLevel - 1][type];
    }

    public List<EnemySpawnData> GetMapLevelsEnemySpawnData()
    {
        return mapLevelsEnemySpawnData;
    }

    public int GetNumbersOfEnemySpawnInSpecificMinute(int minute, int mapLevel)
    {
        return mapLevelsEnemySpawnData[mapLevel - 1].enemySpawnNumbersEachMinute[minute];
    }

    public float GetTimerOfEachSpawn(int minute, int mapLevel)
    {
        return mapLevelsEnemySpawnData[mapLevel - 1].timerEachSpawn[minute];
    }

    public EnemyAttributes GetEnemyAttributesPlusEachMinute(int minute, int mapLevel)
    {
        return mapLevelsEnemySpawnData[mapLevel - 1].enemyDataPlusEachMinute[minute];
    }

}


public struct EnemySpawnData
{
    public List<int> enemySpawnNumbersEachMinute;
    public List<float> timerEachSpawn;
    public List<EnemyAttributes> enemyDataPlusEachMinute;
    //enemy type enum
    //special boss spawn? enum
    //

}