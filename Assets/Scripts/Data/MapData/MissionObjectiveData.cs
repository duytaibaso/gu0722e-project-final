using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
[CreateAssetMenu(fileName = "ObjectiveData", menuName = "MissionObjectiveData")]
public class MissionObjectiveData : ScriptableObject
{
    [SerializeField] List<ObjectivesOrganizer> MapLevelsObjectivesOrganize;

    private Dictionary<MissionObjectiveType, Tuple<string, string>> objTypeDesc = new Dictionary<MissionObjectiveType, Tuple<string, string>>()
    {
        {MissionObjectiveType.ClearEnemy, ("Take them down", "You are surrounded. Open the blood PATH!!!").ToTuple() },
        {MissionObjectiveType.SurviveByTime, ("Survival", "Wait for a chance to escape").ToTuple() },
    };
    public MissionObjectiveType GetObjType(int mapLevel)
    {
        return MapLevelsObjectivesOrganize[mapLevel - 1].objType;
    }
    public int GetObjCondition(int mapLevel)
    {
        return MapLevelsObjectivesOrganize[mapLevel - 1].condition;
    }
    public Tuple<string, string> GetObjTypeDesc(MissionObjectiveType type)
    {
        return objTypeDesc[type];
    }

}

[Serializable]
public struct ObjectivesOrganizer
{
    public MissionObjectiveType objType;
    public int condition;
}

public enum MissionObjectiveType
{
    ClearEnemy, SurviveByTime
}