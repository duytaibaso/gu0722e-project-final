using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MapProgressData 
{
    public const string gameplaySceneName = "GamePlay";
    public const string maintitleSceneName = "MainTitle";
    public const string missionImageResourcePath = "texture/missionicon/";
    public const string missionFormPath = "Mission_";
    public const int MaxLevel = 5;

    public static int HomeMapVisualSelectedID
    {
        get { return PlayerPrefs.GetInt("HomeMapVisualSelectedID", 0); }
        private set { PlayerPrefs.SetInt("HomeMapVisualSelectedID", value); }
    }

    public static void SetHomeMapVisualSelectedID(int id)
    {
        if (id + 1 > MaxLevel)
        {
            Debug.LogError("id " + id + " not exist");
            return;
        }
        HomeMapVisualSelectedID = id;
    }

    public static int CurrentLevel
    {
        get { return PlayerPrefs.GetInt("CurrentLevel", 1); }
        private set { PlayerPrefs.SetInt("CurrentLevel", value); }
    }

    public static int UnlockedLevel
    {
        get { return PlayerPrefs.GetInt("UnlockedLevel", 1); }
        private set { PlayerPrefs.SetInt("UnlockedLevel", value); }
    }

    private static bool CheckLastestLevel()
    {
        return CurrentLevel == UnlockedLevel;
    }

    public static void CheckUnlockLevel()
    {
        if (CheckLastestLevel() == false) return;
        if (UnlockedLevel < MaxLevel)
        UnlockedLevel++;
        CurrentLevel = UnlockedLevel;
    }



    public static void SetCurrentLevel(int level)
    {
        CurrentLevel = level;
    }




}
