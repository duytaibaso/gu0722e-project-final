using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorParameterData
{
    public static string GunNeedReload = "GunNeedReload";
    public static string GunReloadCompleted = "GunReloadCompleted";

    public static string IdleEmpty = "Idle(Empty)";
    public static string SwordSlash = "SwordSlash";
    public static string MeleeSlash = "MeleeSlash";
    public static string MeleeSlashCompleted = "MeleeSlashCompleted";
    public static string Explode = "Explode";
    public static string ActiveForceFieldLightning = "ActiveForceFieldLightning";
    public static string Die = "Die";
    public static string NormalAttack = "NormalAttack";
    public static string Idle = "Idle";

}
