using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System.IO;

public class JSONLizer
{
    

    public static void SerializeList<T>(object obj, string jsonPath)
    {
        string jsonString = JsonConvert.SerializeObject(obj as List<T>, Formatting.Indented);
        File.WriteAllText(jsonPath, jsonString);
    }

    public static void Serialize(object obj, string path)
    {
        string jsonString = JsonConvert.SerializeObject(obj);
        File.WriteAllText(path, jsonString);
    }

    public static List<T> DeserializeList<T>(string jsonPath)
    {
        return JsonConvert.DeserializeObject<List<T>>(File.ReadAllText(jsonPath));
    }

    protected string SerializeObject<T>(object obj)
    {
        return JsonConvert.SerializeObject(obj as List<T>, Formatting.Indented);
    }

    protected List<T> DeserializeObject<T>(string path)
    {
        return JsonConvert.DeserializeObject<List<T>>(path);
    }
}
