using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "EnemyData", menuName = "EnemyData")]
public class EnemyData: ScriptableObject
{
    public string enemyName;
    public string description;
    public EnemyAttributes attributes;
}

[Serializable]
public struct EnemyAttributes
{
    public float cooldownAttack;
    public int damage;
    public int speed;
    public int hp;
}
//public struct EnemyType
//{

//}
