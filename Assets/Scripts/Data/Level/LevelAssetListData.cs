using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;
[Serializable]
[CreateAssetMenu(fileName = "LevelAssetListData ", menuName = "Game/LevelAssetListData", order = 1)]
public class LevelAssetListData : SerializedScriptableObject
{
    [SerializeField] List<LevelAsset> levelAssetListData;


    public Sprite GetBGSprite(int maplevel)
    {
        return levelAssetListData[maplevel - 1].backgroundSprite;
    }

    public Vector2 GetPlayerPos(int maplevel)
    {
        return levelAssetListData[maplevel - 1].playerPosRepresentative;
    }

    public List<EnemySpawnerData> GetSpawnerData(int maplevel)
    {
        return levelAssetListData[maplevel - 1].enemySpawnerDatas;
    }

    public void AddNewLevel(LevelAsset asset)
    {
        levelAssetListData.Add(asset);
    }

}

public class LevelAsset
{
    public Vector2 playerPosRepresentative;
    public Sprite backgroundSprite;
    public List<EnemySpawnerData> enemySpawnerDatas;
    //obstacles, walls,






}
