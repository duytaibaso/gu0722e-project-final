using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
public static class UserData
{

    #region UserAccountLevelRewardDictionary
    private static string LevelRewardDataSavePath = Application.persistentDataPath + "LevelRewardDataSave.txt";

    /// <summary>
    /// int dau` la user account level, int sau la value currency
    /// </summary>
    private static Dictionary<int, Dictionary<Currency, int>> UserAccountLevelRewardDict = new Dictionary<int, Dictionary<Currency, int>>()
    {
        {2 , new Dictionary<Currency, int>(){ { Currency.Gold, 1000 }, } },
        {3 , new Dictionary<Currency, int>(){ { Currency.Gold, 2000 }, { Currency.UpgradePart, 5 }, } },
        {4 , new Dictionary<Currency, int>(){ { Currency.Gold, 3000 }, { Currency.UpgradePart, 5 } } },
        {5 , new Dictionary<Currency, int>(){ { Currency.Gold, 4000 }, { Currency.UpgradePart, 5 } } },
        {6 , new Dictionary<Currency, int>(){ { Currency.Gold, 5000 }, { Currency.UpgradePart, 5 } } },
        {7 , new Dictionary<Currency, int>(){ { Currency.Gold, 6000 }, { Currency.UpgradePart, 5 } } },
        {8 , new Dictionary<Currency, int>(){ { Currency.Gold, 7000 }, { Currency.UpgradePart, 5 } } },
        {9 , new Dictionary<Currency, int>(){ { Currency.Gold, 8000 }, { Currency.UpgradePart, 5 } } },
        {10 , new Dictionary<Currency, int>(){ { Currency.Gold, 9000 }, { Currency.UpgradePart, 5 } } },
        {11 , new Dictionary<Currency, int>(){ { Currency.Gold, 10000 }, { Currency.UpgradePart, 5 } } },
        {12 , new Dictionary<Currency, int>(){ { Currency.Gold, 10000 }, { Currency.UpgradePart, 5 } } },
        {13 , new Dictionary<Currency, int>(){ { Currency.Gold, 10000 }, { Currency.UpgradePart, 5 } } },
        {14 , new Dictionary<Currency, int>(){ { Currency.Gold, 10000 }, { Currency.UpgradePart, 5 } } },
        {15 , new Dictionary<Currency, int>(){ { Currency.Gold, 10000 }, { Currency.UpgradePart, 10 } } },
        {16 , new Dictionary<Currency, int>(){ { Currency.Gold, 10000}, { Currency.UpgradePart, 10 } } },
        {17 , new Dictionary<Currency, int>(){ { Currency.Gold, 10000 }, { Currency.UpgradePart, 10 } } },
        {18 , new Dictionary<Currency, int>(){ { Currency.Gold, 10000 }, { Currency.UpgradePart, 10 } } },
        {19 , new Dictionary<Currency, int>(){ { Currency.Gold, 10000 }, { Currency.UpgradePart, 10 } } },
        {20 , new Dictionary<Currency, int>(){ { Currency.Gold, 15000 }, { Currency.UpgradePart, 10 } } },
        {21 , new Dictionary<Currency, int>(){ { Currency.Gold, 15000 }, { Currency.UpgradePart, 10 } } },
        {22 , new Dictionary<Currency, int>(){ { Currency.Gold, 15000 }, { Currency.UpgradePart, 10 } } },
        {23 , new Dictionary<Currency, int>(){ { Currency.Gold, 15000 }, { Currency.UpgradePart, 10 } } },
        {24 , new Dictionary<Currency, int>(){ { Currency.Gold, 15000 }, { Currency.UpgradePart, 10 } } },
        {25 , new Dictionary<Currency, int>(){ { Currency.Gold, 15000 }, { Currency.UpgradePart, 10 } } },
    };
    public static Dictionary<Currency, int> GetUserRewardsDict(int accountLevel)
    {
        if (UserAccountLevelRewardDict.ContainsKey(accountLevel) == false) return null;
        return UserAccountLevelRewardDict[accountLevel];
    }
    public static int GetUserRewardsTotalCount()
    {
        return UserAccountLevelRewardDict.Count + 2;
    }

    public static int GetLastestLvAccountRewardFromSaveData()
    {
        if (HasLevelRewardDataFile())
        {
            List<bool> list = JSONLizer.DeserializeList<bool>(LevelRewardDataSavePath);
            for (int i = list.Count - 1; i >= 0; i--)
            {
                if (list[i]) return i;
            }
        }
        return 2;
    }
    #endregion
    #region Save-LoadLevelAccountReward
    private static void SendRewardsFromAccountLvRewardDict(int key)
    {
        foreach (var keyValue in UserAccountLevelRewardDict[key])
        {
            SendMoney(keyValue.Key, keyValue.Value);
        }
    }

    private static void SendMoney(Currency type, int value)
    {
        switch (type)
        {
            case Currency.Gold:
                AddUserGold(value);
                break;
            case Currency.UpgradePart:
                AddUserUpgradePart(value);
                break;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="dictKeyIndex">current level equal</param>
    public static void SaveAccountlvReward(int dictKeyIndex)
    {
        List<bool> listRewardSave = new List<bool>();
        if (HasLevelRewardDataFile())
        {
            listRewardSave = LoadAccountLvReward();
        }
        else
            for (int i = 0; i < GetUserRewardsTotalCount(); i++) {listRewardSave.Add(false);}

        for (int i = GetLastestLvAccountRewardFromSaveData(); i <= dictKeyIndex; i++)
        {
            listRewardSave[i] = true;
            SendRewardsFromAccountLvRewardDict(i);
        }
        JSONLizer.SerializeList<bool>(listRewardSave, LevelRewardDataSavePath);
    }
    public static List<bool> LoadAccountLvReward()
    {
        if (HasLevelRewardDataFile() == false)
        {
            List<bool> listRewardSave = new List<bool>();
            for (int i = 0; i < GetUserRewardsTotalCount(); i++)
            {
                listRewardSave.Add(false);
            }
            return listRewardSave;
        }
        return JSONLizer.DeserializeList<bool>(LevelRewardDataSavePath);
    }
    public static bool HasLevelRewardDataFile()
    {
        return File.Exists(LevelRewardDataSavePath);
    }

    #endregion Save-LoadLevelAccountReward


    #region Total stuffs

    public static int totalTimeSurvive
    {
        private set => PlayerPrefs.SetInt("totalTimeSurvive", value);
        get => PlayerPrefs.GetInt("totalTimeSurvive", 0);
    }
    public static void AddTotalTimeSurvive(int time)
    {
        totalTimeSurvive += time;
    }
    public static int totalKills
    {
        private set => PlayerPrefs.SetInt("totalKills", value);
        get => PlayerPrefs.GetInt("totalKills", 0);
    }
    public static int totalGoldEarned
    {
        private set => PlayerPrefs.SetInt("totalGoldEarned", value);
        get => PlayerPrefs.GetInt("totalGoldEarned", 0);
    }


    public static void AddTotalKills(int kills)
    {
        totalKills += kills;
    }
    public static void ClearAllRecordAchievement()
    {
        totalKills = 0;
        totalTimeSurvive = 0;
        totalGoldEarned = 0;
    }

    #endregion Total stuffs


    public static int GetUserCurrentLevel()
    {
        int xp = (int)Mathf.Pow(accountLevel, 3) + 2 * accountLevel;
        while (xp < userXP)
        {
            accountLevel++;
            xp = (int)Mathf.Pow(accountLevel, 3) + 2 * accountLevel;
        }
        //SaveAccountlvReward(accountLevel);
        return accountLevel;
    }
    public static bool CheckUserCanPay(int targetPrice, Currency type)
    {
        switch (type)
        {
            case Currency.UpgradePart:
                return userUpgradePart >= targetPrice;
            default:
                return userGold >= targetPrice;
        }
    }

    public static float GetMaxXPOfCurrentLevel(int level)
    {
        return Mathf.Pow(level, 3) + 2 * level;
    }

    public static int userXP
    {
        get { return PlayerPrefs.GetInt("userXP", 0); }
        private set { PlayerPrefs.SetInt("userXP", value); }
    }

    public static int accountLevel
    {
        get => PlayerPrefs.GetInt("accountLevel", 1);
        private set => PlayerPrefs.SetInt("accountLevel", value);
    }

    public static bool loadedFromGameplayScene = false;

    public static void AddUserXP(int amount)
    {
        userXP += amount;
        //SaveAccountlvReward(GetUserCurrentLevel());
    }

    public static int userGold
    {
        get { return PlayerPrefs.GetInt("userGold", 0); }
        private set { PlayerPrefs.SetInt("userGold", value); }
    }
    public static int userUpgradePart
    {
        get { return PlayerPrefs.GetInt("userUpgradePart", 0); }
        private set { PlayerPrefs.SetInt("userUpgradePart", value); }
    }
    public static void AddUserGold(int plusGold)
    {
        userGold += plusGold;
        totalGoldEarned += plusGold;
    }
    public static int GetUserGold()
    {
        return userGold;
    }
    public static void AddUserUpgradePart(int plusUpgradePart)
    {
        userUpgradePart += plusUpgradePart;
    }

    

    
}

public class VisualCurrencyRewardItemData
{
    public Currency rewardType;
    public int amount;
    public VisualCurrencyRewardItemData (Currency type, int amount)
    {
        rewardType = type;
        this.amount = amount;
    }
}
public enum Currency
{
    Gold, UpgradePart, XP
}
