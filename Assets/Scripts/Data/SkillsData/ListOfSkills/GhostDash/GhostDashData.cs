using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
[CreateAssetMenu(fileName = "GhostDash", menuName = "Skill/GhostDash")]

public class GhostDashData : SkillData
{
    public float generateSpriteGhostEachTimeTimer = 0.04f;
    public float dashTimer  = 0.5f;
    public float dashSpeedMultiplier = 100f;
    public float dashDuration = 0.1f;









}
