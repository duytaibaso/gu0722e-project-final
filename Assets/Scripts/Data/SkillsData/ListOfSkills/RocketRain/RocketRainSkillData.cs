using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "SkillData", menuName = "Skill/RocketRain")]

public class RocketRainSkillData : SkillData
{
    public float rangeOfActtack;
}
