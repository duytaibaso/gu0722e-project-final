using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;



[CreateAssetMenu(fileName = "SkillData", menuName = "Skill/Origin")]
public class SkillData : ScriptableObject
{
    public Sprite icon;
    public string skillName;
    public ListOfSkill skillType;
    public int damage;
    public float cooldown;




}


public enum ListOfSkill
{
    GhostDash = 0, ThreeGrenades = 1, RocketRain = 2, Heal = 3, ShurikenSpin = 4, ForceField = 5
}
