using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "SkillData", menuName = "Skill/RadiusCircularRelatedSkill")]
public class RadiusCircleSkillData : SkillData
{
    public float range;
    public float duration;
    [Tooltip("bao nhieu vong tren giay")]
    public int angularSpeed;
}
