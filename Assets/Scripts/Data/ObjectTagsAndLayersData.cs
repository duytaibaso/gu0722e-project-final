using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectTagsAndLayersData 
{
    public const string enemyTag = "Enemy";
    public const string playerTagAndLayer = "Player";
    public const string IgnoreCollisionLayer = "IgnoreCollision";
    public const string obstacleTag = "Obstacle";
    public const string enemySpawnerTag = "EnemySpawner";


}

