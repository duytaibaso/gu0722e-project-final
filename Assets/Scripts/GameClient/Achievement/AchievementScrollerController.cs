using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using EnhancedUI.EnhancedScroller;
using System.IO;
using System;

public class AchievementScrollerController : MonoBehaviour, IEnhancedScrollerDelegate
{
    const string DataText = "AchievementData.txt";
    private string FilePath;
    [SerializeField] GameObject popup;
    [SerializeField] Sprite inactiveButtSprite;
    [SerializeField] Sprite activeButtSprite;
    [SerializeField] MainTitleController mainTitle;
    [SerializeField] EquipmentSystem equipSys;
    [SerializeField] AchievementData achievementData;
    [SerializeField] AchievementCell cellPrefab;
    [SerializeField] EnhancedScroller scroller;
    [SerializeField] Button closeBtn;
    [SerializeField] RewardPanelPopUp rewardPopup;

    private List<AchievementCellVisualData> Data;
    private List<AchievementCellSaveData> SavedData;

    //void Awake()
    //{
    //    FilePath = Application.persistentDataPath + DataText;
    //}

    // Start is called before the first frame update
    void Start()
    {
        scroller.Delegate = this;
        closeBtn.onClick.AddListener(OnCloseButtonClicked);
    }

    private void OnCloseButtonClicked()
    {
        popup.SetActive(false);
    }

    public void Show()
    {
        Data = new List<AchievementCellVisualData>();
        Data.Clear();
        popup.SetActive(true);
        LoadSavedData();
        LoadItems();
    }

    public Sprite GetInactiveButtonSprite()
    {
        return inactiveButtSprite;
    }
    public Sprite GetActiveButtonSprite() { return activeButtSprite; }

    private void LoadItems()
    {
        for (int i = 0; i < achievementData.GetAchievementObjectiveListCount(); i++)
        {
            string objText = achievementData.GetAchievementObjectiveText(i);
            int value = achievementData.GetValue(i);
            bool canClaimCon = achievementData.CheckCanClaimCondition(i);
            bool isClaimed = SavedData[i].isClaimed;
            Data.Add(new AchievementCellVisualData()
            {
                index = i,
                headerRequirement = string.Format(objText, value),
                canClaim = canClaimCon,
                totalProgress = value,
                type = achievementData.GetAchievementType(i),
                isClaimed = isClaimed
            }); ; ; ;
        }
        scroller.ReloadData();
    }

    public void RewardAndSave(int index)
    {
        Vector2Int goldAndXP =  achievementData.GetRewardGoldAndExp(index);
        Dictionary<Currency, int> currencyDict = new Dictionary<Currency, int>()
        {
            {Currency.Gold, goldAndXP.x },
            {Currency.XP, goldAndXP.y },
        };
        Tuple<ItemBase, bool> itemReward = achievementData.GetRewardItemWhetherGunOrMelee(index);
        if (itemReward.Item1 == null)
        {
            rewardPopup.PopUpCurrencyRewards(currencyDict);
        }
        else
        {
            List<RewardVisualItemData> list = new List<RewardVisualItemData>();
            if (itemReward.Item2) //melee
            {
                MeleeItem item = itemReward.Item1 as MeleeItem;
                equipSys.AddInventoryMeleeItem(item);
                list.Add(new RewardVisualItemData(equipSys.GetBGItemRaritySpriteDict(item.rarity), equipSys.GetWeaponData().GetMeleeItemIconInInventory(item.type), 1));
            }
            else //gun
            {
                GunItem item = itemReward.Item1 as GunItem;
                equipSys.AddInventoryGunItem(item);
                list.Add(new RewardVisualItemData(equipSys.GetBGItemRaritySpriteDict(item.rarity), equipSys.GetWeaponData().GetGunItemIconInInventory(item.type), 1));
            }
            rewardPopup.PopUpMixedRewards(currencyDict, list);
        }
        


        SavedData[index].isClaimed = true;
        JSONLizer.Serialize(SavedData, FilePath);
    }
    private void LoadSavedData()
    {
        FilePath = Application.persistentDataPath + DataText;
        if (File.Exists(FilePath))
        {
            SavedData = JSONLizer.DeserializeList<AchievementCellSaveData>(FilePath);
            for (int i = SavedData.Count; i <= achievementData.GetAchievementObjectiveListCount(); i++)
            {
                SavedData.Add(new AchievementCellSaveData() { isClaimed = false });
            }
        }
        else
        {
            SavedData = new List<AchievementCellSaveData>();
            for (int i = 0; i < achievementData.GetAchievementObjectiveListCount(); i++)
            {
                SavedData.Add(new AchievementCellSaveData() {isClaimed = false });
            }
        }
    }

    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        AchievementCell cellView = scroller.GetCellView(cellPrefab) as AchievementCell;
        cellView.SetData(Data[dataIndex], this);
        return cellView;
    }

    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        return 100f;
    }

    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        return Data.Count;
    }

    //[MenuItem("Duy/Clear Achievement Save Data")]
    //public static void ClearAchievementSaveData()
    //{
    //    if (File.Exists(Application.persistentDataPath + DataText))
    //    {
    //        File.Delete(Application.persistentDataPath + DataText);
    //    }
    //}
}


public class AchievementCellVisualData
{
    public int index;
    public AchievementType type;
    public string headerRequirement;
    public int totalProgress;
    public bool canClaim;
    public bool isClaimed;
    public Sprite grayButton;
}
public class AchievementCellSaveData
{
    public bool isClaimed = false;
}

