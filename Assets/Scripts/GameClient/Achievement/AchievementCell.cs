using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EnhancedUI.EnhancedScroller;
using TMPro;
using System;

public class AchievementCell : EnhancedScrollerCellView
{
    [SerializeField] Button claimButton;
    [SerializeField] TextMeshProUGUI claimText;
    [SerializeField] TextMeshProUGUI infoText;
    [SerializeField] Image fillProgressBar;
    [SerializeField] TextMeshProUGUI fillText;

    private AchievementScrollerController scroller;
    private int index;

    private void Start()
    {
        claimButton.onClick.AddListener(OnClaimButtonClicked);
    }

    private void OnClaimButtonClicked()
    {
        DeactiveClaimButton();
        scroller.RewardAndSave(index);
    }

    public void SetData(AchievementCellVisualData _data, AchievementScrollerController _scroller)
    {
        infoText.text = _data.headerRequirement;
        index = _data.index;
        scroller = _scroller;
        SetFillProgressBar(_data.type, _data.totalProgress);
        ClaimButtonState(_data.canClaim, _data.isClaimed);
    }

    private void SetFillProgressBar(AchievementType type, int totalProgress)
    {
        float value;
        string _fillText;
        switch (type)
        {
            case AchievementType.HighGold:
                value = (float)UserData.totalGoldEarned / (float)totalProgress;
                _fillText = $"{UserData.totalGoldEarned}/{totalProgress}";
                break;
            case AchievementType.HighKills:
                value = (float)UserData.totalKills / (float)totalProgress;
                _fillText = $"{UserData.totalKills}/{totalProgress}";
                break;
            default:// AchievementType.HighTimeSurvival
                value = (float)UserData.totalTimeSurvive / (float)totalProgress;
                _fillText = $"{UserData.totalTimeSurvive}/{totalProgress}";
                break;
        }
        fillProgressBar.fillAmount = value;
        fillText.text = _fillText;
    }

    private void ClaimButtonState(bool canClaim, bool isClaimed)
    {
        claimButton.image.sprite = canClaim ? scroller.GetActiveButtonSprite() : scroller.GetInactiveButtonSprite();
        claimButton.interactable = canClaim;
        DeactiveClaimButton(isClaimed);
    }

    private void DeactiveClaimButton(bool check = true)
    {
        claimButton.gameObject.SetActive(!check);
        claimText.text = !check? "Claim" : "Completed";
        claimText.color = !check ? Color.white : Color.green;
    }
}
