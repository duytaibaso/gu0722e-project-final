using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using TMPro;
using System;
public class ShopPanel : SerializedMonoBehaviour
{
    [SerializeField] MainTitleController mainTitleController;
    [SerializeField] Button DailyShopRefreshButton;
    [SerializeField] Button CloseButton;
    [SerializeField] TextMeshProUGUI RefreshButtonText;
    [SerializeField] ShopSystem shopSystem;
    [SerializeField] ShopItemPrefab DailyItemPrefab;
    [SerializeField] Transform dailyItemCellHolder;
    [SerializeField] Dictionary<bool, Sprite> ButtonStateDict;
    [SerializeField] List<GameObject> dailyItemCellHolderObjectList;

    public MainTitleController GetMainTitlecontroller() => mainTitleController;

    private void Start()
    {
        DailyShopRefreshButton.onClick.AddListener(OnDailyShopRefreshButtonClicked);
        CloseButton.onClick.AddListener(OnCloseButtonClicked);
        RefreshButtonText.text = "Refresh";
        if (shopSystem.HasTurnToRefresh() == false)
        {
            RefreshButtonInCooldownRefresh();
        }
        DailyShopRefreshButton.image.sprite = ButtonStateDict[true];
        AddObjectDailyItems();
    }

    private void OnCloseButtonClicked()
    {
        gameObject.SetActive(false);
    }

    private void RefreshButtonInCooldownRefresh()
    {
        DailyShopRefreshButton.interactable = false;
        DailyShopRefreshButton.image.sprite = ButtonStateDict[false];
        RefreshButtonText.text = "Refresh \n TimeLeft " + (DateTime.Now - DateTime.Parse(shopSystem.lastRefreshedTime)).ToString();
    }

    private void AddObjectDailyItems()
    {
        for (int i = 0; i < shopSystem.GetDailyShopItemData().Count; i++)
        {
            DailyItemPrefab.SetVisualData(shopSystem.GetDailyShopItemData()[i], shopSystem, this);
            var obj = Instantiate(DailyItemPrefab.gameObject, dailyItemCellHolder);
            dailyItemCellHolderObjectList.Add(obj);
        }
    }

    private void OnDailyShopRefreshButtonClicked()
    {
        shopSystem.RefreshItemClicked();

        for (int i = 0; i < shopSystem.GetDailyShopItemData().Count; i++)
        {
            dailyItemCellHolderObjectList[i].GetComponent<ShopItemPrefab>().SetVisualData(shopSystem.GetDailyShopItemData()[i], shopSystem, this );
        }
        RefreshButtonInCooldownRefresh();
    }


}
