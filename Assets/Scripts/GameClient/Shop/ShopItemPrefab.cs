using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class ShopItemPrefab : MonoBehaviour
{
    [SerializeField] Button SelfButton;
    [SerializeField] TextMeshProUGUI qty;
    [SerializeField] Image bg;
    [SerializeField] Image icon;
    [SerializeField] Image priceBG;
    [SerializeField] TextMeshProUGUI pricetagText;
    [SerializeField] GameObject DarkPanel;

    [HideInInspector] [SerializeField] ShopSystem sys;
    [HideInInspector] [SerializeField] ShopItem item;
    [HideInInspector] [SerializeField] ShopPanel shopPanel;

    [SerializeField] int price;

    private void Start()
    {
        SelfButton.onClick.AddListener(OnSelfButtonClicked);
    }

    private void OnSelfButtonClicked()
    {
        if (UserData.CheckUserCanPay(price, Currency.Gold) == false) { 

            return; 
        }
        sys.AddItemToInventory(item.index);
        SelfButton.interactable = false;
        DarkPanel.SetActive(true);
        UserData.AddUserGold(-price);
        shopPanel.GetMainTitlecontroller().SetMoney();
    }

    public void SetVisualData(ShopItem _item, ShopSystem _sys, ShopPanel _shopPanel )
    {
        DarkPanel.SetActive(false);
        item = _item;
        sys = _sys;
        shopPanel = _shopPanel;
        Visualization(item.itemType);
        if (_item.Bought) {
            DarkPanel.SetActive(true);
            SelfButton.interactable = false;
            return; 
        }
        DarkPanel.SetActive(false);
        SelfButton.interactable = true;
    }
    private void Visualization(ShopItemType type)
    {
        switch (type)
        {
            case ShopItemType.Gun:
                VisualizeGunItem();
                break;
            case ShopItemType.Melee:
                VisualizeMeleeItem();
                break;
            case ShopItemType.UpgradePart:
                VisualizeUpgradePart();
                break;
        }

    }
    private void VisualizeGunItem()
    {
        bg.sprite = sys.GetBGItemRarity(item.item.rarity);
        //icon.sprite = sys.GetWeaponData().GetGunItemIconInInventory((item.item as GunItem).type);
        icon.sprite = sys.GetWeaponData().GetGunItemIconInInventory((GunWeaponType)item.weaponEnumTypeIndex);
        priceBG.sprite = sys.GetBGPriceTagSpriteDict(item.item.rarity);
        SetPriceAndQtyAndCheckBuy();
    }
    private void VisualizeMeleeItem()
    {
        bg.sprite = sys.GetBGItemRarity(item.item.rarity);
        //icon.sprite = sys.GetWeaponData().GetMeleeItemIconInInventory((item.item as MeleeItem).type);
        icon.sprite = sys.GetWeaponData().GetMeleeItemIconInInventory((MeleeWeaponType)item.weaponEnumTypeIndex);
        priceBG.sprite = sys.GetBGPriceTagSpriteDict(item.item.rarity);
        SetPriceAndQtyAndCheckBuy();
    }

    private void SetPriceAndQtyAndCheckBuy()
    {
        pricetagText.text = Format.FormatIntThousandMillion(item.price.price);
        price = item.price.price;
        qty.text = item.qty <= 1 ? "" : item.qty.ToString();
        CheckItemBought();
    }

    private void VisualizeUpgradePart()
    {
        bg.sprite = sys.GetBGItemRarity(Rarity.Normal);
        icon.sprite = sys.GetUpgradePartSprite();
        priceBG.sprite = sys.GetBGPriceTagSpriteDict(Rarity.Normal);
        pricetagText.text = item.price.price.ToString();
        price = item.price.price;
        qty.text = item.qty <= 1 ? "" : item.qty.ToString();
        CheckItemBought();
    }

    private void CheckItemBought()
    {
        if (item.Bought)
        {
            DarkPanel.gameObject.SetActive(true);
            SelfButton.interactable = false;
        }
    }
}
