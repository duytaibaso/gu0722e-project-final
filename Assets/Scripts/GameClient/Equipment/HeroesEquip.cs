using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeroesEquip : MonoBehaviour
{
    [SerializeField] Button heroes;
    [SerializeField] Button heroIconButt;
    [SerializeField] Image heroVisual;
    [SerializeField] Image heroIcon;

    [SerializeField] HeroesSelectionPanel heroesSelectionPanel;
    [SerializeField] HeroesData heroesData;
    

    // Start is called before the first frame update
    void Start()
    {
        heroes.onClick.AddListener(OnHeroesButtonClicked);
        heroIconButt.onClick.AddListener(OnHeroesButtonClicked);
        SetData(heroesData.heroesEquippedEnumInt);
    }

    private void OnHeroesButtonClicked()
    {
        heroesSelectionPanel.Show(heroesData);
    }

    public void SetData(int enumIndex)
    {
        heroVisual.sprite = heroesData.GetHeroesBodySprite((Heroes)enumIndex);
        heroIcon.sprite = heroesData.GetHeroesIcon((Heroes)enumIndex);

        heroesData.SetEquipHeroes(enumIndex);
    }

}
