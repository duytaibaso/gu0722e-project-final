using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class EquipmentEquippedCell : MonoBehaviour
{
    [SerializeField] EquipmentPanel popUp;
    [SerializeField] Button SelfButton;
    [SerializeField] Image BG;
    [SerializeField] Image Icon;
    [SerializeField] TextMeshProUGUI level;
    private bool isMelee;

    private WeaponItem item;

    public WeaponItem GetCurrentEquipItem()
    {
        return item;
    }

    private void Start()
    {
        SelfButton.onClick.AddListener(OnSelfButtonClicked);
    }
    private void OnSelfButtonClicked()
    {
        popUp.ShowItemInfoPopUp(isMelee, Icon.sprite, BG.sprite, item, true);
    }
    public void SetVisualData(bool isMelee, Sprite bg, Sprite icon, WeaponItem item)
    {
        this.isMelee = isMelee;
        this.item = item;
        BG.sprite = bg;
        Icon.sprite = icon;
        this.level.text = "Lv." + item.level.ToString();

    }



}
