using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Sirenix.OdinInspector;
using System;
public class EquipmentPanel : SerializedMonoBehaviour
{
    const string levelForm = "Level: ";
    [SerializeField] RewardPanelPopUp rewardPanelPopUp;
    [SerializeField] MainTitleController mainTitle;
    [SerializeField] EquipmentSystem equipSys;
    [SerializeField] EquipmentScrollerController scroller;
    [SerializeField] WarnPopUp warnPopUp;
    [SerializeField] EquipmentEquippedCell gunEquipCell1;
    [SerializeField] EquipmentEquippedCell gunEquipCell2;
    [SerializeField] EquipmentEquippedCell meleeEquipCell;
    [SerializeField] ShopSystem shopSys;
    [SerializeField] GameObject WeaponInfoPopUp;

    [SerializeField] Image VisualItemIcon;
    [SerializeField] Image VisualItemBG;
    [SerializeField] TextMeshProUGUI level;
    [SerializeField] TextMeshProUGUI description;
    [SerializeField] TextMeshProUGUI ItemName;
    [SerializeField] TextMeshProUGUI ItemTypeName;
    [SerializeField] Button equipButton;
    [SerializeField] Button exitPanelButton;
    [SerializeField] Button backPopUpButton;
    [SerializeField] Button levelUpButton;
    [SerializeField] Button maxLevelUpButton;
    [SerializeField] Button evolveButton;

    [SerializeField] List<EquipmentItemModVisual> ItemModList;
    [SerializeField] Dictionary<Rarity, Sprite> RarityModSpriteDict;
    [HideInInspector] [SerializeField] WeaponItem selectedItem;

    [HideInInspector] [SerializeField] bool primarygunEquipped = false;
    [HideInInspector] [SerializeField] bool isMelee;
    [HideInInspector] [SerializeField] bool fromEquipment;
    private int selectedItemMaxLevel;
    private void Start()
    {
        equipButton.onClick.AddListener(OnEquipButton);
        exitPanelButton.onClick.AddListener(OnExitPanelButtonClicked);
        backPopUpButton.onClick.AddListener(OnBackPopUpButtonClicked);
        levelUpButton.onClick.AddListener(OnLevelUpButtonClicked);
        maxLevelUpButton.onClick.AddListener(OnMaxLevelUpButtonClicked);
        evolveButton.onClick.AddListener(OnEvoButtonClicked);
        LoadEquip();
    }

    public void Show()
    {
        gameObject.SetActive(true);
        scroller.LoadItems();
    }

    //IsMelee, iconItem, itemBG, level
    public void ShowItemInfoPopUp(bool _isMelee, Sprite _icon, Sprite _bg, WeaponItem _item, bool _fromEquip = false)
    {
        fromEquipment = _fromEquip;
        selectedItem = _item;
        selectedItemMaxLevel = equipSys.GetWeaponData().GetWeaponMaxLevel(_item.rarity);
        WeaponInfoPopUp.SetActive(true);
        isMelee = _isMelee;
        VisualItemIcon.sprite = _icon;
        VisualItemBG.sprite = _bg;
        level.text = levelForm + $"{_item.level}/{selectedItemMaxLevel}";
        //levelUpButton.gameObject.SetActive(_item.level < selectedItemMaxLevel);
        //maxLevelUpButton.gameObject.SetActive(_item.level < selectedItemMaxLevel);
        levelUpButton.gameObject.SetActive(equipSys.CanLevelUp(_item.level, _item.rarity));
        maxLevelUpButton.gameObject.SetActive(equipSys.CanMaxLevelUp(_item.level, _item.rarity));
        equipButton.gameObject.SetActive(_fromEquip == false);
        evolveButton.gameObject.SetActive(equipSys.CanEvolveGun(_item.level, _item.rarity));

        if (_isMelee == false) SetGunVisual(_item);
        else SetMeleeVisual(_item);
    }
    private void OnEquipButton()
    {
        WeaponInfoPopUp.SetActive(false);
        if (isMelee == false) {
            GunItem gunItem = (selectedItem as GunItem);
            if (primarygunEquipped == false)
            {
                primarygunEquipped = true;
                //equipSys.AddInventoryGunItem((gunEquipCell1.GetCurrentEquipItem() as GunItem));
                equipSys.EquipPrimary(selectedItem.index);
                gunEquipCell1.SetVisualData(false, shopSys.GetBGItemRarity(gunItem.rarity),
                    equipSys.GetWeaponData().GetGunItemIconInInventory(gunItem.type),
                    gunItem);
            }
            else
            {
                primarygunEquipped = false;
                //equipSys.AddInventoryGunItem((gunEquipCell2.GetCurrentEquipItem() as GunItem));
                equipSys.Equip2ndary(selectedItem.index);
                gunEquipCell2.SetVisualData(false, shopSys.GetBGItemRarity(gunItem.rarity),
                    equipSys.GetWeaponData().GetGunItemIconInInventory(gunItem.type),
                    gunItem);
            }
        }
        else
        {
            MeleeItem meleeItem = (selectedItem as MeleeItem);
            //equipSys.AddInventoryMeleeItem((meleeEquipCell.GetCurrentEquipItem() as MeleeItem));
            equipSys.EquipMelee(selectedItem.index);
            meleeEquipCell.SetVisualData(true,
                shopSys.GetBGItemRarity(meleeItem.rarity),
                equipSys.GetWeaponData().GetMeleeItemIconInInventory(meleeItem.type),
                meleeItem
            ); ;
        }
        scroller.LoadItems();
    }
    
    private void OnExitPanelButtonClicked()
    {
        mainTitle.SetMainBodyActive();
        gameObject.SetActive(false);
        WeaponInfoPopUp.SetActive(false);

    }
    private void OnBackPopUpButtonClicked()
    {
        WeaponInfoPopUp.SetActive(false);
    }
    private void OnEvoButtonClicked() 
    {
        Tuple<int, int> requiredValue = equipSys.GetEvolveRecipRequired(selectedItem.rarity + 1);
        string info = $"New Evolve Weapon Rarity {(Rarity)(selectedItem.rarity + 1)} required {requiredValue.Item1} Gold and {requiredValue.Item2} Upgrade Parts." +
            $" Proceed?";
        warnPopUp.SetVisualData(info, Evolve);
    }
    private void Evolve()
    {
        VisualItemBG.sprite = shopSys.GetBGItemRarity((Rarity)(selectedItem.rarity + 1));
        level.text = levelForm + $"1/{equipSys.GetWeaponData().GetWeaponMaxLevel((Rarity)(selectedItem.rarity + 1))}";
        if (isMelee) equipSys.EvolveMelee(selectedItem.index);
        else equipSys.EvolveGun(selectedItem.index);
        scroller.LoadItems();
        LoadEquip();
        rewardPanelPopUp.PopUpOneItemReward(VisualItemBG.sprite, VisualItemIcon.sprite, 1);
    }
    private void OnLevelUpButtonClicked()
    {
        int nextLevel = selectedItem.level + 1;
        string info = $"Up to level {nextLevel} required {equipSys.GetGoldRequiredForALevel(nextLevel)} Gold. Proceed?";
        warnPopUp.SetVisualData(info, LevelUp);
    }

    private void OnMaxLevelUpButtonClicked()
    {
        int nextLevel = selectedItem.level + 1;
        int maxLevel = equipSys.GetWeaponData().GetWeaponMaxLevel(selectedItem.rarity);
        string info = $"Up to max level {nextLevel} required {equipSys.GetGoldRequiredForMaxLeveling(nextLevel, maxLevel)} Gold. Proceed?";
        warnPopUp.SetVisualData(info, MaxLevelUp);
    }    

    private void LevelUp()
    {
        if (isMelee) equipSys.LevelUpMeleeItem(selectedItem.index, selectedItem.level);
        else equipSys.LevelUpGunItem(selectedItem.index, selectedItem.level);
        level.text = $"Level: {selectedItem.level}/{selectedItemMaxLevel}";
        scroller.LoadItems();
        LoadEquip();
    }
    private void MaxLevelUp()
    {
        if (isMelee) equipSys.MaxLevelUpMeleeItem(selectedItem.index, selectedItem.level);
        else equipSys.MaxLevelUpGunItem(selectedItem.index, selectedItem.level);
        selectedItem.level = equipSys.GetWeaponData().GetWeaponMaxLevel(selectedItem.rarity);
        level.text = $"Level: {selectedItem.level}/{selectedItemMaxLevel}";
        scroller.LoadItems();
        LoadEquip();
    }

    private void SetGunVisual(WeaponItem item)
    {
        GunWeaponType _type = (item as GunItem).type;
        description.text = equipSys.GetWeaponData().GetGunDescription(_type);
        ItemName.text = _type.ToString();
        ItemTypeName.text = ShopItemType.Gun.ToString();
        for (int i = 0; i < ItemModList.Count; i++)
        {
            Tuple<WeaponModUpgradeStatType, int> tuple = equipSys.GetWeaponData().GetGunStatTypeAndValue(_type)[i];
            ItemModList[i].SetVisual(i + 1, RarityModSpriteDict[(Rarity)(i + 1)], equipSys.GetWeaponData().
                GetWeaponInUpgradeStatText(tuple.Item1), tuple.Item2, item.rarity);
        }
    }
    private void SetMeleeVisual(WeaponItem item)
    {
        MeleeWeaponType _type = (item as MeleeItem).type;
        description.text = equipSys.GetWeaponData().GetMeleeDescription(_type);
        ItemName.text = _type.ToString();
        ItemTypeName.text = ShopItemType.Melee.ToString();
        for (int i = 0; i < ItemModList.Count; i++)
        {
            Tuple<WeaponModUpgradeStatType, int> tuple = equipSys.GetWeaponData().GetMeleeStatTypeAndValue(_type)[i];
            ItemModList[i].SetVisual(i + 1, RarityModSpriteDict[(Rarity)(i + 1)], equipSys.GetWeaponData().
                GetWeaponInUpgradeStatText(tuple.Item1), tuple.Item2, item.rarity);
        }
    }

    private void LoadEquip()
    {
        GunItem gunItem1 = equipSys.GetPrimaryEquippedWeapon();
        gunEquipCell1.SetVisualData(false,
            shopSys.GetBGItemRarity(gunItem1.rarity),
            equipSys.GetWeaponData().GetGunItemIconInInventory(gunItem1.type),
            gunItem1
            ); ;

        GunItem gunItem2 = equipSys.GetSecondaryEquippedWeapon();
        gunEquipCell2.SetVisualData(false,
            shopSys.GetBGItemRarity(gunItem2.rarity),
            equipSys.GetWeaponData().GetGunItemIconInInventory(gunItem2.type),
            gunItem2
            ); ;

        MeleeItem meleeItem = equipSys.GetEquippedMeleeWeapon();
        meleeEquipCell.SetVisualData(true,
            shopSys.GetBGItemRarity(meleeItem.rarity),
            equipSys.GetWeaponData().GetMeleeItemIconInInventory(meleeItem.type),
            meleeItem
            ); ;
    }


}
