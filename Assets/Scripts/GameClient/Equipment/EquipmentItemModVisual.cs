using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
public class EquipmentItemModVisual : MonoBehaviour
{
    [SerializeField] Image iconRarirty;
    [SerializeField] TextMeshProUGUI description;

    public void SetVisual(int rarityEnumIndx, Sprite icon, string modStatString, int value, Rarity rarity)
    {
        iconRarirty.sprite = icon;
        string valueInt = value > 0? value.ToString() : "";
        description.text = modStatString + " " + valueInt;
        description.color = Color.green;
        if (rarityEnumIndx > (int)rarity)
        {
            description.color = Color.white;
            iconRarirty.color = Color.gray;
        }
    }

}
