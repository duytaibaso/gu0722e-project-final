using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeroesSelectionCell : MonoBehaviour
{
    [SerializeField] Button selfButton;
    [SerializeField] Image heroIcon;

    [HideInInspector] [SerializeField] HeroesEquip equipInterface;
    [HideInInspector][SerializeField] HeroesSelectionPanel panel;
    [HideInInspector] [SerializeField] int heroEnumInt;

    private void Start()
    {
        selfButton.onClick.AddListener(OnSelfButtonClicked);
    }

    private void OnSelfButtonClicked()
    {
        equipInterface.SetData(heroEnumInt);
        panel.Hide();
    }

    public void SetVisualData(int enumInt, Sprite icon, HeroesSelectionPanel heroSelectionPanel, HeroesEquip equip)
    {
        heroEnumInt = enumInt;
        heroIcon.sprite = icon;
        equipInterface = equip;
        panel = heroSelectionPanel;
    }


}
