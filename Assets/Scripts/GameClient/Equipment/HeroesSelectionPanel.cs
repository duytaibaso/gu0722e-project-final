using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeroesSelectionPanel : MonoBehaviour
{
    [SerializeField] Transform scrollerOfHeroIcons;
    [SerializeField] HeroesEquip heroesEquip;
    [SerializeField] HeroesSelectionCell cellPrefab;
    [SerializeField] Button closeButton;

    private void Start()
    {
        closeButton.onClick.AddListener(OnCloseButtonClicked);
    }

    private void OnCloseButtonClicked()
    {
        Hide();
    }

    public void Show(HeroesData data)
    {
        Clear();
        for (int i = 0; i < data.GetHeroesStateTotal(); i++)
        {
            cellPrefab.SetVisualData(i, data.GetHeroesIcon((Heroes)i), this, heroesEquip);
            Instantiate(cellPrefab.gameObject, scrollerOfHeroIcons);
        }
        gameObject.SetActive(true);
    }
    
    public void Hide()
    {
        Clear();
        gameObject.SetActive(false);
    }

    private void Clear()
    {
        for (int i = 0; i < scrollerOfHeroIcons.childCount; i++)
        {
            Destroy(scrollerOfHeroIcons.GetChild(i).gameObject);
        }
    }

}
