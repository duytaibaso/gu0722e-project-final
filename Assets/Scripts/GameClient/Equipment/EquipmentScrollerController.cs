using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EnhancedUI.EnhancedScroller;
public class EquipmentScrollerController : MonoBehaviour, IEnhancedScrollerDelegate
{
    [SerializeField] EnhancedScroller scroller;
    [SerializeField] EquipmentSystem equipSys;
    [SerializeField] EquipmentPanel popUp;
    [SerializeField] GameObject WeaponInfoPopUp; // them class
    [SerializeField] EquipmentRowCell equipmentRowCell;
    [SerializeField] ShopSystem shopSystem;
    //popup equipment

    private List<EquimentItemDisplayData> Data;
    //rowbase view 5 cell
    
    // Start is called before the first frame update
    void Start()
    {
        scroller.Delegate = this;
        
        //LoadItems();
    }

    //public void Show()
    //{
    //    LoadItems();
    //}

    public EquipmentPanel GetEquipmentWeaponInfoPopUp() { return popUp; }

    public void LoadItems()
    {
        //Data.Clear();
        Data = new List<EquimentItemDisplayData>();
        int count = 0;
        EquimentItemDisplayData newData = new EquimentItemDisplayData();
        if (equipSys.ItemData.InventoryGunItemDataList.Count >= 1)
        {
            for (int i = 0; i < equipSys.ItemData.InventoryGunItemDataList.Count; i++)
            {
                if (count == 0)
                {
                    newData = new EquimentItemDisplayData();
                    Data.Add(newData);
                }
                newData.itemDisplay.Add(new EquipmentItemCellDisplayData()
                {
                    index = i,
                    IsMelee = false,
                    Icon = equipSys.GetWeaponData().GetGunItemIconInInventory(equipSys.ItemData.InventoryGunItemDataList[i].type),
                    BGRarity = shopSystem.GetBGItemRarity(equipSys.ItemData.InventoryGunItemDataList[i].rarity),
                    item = equipSys.ItemData.InventoryGunItemDataList[i],
                }); ; ;
                count++;
                if (count == 5) count = 0;
            }
        }
        if (equipSys.ItemData.InventoryMeleeItemDataList.Count >= 1)
        {
            for (int i = 0; i < equipSys.ItemData.InventoryMeleeItemDataList.Count; i++)
            {
                if (count == 0)
                {
                    newData = new EquimentItemDisplayData();
                    Data.Add(newData);
                }
                newData.itemDisplay.Add(new EquipmentItemCellDisplayData()
                {
                    index = i,
                    IsMelee = true,
                    Icon = equipSys.GetWeaponData().GetMeleeItemIconInInventory(equipSys.ItemData.InventoryMeleeItemDataList[i].type),
                    BGRarity = shopSystem.GetBGItemRarity(equipSys.ItemData.InventoryMeleeItemDataList[i].rarity),
                    item = equipSys.ItemData.InventoryMeleeItemDataList[i],
                }); ;
                count++;
                if (count == 5) count = 0;
            }
        }
        scroller.ReloadData();
    }
    
    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        return Data.Count;
    }

    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        return 220f;
    }

    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        EquipmentRowCell cellView = scroller.GetCellView(equipmentRowCell) as EquipmentRowCell;
        cellView.SetData(Data[dataIndex], this);
        return cellView;
    }
}



public class EquimentItemDisplayData
{
    public List<EquipmentItemCellDisplayData> itemDisplay = new List<EquipmentItemCellDisplayData>(); // 5 items
}

public class EquipmentItemCellDisplayData
{
    public int index;
    public bool IsMelee;
    public Sprite BGRarity;
    public Sprite Icon;
    public WeaponItem item;
}

