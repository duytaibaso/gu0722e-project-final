using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class EquipmentItemView : MonoBehaviour
{
    [SerializeField] Button SelfButton;
    [SerializeField] Image Icon;
    [SerializeField] Image BG;
    [SerializeField] TextMeshProUGUI level;

    [HideInInspector] [SerializeField] WeaponItem item;
    private EquipmentScrollerController scroller;
    private bool IsMelee;

    private void Start()
    {
        SelfButton.onClick.AddListener(OnSelfButtonClicked);
    }

    private void OnSelfButtonClicked()
    {
        //show popup using from scroller
        scroller.GetEquipmentWeaponInfoPopUp().ShowItemInfoPopUp(IsMelee, Icon.sprite, BG.sprite, item);
    }

    // Start is called before the first frame update
    public void SetData(EquipmentItemCellDisplayData displayData, EquipmentScrollerController scroller )
    {
        this.scroller = scroller;
        IsMelee = displayData.IsMelee;
        item = displayData.item;
        item.index = displayData.index;

        level.text = "Lv." + displayData.item.level.ToString();
        Icon.sprite = displayData.Icon;
        BG.sprite = displayData.BGRarity;

    }
}
