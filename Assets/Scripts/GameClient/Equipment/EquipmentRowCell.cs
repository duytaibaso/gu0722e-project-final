using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EnhancedUI.EnhancedScroller;
public class EquipmentRowCell : EnhancedScrollerCellView
{
    [SerializeField] List<EquipmentItemView> itemViews;

    public void SetData(EquimentItemDisplayData displayData, EquipmentScrollerController scroller)
    {
        for (int i = 0; i < 5; i++)
        {
            if (i < displayData.itemDisplay.Count)
            {
                itemViews[i].gameObject.SetActive(true);
                itemViews[i].SetData(displayData.itemDisplay[i], scroller);
            }
            else
            {
                itemViews[i].gameObject.SetActive(false);
            }
        }



    }




}
