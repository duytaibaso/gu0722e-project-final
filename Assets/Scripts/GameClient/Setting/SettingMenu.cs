using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using TMPro;
using System;

public class SettingMenu : MonoBehaviour
{
    const float lowestDB = -20f;
    const string volumeString = "Volume";
    const string bgmVolumeString = "BGMVolume";
    [SerializeField] GameObject gameGraphicItem;
    [SerializeField] AudioMixer masterAudioMixer;
    [SerializeField] Slider soundSlider;
    [SerializeField] Slider bgmSoundSlider;
    [SerializeField] Button backButton;
    [SerializeField] TMP_Dropdown graphicDropdown;
    [SerializeField] Toggle fullscreenToggle;
    [SerializeField] TMP_Dropdown resolutionDropdown;
    [SerializeField] Button quitGameBtn;
    Resolution[] resolutions;

    private void Start()
    {
        soundSlider.onValueChanged.AddListener(SetMasterVolumeSlider);
        graphicDropdown.onValueChanged.AddListener(SetQuality);
        fullscreenToggle.onValueChanged.AddListener(SetFullscreen);
        resolutionDropdown.onValueChanged.AddListener(SetResolution);
        backButton.onClick.AddListener(OnBackButtonClicked);
        bgmSoundSlider.onValueChanged.AddListener(SetBgmSoundSlider);
        quitGameBtn.onClick.AddListener(OnQuitGameBtnClicked);
    }

    private void OnQuitGameBtnClicked()
    {
        Application.Quit();
    }

    private void SetBgmSoundSlider(float arg0)
    {
        float suffix = arg0 <= 0 ? 20f : 0;
        float volume = lowestDB - lowestDB * arg0 - suffix;
        masterAudioMixer.SetFloat(bgmVolumeString, volume);
        PlayerPrefs.SetFloat(bgmVolumeString, arg0);
    }

    private void SetMasterVolumeSlider(float arg0)
    {
        float suffix = arg0 <= 0 ? 20f : 0;
        float volume = lowestDB - lowestDB * arg0 - suffix;
        masterAudioMixer.SetFloat(volumeString, volume);
        PlayerPrefs.SetFloat(volumeString, arg0);
    }

    private void OnBackButtonClicked()
    {
        gameObject.SetActive(false);
    }

    public void Show(bool inGame = false)
    {
        gameObject.SetActive(true);
        if (inGame)
        {
            gameGraphicItem.gameObject.SetActive(false);
            quitGameBtn.gameObject.SetActive(false);
        }
    }
    private void SetResolution(int arg0)
    {
        Resolution resolution = resolutions[arg0];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
        PlayerPrefs.SetInt(resolutionWidthPref, resolution.width);
        PlayerPrefs.SetInt(resolutionHeightPref, resolution.height);
    }
    private void SetUpResolutionDropdown()
    {
        resolutions = Screen.resolutions;
        resolutionDropdown.ClearOptions();
        List<string> options = new List<string>();
        int currentResolutionIndex = 0;
        for (int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + "x" + resolutions[i].height;
            options.Add(option); 
            if (resolutions[i].width == Screen.currentResolution.width && 
                resolutions[i].height == Screen.currentResolution.height)
            {
                currentResolutionIndex = i;
            }
        }
        resolutionDropdown.AddOptions(options);
        resolutionDropdown.value = currentResolutionIndex;
        resolutionDropdown.RefreshShownValue();
    }
    
    private void SetQuality(int index)
    {
        QualitySettings.SetQualityLevel(index);
        PlayerPrefs.SetInt(qualitySettingPref, index);
    }
    private void SetFullscreen(bool isFullScreen)
    {
        Screen.fullScreen = isFullScreen;
        PlayerPrefs.SetInt(fullScreenPref, GetFullscreenEnumIntPref(isFullScreen));
    }
    public void Default()
    {
        fullscreenToggle.enabled = fullScreenDict[(IsFullScreen)PlayerPrefs.GetInt(fullScreenPref)];
        Screen.fullScreen = fullScreenDict[(IsFullScreen)PlayerPrefs.GetInt(fullScreenPref)];
        soundSlider.value = PlayerPrefs.GetFloat(volumeString, 1f);
        bgmSoundSlider.value = PlayerPrefs.GetFloat(bgmVolumeString, 1f);
        SetMasterVolumeSlider(PlayerPrefs.GetFloat(volumeString, 1f));
        SetBgmSoundSlider(PlayerPrefs.GetFloat(bgmVolumeString, 1f));
        QualitySettings.SetQualityLevel(PlayerPrefs.GetInt(qualitySettingPref, 6)); ;
        graphicDropdown.value = PlayerPrefs.GetInt(qualitySettingPref, 6);

        Screen.SetResolution(PlayerPrefs.GetInt(resolutionWidthPref, Screen.currentResolution.width), 
            PlayerPrefs.GetInt(resolutionHeightPref, Screen.currentResolution.height),
                fullScreenDict[(IsFullScreen)PlayerPrefs.GetInt(fullScreenPref, (int)IsFullScreen.True)]);
        SetUpResolutionDropdown();

    }
    const string qualitySettingPref = "QualitySettings";
    const string resolutionWidthPref = "resolution.width";
    const string resolutionHeightPref = "resolution.height";
    const string fullScreenPref = "Screen.FullScreen";
    private Dictionary<IsFullScreen, bool> fullScreenDict = new Dictionary<IsFullScreen, bool>()
    {
        {IsFullScreen.True, true },
        {IsFullScreen.False, false }
    };

    private int GetFullscreenEnumIntPref(bool check)
    {
        if (fullScreenDict[IsFullScreen.False] == check) return (int)IsFullScreen.False;
        return (int)IsFullScreen.True;
    }

}

public enum IsFullScreen
{
    True, False
}
