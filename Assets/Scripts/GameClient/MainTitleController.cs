using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using System;

public class MainTitleController : MonoBehaviour
{
    [SerializeField] AudioSource mainTitleBGMplayer;
    [SerializeField] RewardPanelPopUp rewardPanelPopUp;
    [SerializeField] Image xpFiller;
    [SerializeField] TextMeshProUGUI levelText;
    [SerializeField] TextMeshProUGUI goldText;
    [SerializeField] TextMeshProUGUI upgradePartText;

    [SerializeField] MapSelectPanel mapSelectPanel;
    [SerializeField] SettingMenu settingPanel;
    [SerializeField] GameObject mainBody;
    [SerializeField] GameObject ShopPanel;
    [SerializeField] EquipmentPanel equipmentPanel;
    [SerializeField] AchievementScrollerController achievementPanel;
    [SerializeField] Button achievementButton;
    [SerializeField] Button ToBattleButton;
    [SerializeField] Button settingButton;
    [SerializeField] Button ShopButton;
    [SerializeField] Button EquipmentButton;

    [SerializeField] Image HomeMapIcon;
    [SerializeField] TextMeshProUGUI HeaderHomeMapText;

    [HideInInspector][SerializeField] int currentAccountLevel;
    private List<bool> LevelRewardsListChecker;

    // Start is called before the first frame update
    void Start()
    {
        UserData.AddUserGold(10000);

        ShopButton.onClick.AddListener(OnShopButtonClicked);
        EquipmentButton.onClick.AddListener(OnEquipmentButtonClicked);
        ToBattleButton.onClick.AddListener(OnToBattleButton);
        settingButton.onClick.AddListener(OnSettingButtonClicked);
        achievementButton.onClick.AddListener(OnAchievementButtonClicked);
        currentAccountLevel = UserData.GetUserCurrentLevel();
        SetHomeMapVisual();
        SetLevelXpProgress();
        SetMainBodyActive();
        CheckAccountLevelReward();
        SetMoney();
        settingPanel.Default();
    }

    private void CheckAccountLevelReward()
    {
        LevelRewardsListChecker = UserData.LoadAccountLvReward();
        int lastestLvRewarded = UserData.GetLastestLvAccountRewardFromSaveData();
        if (lastestLvRewarded < currentAccountLevel)
        {
            PopUpAccountLevelRewards(lastestLvRewarded);
            UserData.SaveAccountlvReward(currentAccountLevel);   
        }
    }

    private void PopUpAccountLevelRewards(int lastestLvRewarded)
    {
        Dictionary<Currency, int> rewardsDict = new Dictionary<Currency, int>();
        for (int i = UserData.GetUserCurrentLevel(); i >= lastestLvRewarded; i--)
        {
            if (LevelRewardsListChecker[i] == false)
            {
                foreach (var keyValue in UserData.GetUserRewardsDict(i))
                {
                    if (rewardsDict.ContainsKey(keyValue.Key))
                        rewardsDict[keyValue.Key] += keyValue.Value;
                    else
                        rewardsDict.Add(keyValue.Key, keyValue.Value);
                }
            }
        }
        rewardPanelPopUp.PopUpCurrencyRewards(rewardsDict);
        SetMoney();
        xpFiller.fillAmount = 0;
    }

    public void AddUserXpUI(int xp)
    {
        UserData.AddUserXP(xp);
        SetLevelXpProgress();
        if (currentAccountLevel < UserData.GetUserCurrentLevel())
        {
            PopUpAccountLevelRewards(currentAccountLevel);
            currentAccountLevel = UserData.GetUserCurrentLevel();
        }
    }

    private void OnAchievementButtonClicked()
    {
        achievementPanel.Show();
    }

    private void OnSettingButtonClicked()
    {
        settingPanel.gameObject.SetActive(true);
    }

    private void OnShopButtonClicked()
    {
        ShopPanel.SetActive(true);
    }
    private void OnEquipmentButtonClicked()
    {
        equipmentPanel.Show();
        SetMainBodyActive(false);
    }

    private void OnToBattleButton()
    {
        mapSelectPanel.Show();
    }

    private void SetHomeMapVisual()
    {
        HeaderHomeMapText.text = "Map " + (MapProgressData.HomeMapVisualSelectedID + 1).ToString();
        HomeMapIcon.sprite = Resources.Load<Sprite>(MapProgressData.missionImageResourcePath + MapProgressData.missionFormPath +
            (MapProgressData.HomeMapVisualSelectedID + 1).ToString().PadLeft(3, '0'));
    }

    private void SetLevelXpProgress()
    {
        levelText.text = currentAccountLevel.ToString();
        float max = UserData.GetMaxXPOfCurrentLevel(currentAccountLevel);
        //xpFiller.fillAmount = (float)UserData.userXP / max;
        CommonTweener.ProgressBarUp(xpFiller, (float)UserData.userXP / max);
    }

    public void SetMoney()
    {
        goldText.text = UserData.userGold.ToString();
        upgradePartText.text = UserData.userUpgradePart.ToString();
    }
    public void SetMainBodyActive(bool check = true)
    {
        mainBody.SetActive(check);
    }
}
