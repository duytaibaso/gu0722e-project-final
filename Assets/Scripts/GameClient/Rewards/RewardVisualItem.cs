using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class RewardVisualItem : MonoBehaviour
{
    [SerializeField] Image Background;
    [SerializeField] Image Icon;
    [SerializeField] TextMeshProUGUI qty;

    public void SetVisualData(Sprite bg, Sprite icon, int qty)
    {
        Background.sprite = bg;
        Icon.sprite = icon;
        this.qty.text = "x" + qty.ToString();
    }

    public void SetVisualData(Sprite bg, Sprite icon, string qty)
    {
        Background.sprite = bg;
        Icon.sprite = icon;
        this.qty.text = "x" + qty;
    }
}

public class RewardVisualItemData {
    public Sprite bg;
    public Sprite icon;
    public int qty;
    public RewardVisualItemData(Sprite _bg, Sprite _icon, int _qty)
    {
        bg = _bg;
        icon = _icon;
        qty = _qty;
    }
}

