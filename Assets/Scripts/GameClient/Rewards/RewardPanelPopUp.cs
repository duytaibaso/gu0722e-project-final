using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Sirenix.OdinInspector;

public class RewardPanelPopUp : SerializedMonoBehaviour
{
    [SerializeField] Button wholePanelButtn;
    [SerializeField] Transform rewardsHolder;
    [SerializeField] RewardVisualItem visualItem;
    [SerializeField] Dictionary<Currency, Sprite> currencyRewardIcon;
    [SerializeField] Dictionary<Currency, Sprite> currencyRewardBG;


    // Start is called before the first frame update
    void Start()
    {
        wholePanelButtn.onClick.AddListener(OnWholePanelButton);
    }

    private void OnWholePanelButton()
    {
        gameObject.SetActive(false);
        Clear();
    }

    public void PopUpMixedRewards(Dictionary<Currency, int> rewardsDict, List<RewardVisualItemData> list)
    {
        Clear();
        List<RewardVisualItemData> rewardCurrencyVisualItemList = new List<RewardVisualItemData>();
        foreach (var keyValue in rewardsDict)
        {
            rewardCurrencyVisualItemList.Add(new RewardVisualItemData(currencyRewardBG[keyValue.Key], currencyRewardIcon[keyValue.Key], keyValue.Value));
        }
        for (int i = 0; i < rewardCurrencyVisualItemList.Count; i++)
        {
            visualItem.SetVisualData(rewardCurrencyVisualItemList[i].bg, rewardCurrencyVisualItemList[i].icon, rewardCurrencyVisualItemList[i].qty);
            Instantiate(visualItem.gameObject, rewardsHolder);
        }
        for (int i = 0; i < list.Count; i++)
        {
            visualItem.SetVisualData(list[i].bg, list[i].icon, list[i].qty);
            Instantiate(visualItem.gameObject, rewardsHolder);
        }
        gameObject.SetActive(true);
    }

    public void PopUpCurrencyRewards(Dictionary<Currency, int> rewardsDict)
    {
        List<RewardVisualItemData> rewardVisualItemList = new List<RewardVisualItemData>();
        foreach (var keyValue in rewardsDict)
        {
            rewardVisualItemList.Add(new RewardVisualItemData(currencyRewardBG[keyValue.Key], currencyRewardIcon[keyValue.Key], keyValue.Value));
        }
        PopUpCurrencyRewards(rewardVisualItemList);
    }

    public void PopUpCurrencyRewards(List<RewardVisualItemData> list)
    {
        Clear();
        for (int i = 0; i < list.Count; i++)
        {
            visualItem.SetVisualData(list[i].bg, list[i].icon, list[i].qty);
            Instantiate(visualItem.gameObject, rewardsHolder);
        }
        gameObject.SetActive(true);
    }
    public void PopUpOneItemReward(Sprite bg, Sprite icon, int level)
    {
        Clear();
        string newLevelDisplay = "Lv. " + level.ToString();
        visualItem.SetVisualData(bg, icon, newLevelDisplay);
        Instantiate(visualItem.gameObject, rewardsHolder);
        gameObject.SetActive(true); 
    }

    private void Clear()
    {
        for (int i = 0; i < rewardsHolder.childCount; i++)
        {
            Destroy(rewardsHolder.GetChild(i).gameObject);
        }


    }



}
