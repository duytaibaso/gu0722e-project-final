using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using AirFishLab.ScrollingList;

public class MapItemIntListBank : BaseListBank
{
    private readonly int[] Contents = new int[MapProgressData.MaxLevel];

    public override object GetListContent(int index)
    {
        return Contents[index];
    }

    public override int GetListLength()
    {
        return Contents.Length;
    }





}
