using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AirFishLab.ScrollingList;

public class CircularScrollerIntListBank : BaseListBank
{
    private readonly int[] _contents = {
            1, 2, 3, 4, 5
        };

    public override object GetListContent(int index)
    {
        return _contents[index];
    }

    public override int GetListLength()
    {
        return _contents.Length;
    }
}
