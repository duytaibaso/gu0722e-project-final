using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AirFishLab.ScrollingList;
using UnityEngine.SceneManagement;
using TMPro;
using System;

public class MapSelectPanel : MonoBehaviour
{
    [SerializeField] CircularMapListBox circularListBoxPrefab;
    [SerializeField] CircularScrollingList scroller;
    [SerializeField] Button StartGameButton;
    [SerializeField] Image homeIcon;
    [SerializeField] TextMeshProUGUI homeHeadeText;

    [SerializeField] Button backButton;

    private void Start()
    {
        StartGameButton.onClick.AddListener(OnStartGameButtonClicked);
        backButton.onClick.AddListener(OnBackButtonClicked);
        CreateList();
    }

    public void Show()
    {
        //CreateList();
        gameObject.SetActive(true);
    }

    private void OnBackButtonClicked()
    {
        gameObject.SetActive(false);
    }

    private void CreateList()
    {
        List<ListBox> list = new List<ListBox>();
        for (int i = 0; i < MapProgressData.MaxLevel; i++)
        {
            Sprite sprite = Resources.Load<Sprite>(MapProgressData.missionImageResourcePath + MapProgressData.missionFormPath + (i + 1).ToString().PadLeft(3, '0'));
            circularListBoxPrefab.SetIMG(sprite, i);
            var obj = Instantiate(circularListBoxPrefab.gameObject, scroller.transform);
            list.Add(obj.GetComponent<CircularMapListBox>());
        }
        list.Reverse();
        scroller.LoadItems(list, MapProgressData.HomeMapVisualSelectedID);
    }

    private void OnStartGameButtonClicked()
    {
        MapProgressData.SetHomeMapVisualSelectedID(scroller.GetCenteredContentID());
        MapProgressData.SetCurrentLevel( scroller.GetCenteredContentID() + 1);
        SceneManager.LoadScene(MapProgressData.gameplaySceneName);
    }


}
