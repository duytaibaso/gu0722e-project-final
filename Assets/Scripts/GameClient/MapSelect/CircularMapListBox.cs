using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AirFishLab.ScrollingList;

public class CircularMapListBox : ListBox
{
    [SerializeField] Image mapImg;
    [SerializeField] GameObject locker;
    public void SetIMG(Sprite sprite, int index)
    {
        mapImg.sprite = sprite;
        locker.SetActive(false);
        mapImg.color = Color.white;
        if (index + 1 > MapProgressData.UnlockedLevel)
        {
            mapImg.color = Color.gray;
            locker.SetActive(true);
        }
    }


}
