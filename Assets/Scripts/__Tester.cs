using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System.IO;
using UnityEngine.SceneManagement;
public class __Tester : MonoBehaviour
{
    const string FileName = "InventoryGunItemDataList.txt";
    const string FileName2 = "InventoryMeleeItemDataList.txt";
    // Start is called before the first frame update
    [SerializeField] EquipmentSystem sys;
    [SerializeField] MainTitleController mainTitle;
    [SerializeField] int level;

    [Button(50)]
    private void PlayLevel()
    {
        MapProgressData.SetCurrentLevel(level);
        SceneManager.LoadScene(MapProgressData.gameplaySceneName);
    }

    [Button(50)]
    private void ClearAchievement()
    {
        UserData.ClearAllRecordAchievement();
    }

    [Button(ButtonHeight = 50)]
    private void AddAchievementObject()
    {
        UserData.AddTotalTimeSurvive(120);
        UserData.AddTotalKills(1000);
        UserData.AddUserGold(100000);
    }
    [Button(ButtonHeight = 50)]
    private void AddItems()
    {
        //public GunItem(Rarity rarity, GunWeaponType type, int level)
        sys.AddInventoryGunItem(new GunItem(Rarity.Purple, GunWeaponType.AK47, 24));
        sys.AddInventoryGunItem(new GunItem(Rarity.Red, GunWeaponType.AK47, 12));
    }
    [Button(ButtonHeight = 50)]
    private void AddXp()
    {
        UserData.AddUserXP(100);
    }
    [Button(ButtonHeight = 50)]
    private void AddXpOnUIRespone()
    {
        mainTitle.AddUserXpUI(100);
    }

    [Button(ButtonHeight = 50)]
    private void ResetUserXP()
    {
        PlayerPrefs.DeleteKey("userXP");
    }
    [Button(ButtonHeight = 50)]
    private void AddMoney()
    {
        UserData.AddUserGold(1000000);
        UserData.AddUserUpgradePart(300);
    }
    [Button(ButtonHeight = 50)]
    private void CheckMoney()
    {
        Debug.Log(UserData.userGold);
        Debug.Log(UserData.userUpgradePart);
    }
    [Button(ButtonHeight = 50)]
    private void ClearJson()
    {
        if (File.Exists(Application.persistentDataPath + "InventoryGunItemDataList.txt"))
        {
            File.Delete(Application.persistentDataPath + "InventoryGunItemDataList.txt");
        }
        if (File.Exists(Application.persistentDataPath + "InventoryMeleeItemDataList.txt"))
        {
            File.Delete(Application.persistentDataPath + "InventoryMeleeItemDataList.txt");
        }
    }
    //[Button(ButtonHeight = 50)]
    //public static void ClearAchievementSaveData()
    //{
    //    if (File.Exists(Application.persistentDataPath + DataText))
    //    {
    //        File.Delete(Application.persistentDataPath + DataText);
    //    }
    //}

    //[MenuItem("User Data/Add Currency")]
    //public static void AddMoney()
    //{
    //    userGold += 1000;
    //    userUpgradePart += 100;
    //}
    //[MenuItem("User Data/Reset Currency")]
    //public static void ResetMoney()
    //{
    //    userGold = 0;
    //    userUpgradePart = 0;
    //}

    //[MenuItem("User Data/Add Exp")]
    //public static void AddExp()
    //{
    //    userXP += 24;
    //    Debug.Log(userXP + " xp");
    //}
    //[MenuItem("User Data/Clear Exp")]
    //public static void ClearExp()
    //{
    //    userXP = 0;
    //}

}
