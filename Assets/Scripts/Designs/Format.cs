using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Format
{
    public static string TimeFormatInMinutesSeconds(float displayTime)
    {
        float minutes = Mathf.FloorToInt(displayTime / 60);
        float seconds = Mathf.FloorToInt(displayTime % 60);
        return string.Format("{0:00}:{1:00}", minutes, seconds);
    }

    public static string FormatIntThousandMillion(int amount)
    {
        string newString = amount.ToString();
        if (amount >= 1000) newString = newString.Remove(newString.Length - 3) + "K";
        else if (amount >= 1000000) newString = newString.Remove(newString.Length - 6) + "M"; 

        return newString;
    }

}
