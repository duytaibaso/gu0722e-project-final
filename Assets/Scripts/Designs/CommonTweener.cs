
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class CommonTweener
{
    public static void TweenRotateNinetyDegreeWithCallBack(Transform transform, TweenCallback callback, int dir = 1)
    {
        transform.DOLocalRotate(new Vector3(0, 0, dir * 90f), 0.2f).SetEase(Ease.Linear).OnComplete(callback);
    }

    public static void TweenUIScaleFromSmallToNormal(Transform transform, TweenCallback callBack)
    {
        transform.localScale = new Vector3(0.8f, 0.8f);
        transform.DOScale(1f, 0.3f).SetEase(Ease.OutBounce).OnComplete(callBack);
    }

    public static void TweenShaker(Transform transform)
    {
        transform.DOShakeRotation(0.1f).SetEase(Ease.InBounce).SetLoops(5, LoopType.Restart);
    }

    public static void FlashRed(SpriteRenderer render)
    {
        Color red = Color.red;
        render.DOColor(new Color(red.r, red.g/2, red.b/2), 0.1f).SetEase(Ease.InBack).OnComplete(() => render.color = Color.white);
    }

    public static void ProgressBarUp(Image image, float value)
    {
        image.DOFillAmount(value, 0.3f).SetEase(Ease.Linear);
    }

}
