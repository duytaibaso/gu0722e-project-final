using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
public class WarnPopUp : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI text;
    [SerializeField] Button YesButt;
    [SerializeField] Button NoButt;
    [HideInInspector] [SerializeField] Action action;
    // Start is called before the first frame update
    void Start()
    {
        YesButt.onClick.AddListener(OnYesButtClicked);
        NoButt.onClick.AddListener(OnNoButtClicked);
    }

    public void SetVisualData(string info, Action doSth)
    {
        text.text = info;
        action = doSth;
        gameObject.SetActive(true);
    }
    
    private void OnYesButtClicked()
    {
        action.Invoke();
        Close();
    }
    private void OnNoButtClicked()
    {
        Close();
    }
    private void Close()
    {
        gameObject.SetActive(false);
    }
}
