using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Sirenix.OdinInspector;

public class __GamplayTest : MonoBehaviour
{
    [SerializeField] GameManager gameManager;
    [Button(50)]
    private void ResetKeyPlayerPrefChapterLevel()
    {
        PlayerPrefs.SetInt("UnlockedLevel", 1);
        PlayerPrefs.SetInt("CurrentLevel", 1);
    }

    [Button(50)]
    private void AddCurrencyReward()
    {
        gameManager.AddReward(Currency.Gold, 200);
        gameManager.AddReward(Currency.XP, 100);
        gameManager.AddReward(Currency.UpgradePart, 20);
    }
    [Button(50)]
    private void CheckCurrencyReceive()
    {
        Debug.Log("gold " + gameManager.GetAmountCurrencyDict()[Currency.Gold]);
        Debug.Log("xp " + gameManager.GetAmountCurrencyDict()[Currency.XP]);
        Debug.Log("upgradePart " + gameManager.GetAmountCurrencyDict()[Currency.UpgradePart]);
    }

    [Button(50)]
    private void EndGame()
    {
        gameManager._Victory();
    }
}
