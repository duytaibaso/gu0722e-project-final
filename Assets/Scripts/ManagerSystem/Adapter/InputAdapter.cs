using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputAdapter : MonoBehaviour
{
    public const string VerticalSettingInputString = "Vertical";
    public const string HorizontalSettingInputString = "Horizontal";

    public static InputAdapter Instance;
    [SerializeField] GameManager gameManager;
    [SerializeField] InputMode mode;
    [SerializeField] Joystick joystick;
    private void Awake()
    {
        Instance = this;
        gameManager.AndroidPlatformNecessariesActive += ActiveJoyStick;
    }

    private void ActiveJoyStick(bool check)
    {
        joystick.gameObject.SetActive(check);
    }

    public enum InputMode
    {
        Keyboard = 0, Joystick = 1
    }

    public bool KeyEscPressedUp
    {
        get => Input.GetKeyUp(KeyCode.Escape);
    }

    public float Vertical
    {
        get
        {
            if (mode == InputMode.Joystick ||Mathf.Abs(joystick.Vertical) > 0.001f)
            {
                return joystick.Vertical;
            }
            return  Input.GetAxisRaw(VerticalSettingInputString);
        }
    }

    public float Horizontal
    {
        get
        {
            if (mode == InputMode.Joystick ||Mathf.Abs(joystick.Horizontal) > 0.001f)
            {
                return joystick.Horizontal;
            }

            return Input.GetAxisRaw(HorizontalSettingInputString);
        }
    }

    public bool KeyRPressedUp
    {
        get { return Input.GetKeyUp(KeyCode.R); }
    }
    public bool KeyEPressedUp
    {
        get { return Input.GetKeyUp(KeyCode.E); }
    }
    public bool KeyQPressedUp
    {
        get { return Input.GetKeyUp(KeyCode.Q); }
    }
    public bool MeleeAttackPressedUp { get { return Input.GetKeyUp(KeyCode.F) || Input.GetMouseButtonUp(1); } }

    public bool Mouse0Pressed
    {
        get { return Input.GetMouseButton(0); }
    }
    public bool Key1Pressed
    {
        get { return Input.GetKeyDown(KeyCode.Alpha1); }
    }
    public bool Key2Pressed
    {
        get { return Input.GetKeyDown(KeyCode.Alpha2); }
    }
    public bool Key3Pressed
    {
        get { return Input.GetKeyDown(KeyCode.Alpha3); }
    }
    public bool CheckIsNotMoving()
    {
        return Vertical == 0 && Horizontal == 0;
    }


}
