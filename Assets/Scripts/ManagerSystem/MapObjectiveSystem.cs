using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class MapObjectiveSystem : MonoBehaviour
{
    [SerializeField] MissionObjectiveData missionObjectiveData;



    public MissionObjectiveType GetObjType(int mapLevel)
    {
        return missionObjectiveData.GetObjType(mapLevel);
    }

    public bool CheckMissionIsCompleted(int condition, int mapLevel)
    {
        //neu la clear nwenemy thi tinh so enenmy kill nhieu
        if (missionObjectiveData.GetObjType(mapLevel) == MissionObjectiveType.ClearEnemy) 
            return condition >= missionObjectiveData.GetObjCondition(mapLevel) ;
        return 0 >= condition;
    }

    public int GetDecidedMapTimeValue(int mapLevel)
    {
        if (missionObjectiveData.GetObjType(mapLevel) == MissionObjectiveType.ClearEnemy) return 0;
        return missionObjectiveData.GetObjCondition(mapLevel);
    }

    public string GetObjTypeName(int mapLevel)
    {
        return missionObjectiveData.GetObjTypeDesc(missionObjectiveData.GetObjType(mapLevel)).Item1;
    }
    public string GetObjTypeDesc(int mapLevel)
    {
        return missionObjectiveData.GetObjTypeDesc(missionObjectiveData.GetObjType(mapLevel )).Item2;
    }


}






