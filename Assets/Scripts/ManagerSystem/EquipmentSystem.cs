using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;
using System.IO;
using UnityEditor;

public class EquipmentSystem : SerializedMonoBehaviour
{
    const string FileName = "InventoryGunItemDataList.txt";
    const string FileName2 = "InventoryMeleeItemDataList.txt";
    private string FilePath;
    private string FilePath2;
    public InventoryItemData ItemData;
    [SerializeField] ShopSystem shopSystem;
    [SerializeField] WeaponData weaponData;
    [SerializeField] Dictionary<Rarity, Sprite> BGItemRaritySpriteDict;


    private void Awake()
    {
        FilePath = Application.persistentDataPath + FileName;
        FilePath2 = Application.persistentDataPath + FileName2;
        LoadInventory();
        //primaryEnumWeaponEquipped = ((int)(GunWeaponType.AK47), ((int)(Rarity.Normal), 1).ToTuple()).ToTuple();
    }
    public WeaponData GetWeaponData()
    {
        return weaponData;
    }

    public Sprite GetBGItemRaritySpriteDict(Rarity rarity)
    {
        return BGItemRaritySpriteDict[rarity];
    }

    /// <summary>
    /// ind dau la enum weapon type, int sau la rarity va level
    /// </summary>
    public static Tuple<int, Tuple<int,int>> primaryEnumWeaponEquipped
    {
        get => (PlayerPrefs.GetInt("primaryEnumWeaponEquipped", (int)(GunWeaponType.AK47)), 
            (PlayerPrefs.GetInt("primaryEnumWeaponRarityEquipped", (int)(Rarity.Normal)), PlayerPrefs.GetInt("primaryWeaponLevelEquipped", 1)).
            ToTuple()).ToTuple();
        private set { PlayerPrefs.SetInt("primaryEnumWeaponEquipped", value.Item1); PlayerPrefs.SetInt("primaryEnumWeaponRarityEquipped",
            value.Item2.Item1);
            PlayerPrefs.SetInt("primaryWeaponLevelEquipped", value.Item2.Item2);
        }
    }
    public GunItem GetPrimaryEquippedWeapon()
    {
        return new GunItem(-1, (Rarity)primaryEnumWeaponEquipped.Item2.Item1,
            (GunWeaponType)primaryEnumWeaponEquipped.Item1, primaryEnumWeaponEquipped.Item2.Item2);
    }
    /// <summary>
    /// ind dau la enum weapon type, int sau la rarity va level
    /// </summary>
    public static Tuple<int, Tuple<int, int>> secondaryEnumWeaponEquipped
    {
        get => (PlayerPrefs.GetInt("secondaryEnumWeaponEquipped", (int)(GunWeaponType.Glock)),
            (PlayerPrefs.GetInt("secondaryEnumWeaponRarityEquipped", (int)(Rarity.Normal)), PlayerPrefs.GetInt("2ndWeaponLevelEquipped", 1)).ToTuple()).ToTuple();
        private set { PlayerPrefs.SetInt("secondaryEnumWeaponEquipped", value.Item1); PlayerPrefs.SetInt("secondaryEnumWeaponRarityEquipped", value.Item2.Item1);
            PlayerPrefs.SetInt("2ndWeaponLevelEquipped", value.Item2.Item2);
        }
    }

    public GunItem GetSecondaryEquippedWeapon()
    {
        return new GunItem(-2, (Rarity)secondaryEnumWeaponEquipped.Item2.Item1,
            (GunWeaponType)secondaryEnumWeaponEquipped.Item1, secondaryEnumWeaponEquipped.Item2.Item2);
    }

    /// <summary>
    /// ind dau la enum weapon type, int sau la rarity va level
    /// </summary>
    public static Tuple<int, Tuple<int, int>> meleeEnumWeaponEquipped
    {
        get => (PlayerPrefs.GetInt("meleeEnumWeaponEquipped", (int)(MeleeWeaponType.Katana)), 
            (PlayerPrefs.GetInt("meleeEnumWeaponRarityEquipped", (int)(Rarity.Normal)), PlayerPrefs.GetInt("meleeWeaponLevelEquipped", 1)).ToTuple()).ToTuple();
        private set { PlayerPrefs.SetInt("meleeEnumWeaponEquipped", value.Item1); PlayerPrefs.SetInt("meleeEnumWeaponRarityEquipped", value.Item2.Item1);
            PlayerPrefs.SetInt("meleeWeaponLevelEquipped", value.Item2.Item2);
        }
    }

    public MeleeItem GetEquippedMeleeWeapon()
    {
        return new MeleeItem(-3, (Rarity)meleeEnumWeaponEquipped.Item2.Item1,
            (MeleeWeaponType)meleeEnumWeaponEquipped.Item1, meleeEnumWeaponEquipped.Item2.Item2);
    }

    public void EquipPrimary(GunWeaponType type, Rarity rarity, int level)
    {
        primaryEnumWeaponEquipped = Tuple.Create((int)type, ((int)rarity,level).ToTuple());
    }
    public void Equip2ndary(GunWeaponType type, Rarity rarity, int level)
    {
        secondaryEnumWeaponEquipped = Tuple.Create((int)type, ((int)rarity, level).ToTuple());
    }
    public void EquipMelee(MeleeWeaponType type, Rarity rarity, int level)
    {
        meleeEnumWeaponEquipped = Tuple.Create((int)type, ((int)rarity, level).ToTuple());
    }
    private void Save()
    {
        JSONLizer.SerializeList<GunItem>(ItemData.InventoryGunItemDataList, FilePath);
        JSONLizer.SerializeList<MeleeItem>(ItemData.InventoryMeleeItemDataList, FilePath2);
    }
    public void LoadInventory()
    {
        if (System.IO.File.Exists(FilePath))
            ItemData.InventoryGunItemDataList = JSONLizer.DeserializeList<GunItem>(FilePath);
        else
            __TesterAddItem();
        if (System.IO.File.Exists(FilePath2))
            ItemData.InventoryMeleeItemDataList = JSONLizer.DeserializeList<MeleeItem>(FilePath2);
    }

    public void EquipPrimary(int index)
    {
        AddInventoryGunItem(GetPrimaryEquippedWeapon());
        GunItem item = ItemData.InventoryGunItemDataList[index];
        EquipPrimary(item.type, item.rarity, item.level);
        ItemData.InventoryGunItemDataList.RemoveAt(index);
        Save();
    }
    public void Equip2ndary(int index)
    {
        AddInventoryGunItem(GetSecondaryEquippedWeapon());
        GunItem item = ItemData.InventoryGunItemDataList[index];
        Equip2ndary(item.type, item.rarity, item.level);
        ItemData.InventoryGunItemDataList.RemoveAt(index);
        Save();
    }
    public void EquipMelee(int index)
    {
        MeleeItem item = ItemData.InventoryMeleeItemDataList[index];
        EquipMelee(item.type, item.rarity, item.level);
        ItemData.InventoryMeleeItemDataList.RemoveAt(index);
        Save();
    }
    #region Add - Replace Items
    public void AddInventoryGunItem(GunItem item)
    {
        item.index = ItemData.InventoryGunItemDataList.Count;
        ItemData.InventoryGunItemDataList.Add(item);
        Save();
    }

    public void AddInventoryMeleeItem(MeleeItem item)
    {
        item.index = ItemData.InventoryMeleeItemDataList.Count;
        ItemData.InventoryMeleeItemDataList.Add(item);
        Save();
    }
    public void ReplaceGunItem(int index, GunItem item)
    {
        ItemData.InventoryGunItemDataList[index] = item;
    }
    public void ReplaceMeleeItem(int index, MeleeItem item)
    {
        ItemData.InventoryMeleeItemDataList[index] = item;
    }
    #endregion

    #region LevelUp
    public void LevelUpGunItem(int index, int itemNextLevel)
    {
        //GunItem item = ItemData.InventoryGunItemDataList[index];
        GunItem item = GetTargetGunItem(index);
        item.level++;
        UserData.AddUserGold( - GetGoldRequiredForALevel(itemNextLevel));
        Save();
    }

    public void MaxLevelUpGunItem(int index,int itemNextLevel)
    {
        //GunItem item = ItemData.InventoryGunItemDataList[index];    
        GunItem item = GetTargetGunItem(index);
        int maxLevel = weaponData.GetWeaponMaxLevel(item.rarity);
        item.level = maxLevel;
        UserData.AddUserGold( - GetGoldRequiredForMaxLeveling(itemNextLevel, maxLevel));
        Save();
    }
    public int GetGoldRequiredForALevel(int nextLevel)
    {
        return weaponData.GetLevelUpGoldCostDict(nextLevel);
    }
    public int GetGoldRequiredForMaxLeveling(int fromLevel, int toLevel)
    {
        return weaponData.GetTotalGoldRequiredForLevelUp(fromLevel, toLevel);
    }
    public void LevelUpMeleeItem(int index, int itemNextLevel)
    {
        //MeleeItem item = ItemData.InventoryMeleeItemDataList[index];
        MeleeItem item = GetTargetMeleeItem(index);
        item.level++;
        UserData.AddUserGold( - GetGoldRequiredForALevel(itemNextLevel));
        Save();
    }

    public void MaxLevelUpMeleeItem(int index, int itemNextLevel)
    {
        //MeleeItem item = ItemData.InventoryMeleeItemDataList[index];
        MeleeItem item = GetTargetMeleeItem(index);
        int maxLevel = weaponData.GetWeaponMaxLevel(item.rarity);
        item.level = maxLevel;
        UserData.AddUserGold( - GetGoldRequiredForMaxLeveling(itemNextLevel, maxLevel));
        Save();
    }
    public bool CanLevelUp(int level, Rarity rarity)
    {
        return UserData.CheckUserCanPay(GetGoldRequiredForALevel(level + 1), Currency.Gold) && level < GetWeaponData().GetWeaponMaxLevel(rarity); ;
    }
    public bool CanMaxLevelUp(int level, Rarity rarity)
    {
        int maxLevel = weaponData.GetWeaponMaxLevel(rarity);
        return UserData.CheckUserCanPay(GetGoldRequiredForMaxLeveling(level + 1, maxLevel), Currency.Gold) && level < GetWeaponData().GetWeaponMaxLevel(rarity); ;
    }
    #endregion LevelUp


    #region Evo
    /// <summary>
    /// int dau la Gold, int sau la Upgrade Part
    /// </summary>
    private Dictionary<Rarity, Tuple<int, int>> EvolveRecipe = new Dictionary<Rarity, Tuple<int, int>>()
    {
        {Rarity.Blue, Tuple.Create(10000,10) },
        {Rarity.Green, Tuple.Create(20000,20) },
        {Rarity.Purple, Tuple.Create(50000,50) },
        {Rarity.Red, Tuple.Create(100000,100) },
        {Rarity.Yellow, Tuple.Create(200000,200) },
    };
    /// <summary>
    /// int dau la Gold, int sau la Upgrade Part
    /// </summary>
    /// <param name="rarity"></param>
    /// <returns></returns>
    public Tuple<int,int> GetEvolveRecipRequired(Rarity rarity)
    {
        return EvolveRecipe[rarity];
    }
    
    public bool CanEvolveGun(int level, Rarity rarity)
    {
        return (level == weaponData.GetWeaponMaxLevel(rarity));
    }
    public void EvolveGun(int index)
    {
        GunItem item = GetTargetGunItem(index);
        int rarityInt = (int)(item.rarity) + 1;
        item.rarity = (Rarity)(rarityInt);
        item.level = 1;
        Save();
    }

    public bool CanEvolveMelee(int index)
    {
        MeleeItem item = GetTargetMeleeItem(index);
        return (item.level == weaponData.GetWeaponMaxLevel(item.rarity));
    }
    public void EvolveMelee(int index)
    {
        MeleeItem item = GetTargetMeleeItem(index);
        int rarityInt = (int)(item.rarity) + 1;
        item.rarity = (Rarity)(rarityInt);
        item.level = 1;
        Save();
    }
    #endregion Evo


    public GunItem GenerateRandomGunItemRarityBelowPurple()
    {
        return new GunItem()
        {
            type = (GunWeaponType)UnityEngine.Random.Range((int)(GunWeaponType.Revolver), (int)(GunWeaponType.RPG)),
            level = 1,
            rarity = (Rarity)UnityEngine.Random.Range((int)Rarity.Normal, (int)Rarity.Purple),
        };
    }
    public MeleeItem GenerateRandomMeleeItemRarityBelowPurple()
    {
        return new MeleeItem()
        {
            type = (MeleeWeaponType)UnityEngine.Random.Range((int)(MeleeWeaponType.BaseBall), (int)(MeleeWeaponType.Katana)),
            level = 1,
            rarity = (Rarity)UnityEngine.Random.Range((int)Rarity.Normal, (int)Rarity.Purple),
        };
    }


    private void __TesterAddItem()
    {
        ItemData.InventoryGunItemDataList.Add(new GunItem(Rarity.Blue, GunWeaponType.AK47, 1));
        ItemData.InventoryGunItemDataList.Add(new GunItem(Rarity.Blue, GunWeaponType.Glock, 5));
        ItemData.InventoryGunItemDataList.Add(new GunItem(Rarity.Blue, GunWeaponType.Glock, 5));
        ItemData.InventoryGunItemDataList.Add(new GunItem(Rarity.Blue, GunWeaponType.Glock, 5));
        ItemData.InventoryGunItemDataList.Add(new GunItem(Rarity.Blue, GunWeaponType.Glock, 5));
        ItemData.InventoryGunItemDataList.Add(new GunItem(Rarity.Red, GunWeaponType.Revolver, 5));
        ItemData.InventoryGunItemDataList.Add(new GunItem(Rarity.Blue, GunWeaponType.RPG, 5));
        ItemData.InventoryMeleeItemDataList.Add(new MeleeItem(Rarity.Red, MeleeWeaponType.Katana, 1));
        ItemData.InventoryMeleeItemDataList.Add(new MeleeItem(Rarity.Red, MeleeWeaponType.BaseBall, 1));
    }

    //[MenuItem("Duy/Clear Equipment Inventory Data")]
    //public static void ClearEquipmentInventoryData()
    //{
    //    File.Delete(Application.persistentDataPath + FileName);
    //    File.Delete(Application.persistentDataPath + FileName2);
    //}
    private GunItem GetTargetGunItem(int index)
    {
        GunItem item;
        if (index == -1)
        {
            item = GetPrimaryEquippedWeapon();
        }
        else if (index == -2)
        {
            item = GetSecondaryEquippedWeapon();
        }
        else item = ItemData.InventoryGunItemDataList[index];
        return item;
    }
    private MeleeItem GetTargetMeleeItem(int index)
    {
        MeleeItem item;
        if (index == -3)
        {
            item = GetEquippedMeleeWeapon();
        }
        else item = ItemData.InventoryMeleeItemDataList[index];
        return item;
    }
}
[Serializable]
public class InventoryItemData
{
    public List<GunItem> InventoryGunItemDataList = new List<GunItem>();
    public List<MeleeItem> InventoryMeleeItemDataList = new List<MeleeItem>();
}

public class MeleeItem : WeaponItem
{
    public MeleeWeaponType type;
    public MeleeItem(){}
    public MeleeItem(int index, Rarity rarity, MeleeWeaponType type, int level)
    {
        this.rarity = rarity;
        this.type = type;
        this.level = level;
        this.index = index;
    }
    public MeleeItem(Rarity rarity, MeleeWeaponType type, int level)
    {
        this.rarity = rarity;
        this.type = type;
        this.level = level;
    }
    
}

public class GunItem : WeaponItem
{
    public GunWeaponType type;
    public GunItem() { }
    public GunItem(int index, Rarity rarity, GunWeaponType type, int level)
    {
        this.rarity = rarity;
        this.type = type;
        this.level = level;
        this.index = index;
    }
    public GunItem(Rarity rarity, GunWeaponType type, int level)
    {
        this.rarity = rarity;
        this.type = type;
        this.level = level;
    }

}

public class WeaponItem: ItemBase
{
    public int index;
    public int level;
}
public class UpgradePartItem : ItemBase
{
    public int qty;
    public UpgradePartItem(int qty)
    {
        this.qty = qty;
        rarity = Rarity.Normal;
    }
}
public class ItemBase
{
    public Rarity rarity;
}
public enum Rarity
{
    Normal = 0,
    Green = 1,
    Blue = 2,
    Purple = 3,
    Yellow = 4,
    Red = 5,
}


