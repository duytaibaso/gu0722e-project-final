using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
public class SoundMaster : SerializedMonoBehaviour
{
    [SerializeField] AudioSource mainBGMplayer;
    [SerializeField] AudioSource playerSoundFx;
    [SerializeField] AudioSource gunSoundFx;
    [SerializeField] AudioSource meleeSoundFx;
    [SerializeField] List<AudioSource> explosionFxChannels;

    [SerializeField] List<AudioClip> levelBGMList;
    //[SerializeField] AudioClip explosionFx;
    [SerializeField] Dictionary<PlayerSoundType, AudioClip> playerSoundDict;
    [SerializeField] Dictionary<GunSoundType, AudioClip> gunSoundDict;
    [SerializeField] Dictionary<MeleeSoundType, AudioClip> meleeSoundDict;
    //[SerializeField] AudioClip explosionSoundClip;
    [SerializeField] AudioClip victorySound;
    [SerializeField] AudioClip loserSound;
    [SerializeField] Dictionary<GunSoundType, float> soundVolumeEachGunTypeDict = new Dictionary<GunSoundType, float>()
    {
        { GunSoundType.AK47, 0.45f },
        { GunSoundType.Glock, 0.7f },
        { GunSoundType.RPG, 0.5f },
        { GunSoundType.Revolver, 0.5f },
        { GunSoundType.Reload, 0.5f },
    };

    // Start is called before the first frame update
    void Start()
    {
        mainBGMplayer.clip = levelBGMList[MapProgressData.CurrentLevel - 1];
        mainBGMplayer.Play();
    }

    public void PlayMeleeFxSound(MeleeWeaponType type)
    {
        meleeSoundFx.clip = meleeSoundDict[meleeWeaponTypeToSoundTypeTranslationDict[type]];
        meleeSoundFx.Play();
    }

    public void PlayPlayerFxSound(PlayerSoundType type)
    {
        //playerSoundFx.clip = playerSoundDict[type];
        //playerSoundFx.Play();
        playerSoundFx.PlayOneShot(playerSoundDict[type]);
    }
    public void PLayerGunFxSound(GunSoundType type)
    {
        if (type == GunSoundType.Reload)
        {
            gunSoundFx.clip = gunSoundDict[type];
            gunSoundFx.Play();
            return;
        }
        gunSoundFx.PlayOneShot(gunSoundDict[type], soundVolumeEachGunTypeDict[type]);
    }

    public void VictorySound()
    {
        mainBGMplayer.Pause();
        mainBGMplayer.loop = false;
        mainBGMplayer.clip = victorySound;
        mainBGMplayer.Play();
    }
    public void LoserSound()
    {
        mainBGMplayer.Pause();
        mainBGMplayer.loop = false;
        mainBGMplayer.clip = loserSound;
        mainBGMplayer.Play();
    }

    public void PlayAnExplosion()
    {
        GetExplosionAudioPlayer().Play();
    }

    private AudioSource GetExplosionAudioPlayer()
    {
        for (int i = 0; i < explosionFxChannels.Count; i++)
        {
            if (explosionFxChannels[i].isPlaying == false)
            {
                return explosionFxChannels[i];
            }
        }
        Debug.LogError("co gi do sai sai");
        return null;
    }


    private Dictionary<MeleeWeaponType, MeleeSoundType> meleeWeaponTypeToSoundTypeTranslationDict = new Dictionary<MeleeWeaponType, MeleeSoundType>()
    {
        {MeleeWeaponType.Katana, MeleeSoundType.Katana },
        {MeleeWeaponType.BaseBall, MeleeSoundType.BaseBall },
    };
}

public enum PlayerSoundType
{
    Run, Die, TakeDamage, Dash, GrenadeThrow, ShurikenSpinner, AirStrikeRainfallRockets, Heal, Forcefield
}
public enum GunSoundType
{
    Reload, AK47, Glock, RPG, Revolver
}

public enum MeleeSoundType
{
    Katana, BaseBall
}


