using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosiveFxManager : MonoBehaviour
{
    [SerializeField] int poolSize = 30;
    [SerializeField] GameObject explosionFx;
    [HideInInspector] [SerializeField] List<GameObject> explosionObjectPool = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < poolSize; i++)
        {
            var obj = Instantiate(explosionFx, transform);
            obj.SetActive(false);
            explosionObjectPool.Add(obj);
        }
    }

    public void SpawnExplosion(Vector3 position)
    {
        GetExplosionObj().transform.position = position;
    }
    private GameObject GetExplosionObj()
    {
        for (int i = 0; i < poolSize; i++)
        {
            if (explosionObjectPool[i].activeSelf == false)
            {
                explosionObjectPool[i].SetActive(true);
                return explosionObjectPool[i];
            }
        }
        Debug.LogError("null");
        return null;
    }


}
