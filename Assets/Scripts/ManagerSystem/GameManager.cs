using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;
using Sirenix.OdinInspector;
public class GameManager : SerializedMonoBehaviour
{
    public Action<bool> AndroidPlatformNecessariesActive;
    //public Action<bool> HUDIconInteractable;
    [SerializeField] ExplosiveFxManager explosiveManager;
    [SerializeField] SoundMaster soundMaster;
    [SerializeField] IngameItemsDropSystem dropItemSystem;
    [SerializeField] MapObjectiveSystem objectiveSystem;
    [SerializeField] SpawnSystem spawnSystem;
    [SerializeField] MapObjectiveSystem objSys;
    [SerializeField] LevelManager levelManager;
    [SerializeField] HeroesData heroesData;
    [SerializeField] GameMenuController gameMenuController;
    [SerializeField] Transform pointerTarget;
    [SerializeField] CameraFollow cameraFollow;
    [SerializeField] EnemySpawner enemyMainSpawner;
    [SerializeField] CountdownTimer countdownTimer;
    [HideInInspector] [SerializeField] GunWeaponController gunController;
    [HideInInspector] [SerializeField] Player player;
    [HideInInspector] [SerializeField] int gameplayMinuteFromZero = 0;
    [HideInInspector] [SerializeField] int damageCounter;

    [SerializeField] Dictionary<MissionObjectiveType, int> ConditionalObjectiveDict = new Dictionary<MissionObjectiveType, int>() {
        { MissionObjectiveType.ClearEnemy, 0 },
        { MissionObjectiveType.SurviveByTime, 0 },
    };
    [SerializeField] private Dictionary<Currency, int> amountCurrencyDict = new Dictionary<Currency, int>()
    {
        {Currency.Gold, 0 },
        {Currency.XP, 0 },
        {Currency.UpgradePart, 0 },
    };
    private void Start()
    {
        CheckPlatform();
        int gameplayMinute = objectiveSystem.GetDecidedMapTimeValue(MapProgressData.CurrentLevel);

        Player _player = Resources.Load<Player>(HeroesData.ResourcesPath +
            heroesData.GetHeroesResourceStringDict()[(Heroes)heroesData.heroesEquippedEnumInt]);
        var obj = Instantiate(_player.gameObject);
        obj.name = _player.name;
        obj.GetComponent<Player>().SetObjectTypes(pointerTarget, this);
        player = obj.GetComponent<Player>();
        gunController = player.GetGunController();

        levelManager.Load(obj.transform);
        cameraFollow.SetTargetFollow(player.gameObject);
        //countdownTimer.SetTimeValue(gameplayMinute * 60, true);
        countdownTimer.SetTimeValue(gameplayMinute * 60, objectiveSystem.GetObjType(MapProgressData.CurrentLevel) == MissionObjectiveType.SurviveByTime);
        enemyMainSpawner.SetMainSpawnerData(spawnSystem.GetSpawnData(), gameplayMinuteFromZero, MapProgressData.CurrentLevel);
        gameMenuController.SetMissionObjectiveTextInfo(objectiveSystem.GetObjTypeName(MapProgressData.CurrentLevel), 
            objectiveSystem.GetObjTypeDesc(MapProgressData.CurrentLevel));
        ConditionalObjectiveDict[objectiveSystem.GetObjType(MapProgressData.CurrentLevel)] = gameplayMinute;
    }

    public ExplosiveFxManager GetExplosiveManager()
    {
        return explosiveManager;
    }
    public SoundMaster GetSoundMaster()
    {
        return soundMaster;
    }
    public SpawnSystem GetSpawnSystem()
    {
        return spawnSystem;
    }
    public int GetEnemiesKilled()
    {
        return ConditionalObjectiveDict[MissionObjectiveType.ClearEnemy];
    }
    public Dictionary<Currency, int> GetAmountCurrencyDict()
    {
        return amountCurrencyDict;
    }
    public int GetDmgCounter()
    {
        return damageCounter;
    }
    private void CheckPlatform()
    {
        if (Application.platform == RuntimePlatform.Android)
            AndroidPlatformNecessariesActive.Invoke(true);
        else
        {
            AndroidPlatformNecessariesActive.Invoke(false);
        }
    }

    public Player GetPlayer()
    {
        return player;
    }
    public GameMenuController GetMenu()
    {
        return gameMenuController;
    }

    public void AddReward(Currency currency, int amount)
    {
        amountCurrencyDict[currency] += amount;
    }

    public void OnEnemyDie(Vector3 pos)
    {
        ConditionalObjectiveDict[MissionObjectiveType.ClearEnemy]++;
        gameMenuController.SetKillsCountText(ConditionalObjectiveDict[MissionObjectiveType.ClearEnemy]);
        dropItemSystem.RandomSpawnDropItem(pos);
        CheckEndingSceneCondition(MissionObjectiveType.ClearEnemy);
    }

    public void OnPlayerDie()
    {
        LoserGameOver();
    }

    public void Report1MinuteElasped()
    {
        //gameplayMinute--;
        ConditionalObjectiveDict[objectiveSystem.GetObjType(MapProgressData.CurrentLevel)]--;
        gameplayMinuteFromZero++;

        enemyMainSpawner.SetMainSpawnerData(spawnSystem.GetSpawnData(), gameplayMinuteFromZero, MapProgressData.CurrentLevel);

        CheckEndingSceneCondition(MissionObjectiveType.SurviveByTime);
    }

    private void CheckEndingSceneCondition(MissionObjectiveType type)
    {
        if (objectiveSystem.GetObjType(MapProgressData.CurrentLevel) != type) return;
        if (objectiveSystem.CheckMissionIsCompleted(ConditionalObjectiveDict[objectiveSystem.GetObjType(MapProgressData.CurrentLevel)],
            MapProgressData.CurrentLevel))
        {
            Victory();
        }
    }
    private void LoserGameOver()
    {
        soundMaster.LoserSound();
        PauseTime();
        gameMenuController.OnSceneCompleted(GenerateRewardList(), countdownTimer.Stop(), ConditionalObjectiveDict[MissionObjectiveType.ClearEnemy], false);
        AddCurrencyRewardAndRecordToUserData();
    }
    private void Victory()
    {
        soundMaster.VictorySound();
        PauseTime();
        gameMenuController.OnSceneCompleted(GenerateRewardList(), countdownTimer.Stop(), ConditionalObjectiveDict[MissionObjectiveType.ClearEnemy]);
        AddCurrencyRewardAndRecordToUserData();
    }
    
    private void AddCurrencyRewardAndRecordToUserData()
    {
        UserData.AddUserXP(amountCurrencyDict[Currency.XP]);
        UserData.AddUserUpgradePart(amountCurrencyDict[Currency.UpgradePart]);
        UserData.AddUserGold(amountCurrencyDict[Currency.Gold]);
        UserData.loadedFromGameplayScene = true;
        UserData.AddTotalKills(ConditionalObjectiveDict[MissionObjectiveType.ClearEnemy]);
    }

    private List<VisualCurrencyRewardItemData> GenerateRewardList()
    {
        List<VisualCurrencyRewardItemData> newList = new List<VisualCurrencyRewardItemData>() {
            new VisualCurrencyRewardItemData(Currency.Gold, amountCurrencyDict[Currency.Gold]),
            new VisualCurrencyRewardItemData(Currency.XP, amountCurrencyDict[Currency.XP]),
            new VisualCurrencyRewardItemData(Currency.UpgradePart, amountCurrencyDict[Currency.UpgradePart]),
        };
        return newList;
    }

    public void SetUpPlayerSkillsIconToInterface(SkillData[] datas, Action[] skillActiveAction)
    {
        gameMenuController.SetSkillIconSpritesHUD(datas, skillActiveAction);
    }
    public void SkillActivate(int index)
    {
        gameMenuController.EnactSkillIconCoolDown(index);
    }
    public void SetMeleeAction(Melee melee)
    {
        gameMenuController.SetMeleeAction(melee);
    }

    public void HUDSwitchGunButtonTapped()
    {
        gunController.SwitchGun();
    }

    public void SetPlayerHealthToMenuController(int currentHp)
    {
        gameMenuController.SetPlayerHealth(currentHp);
    }

    public void SendDamageToPlayer(int damage)
    {
        player.TakeDamage(damage);
    }
    public void SendDamageToEnemy(int damage, Enemy target)
    {
        target.TakeDamage(damage);
    }

    public void SendBulletInfoMenuController(int ammoMag, int ammoSupply)
    {
        gameMenuController.ReceiveCurrentBulletOfCurrentGunInfo(ammoMag, ammoSupply);
    }

    public void BackToTitleScene()
    {
        SceneManager.LoadScene(MapProgressData.maintitleSceneName);
    }
    public void ReloadThisScene()
    {
        SceneManager.LoadScene(MapProgressData.gameplaySceneName);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void PauseTime()
    {
        Time.timeScale = 0;
    }
    public void ResumeTime()
    {
        Time.timeScale = 1f;
    }

    public void _Victory()
    {
        soundMaster.VictorySound();
        PauseTime();
        gameMenuController.OnSceneCompleted(GenerateRewardList(), countdownTimer.Stop(), ConditionalObjectiveDict[MissionObjectiveType.ClearEnemy]);
        AddCurrencyRewardAndRecordToUserData();

    }

}
