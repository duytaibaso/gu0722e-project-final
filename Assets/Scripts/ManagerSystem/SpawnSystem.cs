using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class SpawnSystem : SerializedMonoBehaviour
{
    [SerializeField] MapLevelDataSpawn MapLevelEnemySpawnData;


    public MapLevelDataSpawn GetSpawnData()
    {
        return MapLevelEnemySpawnData;
    }

    public int GetEnemySpawnRate(EnemyType type)
    {
        return MapLevelEnemySpawnData.GetEnemySpawnRate(type);
    }


}
