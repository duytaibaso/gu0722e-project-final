using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using Sirenix.OdinInspector;

public class ShopSystem : SerializedMonoBehaviour
{
    const string lastRefreshedTimeConst = "lastRefreshedTime";
    const string FileName = "/ShopSystemData.txt";
    const int MaxDailyItems = 10;

    [SerializeField] MainTitleController mainTitleController;
    [SerializeField] EquipmentSystem equiptmentSys;
    [SerializeField] Dictionary<ShopItemType, Dictionary<Rarity, int>> ShopItemPriceTagDict;
    [SerializeField] Dictionary<Rarity, Sprite> BGShopItemRaritySpriteDict;
    [SerializeField] Dictionary<Rarity, Sprite> BGPriceTagSpriteDict;
    [SerializeField] Sprite UpgradePartSprite;
    private List<ShopItem> DailyShopItemData;
    private string FilePath;
    public string lastRefreshedTime
    {
        get => PlayerPrefs.GetString(lastRefreshedTimeConst, "");
        private set => PlayerPrefs.SetString(lastRefreshedTimeConst, value);
    }
    private int turnToRefreshInDay
    {
        get => PlayerPrefs.GetInt("turnToRefreshInDay", 1);
        set => PlayerPrefs.SetInt("turnToRefreshInDay", value);
    }
    private void Awake()
    {
        FilePath = Application.persistentDataPath + FileName;
        if (File.Exists(FilePath))
        {
            //DailyShopItemData = JSONLizer.DeserializeList<ShopItem>(FilePath);
            DailyShopItemData = LoadDailyShopItemData();
        }
        if (CheckIf1DayElasped()) NewDailyItemData();

    }
    public WeaponData GetWeaponData()
    {
        return equiptmentSys.GetWeaponData() ;
    }
    public Sprite GetBGItemRarity(Rarity rarity)
    {
        return BGShopItemRaritySpriteDict[rarity];
    }
    public Sprite GetBGPriceTagSpriteDict(Rarity rarity)
    {
        return BGPriceTagSpriteDict[rarity];
    }
    public Sprite GetUpgradePartSprite()
    {
        return UpgradePartSprite;
    }
    public void SaveDailyShopItemData()
    {
        JSONLizer.SerializeList<ShopItem>(DailyShopItemData, FilePath);
    }
    public List<ShopItem> LoadDailyShopItemData()
    {
        List<ShopItem> list = JSONLizer.DeserializeList<ShopItem>(FilePath);
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i].isMelee)
            {
                list[i].item = RetrieveSavedMeleeItem(list[i].weaponEnumTypeIndex, list[i].item.rarity);
            }
            else
            {
                list[i].item = RetrieveSavedGunItem(list[i].weaponEnumTypeIndex, list[i].item.rarity);
            }
        }
        return list;
    }
    #region DailyItemShop
    public void AddItemToInventory(int indx)
    {
        ShopItem shopItem = DailyShopItemData[indx];
        shopItem.Bought = true;
        switch (shopItem.itemType)
        {
            case ShopItemType.Gun:
                equiptmentSys.AddInventoryGunItem((shopItem.item as GunItem));
                break;
            case ShopItemType.Melee:
                equiptmentSys.AddInventoryMeleeItem((shopItem.item as MeleeItem));
                break;
            case ShopItemType.UpgradePart:
                UserData.AddUserUpgradePart((shopItem.item as UpgradePartItem).qty);
                mainTitleController.SetMoney();
                break;
        }
        SaveDailyShopItemData();
    }

    public int GetShopItemPrice(ShopItemType type, Rarity rarity)
    {
        return ShopItemPriceTagDict[type][rarity];
    }

    public bool CheckIf1DayElasped()
    {
        if (lastRefreshedTime == "")
        {
            SaveRefreshTime();
            return true;
        }
        DateTime parsed = DateTime.Parse(lastRefreshedTime);
        //if ((DateTime.Now - parsed).Days >= 1 || (DateTime.Now - parsed).Seconds > 24 * 3600)
        if ((DateTime.Now - parsed).Days >= 1)
        {
            turnToRefreshInDay = 1;
            return true;
        }
        return  false;
    }
    public bool HasTurnToRefresh()
    {
        return turnToRefreshInDay > 0;
    }
    public void RefreshItemClicked()
    {
        turnToRefreshInDay--;
        NewDailyItemData();
    }

    public void NewDailyItemData()
    {
        SaveRefreshTime();
        DailyShopItemData = GenerateRandomItems();
    }
    public void SaveRefreshTime()
    {
        lastRefreshedTime = DateTime.Now.ToString(); ;
    }
    private List<ShopItem> GenerateRandomItems()
    {
        List<ShopItem> list = new List<ShopItem>();
        for (int i = 0; i < MaxDailyItems; i++)
        {
            switch (UnityEngine.Random.Range((int)ShopItemType.Gun, (int)ShopItemType.UpgradePart))
            {
                case (int)ShopItemType.Gun:
                    list.Add(CreateCasualRarityGunItem(i));
                    break;
                case (int)ShopItemType.Melee:
                    list.Add(CreateCasualRarityMeleeItem(i));
                    break;
                case (int)ShopItemType.UpgradePart:
                    list.Add(CreateUpgradePartItem(i));
                    break;
            }
        }
        return list;
    }

    private ShopItem CreateCasualRarityMeleeItem(int index)
    {
        ItemBase item = equiptmentSys.GenerateRandomMeleeItemRarityBelowPurple();
        return new ShopItem(index,
            ShopItemType.Melee,
            item,
            new ItemPrice(ItemPriceType.Gold, GetShopItemPrice(ShopItemType.Melee,item.rarity)),
            (int)(item as MeleeItem).type, true
            );
    }

    private ShopItem CreateCasualRarityGunItem(int index)
    {
        ItemBase item = equiptmentSys.GenerateRandomGunItemRarityBelowPurple();
        return new ShopItem(index,
            ShopItemType.Gun,
            item,
            new ItemPrice(ItemPriceType.Gold, GetShopItemPrice(ShopItemType.Gun, item.rarity)),
            (int)(item as GunItem).type, false
            );
    }
    private ShopItem CreateUpgradePartItem(int index)
    {
        UpgradePartItem item = new UpgradePartItem(UnityEngine.Random.Range(1, 12));
        return new ShopItem(index,
            ShopItemType.UpgradePart,
            item,
            new ItemPrice(ItemPriceType.Gold, GetShopItemPrice(ShopItemType.UpgradePart, item.rarity) * item.qty),
            0, false
            );
    }
    private GunItem RetrieveSavedGunItem(int _enumType, Rarity _rarity)
    {
        return new GunItem()
        {
            type = (GunWeaponType)_enumType,
            level = 1,
            rarity = _rarity,
        };
    }
    private MeleeItem RetrieveSavedMeleeItem(int _enumType, Rarity _rarity)
    {
        return new MeleeItem()
        {
            type = (MeleeWeaponType)_enumType,
            level = 1,
            rarity = _rarity,
        };
    }
    #endregion
    #region Gacha
    public void GachaItem()
    {



    }
    #endregion
    
    public List<ShopItem> GetDailyShopItemData()
    {
        if (DailyShopItemData == null) NewDailyItemData();
        return DailyShopItemData;
    }

    private void __TesterResetRefreshDailyItem()
    {
        lastRefreshedTime = "";
        turnToRefreshInDay = 1;
    }

}

[Serializable]
public class ShopItem
{
    public int index;
    public bool Bought = false;
    public int qty;
    public ShopItemType itemType;
    public ItemBase item;
    public ItemPrice price;
    public int weaponEnumTypeIndex;
    public bool isMelee;
    //public ShopItem() { }
    public ShopItem(int idx , ShopItemType type, ItemBase item, ItemPrice price, int weaponEnumTypeIndex, bool isMelee)
    {
        index = idx;
        itemType = type;
        this.item = item;
        this.price = price;
        this.weaponEnumTypeIndex = weaponEnumTypeIndex;
        this.isMelee = isMelee;
    }
}
[Serializable]
public struct ItemPrice
{
    public ItemPriceType priceType;
    public int price;
    public ItemPrice (ItemPriceType type, int price)
    {
        priceType = type;
        this.price = price;
    }
}

public enum ShopItemType
{
    Gun, Melee, UpgradePart
}

public enum ItemPriceType
{
    Gold, Gem

}
