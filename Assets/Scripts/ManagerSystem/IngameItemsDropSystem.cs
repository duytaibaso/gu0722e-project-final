using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class IngameItemsDropSystem : SerializedMonoBehaviour
{
    //prefabs
    [SerializeField] GameManager gameManager;
    [SerializeField] Transform background;
    [SerializeField] DropItem dropItemPrefab;
    [SerializeField] Dictionary<ItemDrop, Sprite> dropItemSpriteDict;
    [SerializeField] int dropItemPoolSize;
    [HideInInspector] [SerializeField] List<DropItem> dropItemList = new List<DropItem>();
    //private Dictionary<ItemDrop, Currency> itemDropToCurrencyTranslationDict = new Dictionary<ItemDrop, Currency>()
    //{
    //    {ItemDrop.Gold, Currency.Gold },
    //    {ItemDrop.Exp, Currency.XP },
    //    {ItemDrop.UpgradePart, Currency.UpgradePart },
    //};

    private void Start()
    {
        for (int i = 0; i< dropItemPoolSize; i++)
        {
            var obj = Instantiate(dropItemPrefab.gameObject, transform, true);
            obj.SetActive(false);
            dropItemList.Add(obj.GetComponent<DropItem>());
        }
    }

    public void SendItemToPlayer(ItemDrop type)
    {
        switch (type)
        {
            case ItemDrop.Gold:
                //gameManager.AddReward(itemDropToCurrencyTranslationDict[ItemDrop.Gold], Random.Range(500, 1000));
                gameManager.AddReward(Currency.Gold, Random.Range(500, 1000));
                return;
            case ItemDrop.Ammo:
                gameManager.GetPlayer().GetGunController().SupplyAmmo(Random.Range(50, 100));
                return;
            case ItemDrop.Health:
                gameManager.GetPlayer().AddHp(Random.Range(5, 10) / 100 * PlayerData.maxHP);
                return;
            case ItemDrop.Exp:
                //gameManager.AddReward(itemDropToCurrencyTranslationDict[ItemDrop.Exp], Random.Range(20, 100));
                gameManager.AddReward(Currency.XP, Random.Range(20, 100));
                return;
            case ItemDrop.UpgradePart:
                //gameManager.AddReward(itemDropToCurrencyTranslationDict[ItemDrop.UpgradePart], Random.Range(1, 5));
                gameManager.AddReward(Currency.UpgradePart, Random.Range(1, 5));
                return;
        }
    }
    
    public void RandomSpawnDropItem(Vector3 pos)
    {
        ItemDrop randomItemEnum = DropRate();
        if (randomItemEnum == ItemDrop.None) return;

        DropItem item = GetDropItem();

        item.SetUp(this, dropItemSpriteDict[randomItemEnum], randomItemEnum);
        item.transform.position = pos;
    }

    private DropItem GetDropItem()
    {
        for (int i = 0; i < dropItemPoolSize; i++)
        {
            if (dropItemList[i].gameObject.activeSelf == false)
            {
                return dropItemList[i];
            }
        }
        Debug.Log("item null");
        return null;
    }

    private ItemDrop DropRate()
    {
        int i = Random.Range(0, 100);
        switch (i)
        {
            case int a when 40 < a && a <= 45:
                return ItemDrop.Health;
            case int a when 47 < a && a <= 60:
                return ItemDrop.Ammo;
            case int a when 80 < a && a <= 90:
                return ItemDrop.Exp;
            case int a when 90 < a && a <= 95:
                return ItemDrop.Gold;
            case int a when 96 < a && a < 100:
                return ItemDrop.UpgradePart;
        }
        return ItemDrop.None;
    }
}

public enum ItemDrop
{
    Gold = 0, Ammo = 1, Health = 2, Exp = 3, UpgradePart = 4, None = 5
}

